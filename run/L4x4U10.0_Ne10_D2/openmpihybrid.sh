#!/bin/sh

#$ -S /bin/sh
#$ -cwd
#$ -V
#$ -q gr3.q
#$ -pe openmpi16 16

export OMP_NUM_THREADS=1



mpirun  -npernode 16 -np 16 ./vmc.out xnamelist.def zinitial.def

