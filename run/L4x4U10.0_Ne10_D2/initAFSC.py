#!/usr/bin/env python
import sys, glob
from cmath import *
import random

def position(r):
    x=(r[0]+Lx)%Lx
    y=(r[1]+Ly)%Ly
    return (x,y)

def indexToPosition(i):
    x=i%Lx
    y=i/Lx
    return (x,y)

def positionToIndex(r):
    x=(r[0]+Lx)%Lx
    y=(r[1]+Ly)%Ly
    return x+Lx*y

def neighborIndex(i,dr):
    r=indexToPosition(i)
    x=r[0]+dr[0]
    y=r[1]+dr[1]
    return positionToIndex([x,y])

def direction(i,j):
    ri=indexToPosition(i)
    rj=indexToPosition(j)
    dx=(rj[0]-ri[0]+Lx)%Lx
    dy=(rj[1]-ri[1]+Ly)%Ly
    return (dx,dy)

def subIndex(i):
    r=indexToPosition(i)
    sx=r[0]%Sx
    sy=r[1]%Sy
    return sx+Sx*sy

def BrillouinZoneList(Lx,Ly,APFlag):
  wave = [] 
  if(APFlag == 1):  
    for mx in xrange(-Lx,Lx):
      qx = 2.0*float(0.5+mx)/float(Lx)
      for my in xrange(-Ly,Ly+1):
        qy = 2.0*float(my)/float(Ly)
        qx = round(qx,7)
        qy = round(qy,7)
        wave.append((qx,qy))
  else:
    for mx in range(-Lx,Lx+1):
      qx = 2.0*float(mx)/float(Lx)
      for my in xrange(-Ly,Ly+1):
        qy = 2.0*float(my)/float(Ly)
        qx = round(qx,7)
        qy = round(qy,7)
        wave.append((qx,qy))
  return wave

def AFBrillouinZoneList(Lx,Ly,APFlag):
  wave = [] 
  if(APFlag == 1):  
    for mx in xrange(-Lx/2,Lx/2):
      qx = 2.0*float(0.5+mx)/float(Lx)
      for my in xrange(-Ly/2,Ly/2+1):
        qy = 2.0*float(my)/float(Ly)
        qx = round(qx,7)
        qy = round(qy,7)
        if FlagAFB(qx,qy) == 0:
          wave.append((qx,qy))
  else:
    for mx in range(-Lx/2,Lx/2+1):
      qx = 2.0*float(mx)/float(Lx)
      for my in xrange(-Ly/2,Ly/2+1):
        qy = 2.0*float(my)/float(Ly)
        qx = round(qx,7)
        qy = round(qy,7)
        if FlagAFB(qx,qy) == 0:
          wave.append((qx,qy))
  return wave

def VecSum(x,y):
  return (round(x[0]+y[0],7), round(x[1]+y[1], 7))


def FlagAFB(kx,ky):
    flag=1
    #if((ky <= -kx+1.0) and (ky>kx-1.0) and (ky>-kx-1.0) and (ky <=kx+1.0)):
    if((ky <= -kx+1.0) and (ky>=kx-1.0) and (ky>=-kx-1.0) and (ky <=kx+1.0)):
      flag = 0
    return flag

if len(sys.argv)<7:
    print "./average.py Lx Ly Nup tp AF SC APFlag"
    sys.exit()
Lx  = int(sys.argv[1])
Ly  = int(sys.argv[2])
Ne  = int(sys.argv[3])
tp  = float(sys.argv[4])
AF  = float(sys.argv[5])
SC  = float(sys.argv[6])
APFlag = int(sys.argv[7])
Nsite = Lx*Ly

print "#Lx={0} Ly={1} Nup={2}  : AF={3}, SC={4} ".format(Lx,Ly,Ne,AF,SC)

pair = [[[]   for j in range(Nsite)]\
              for i in range(Nsite)]
QVector = (1.0,1.0)
BrillouinZone   = BrillouinZoneList(Lx,Ly,APFlag)
AFBrillouinZone = AFBrillouinZoneList(Lx,Ly,APFlag)
#print AFBrillouinZone
#print BrillouinZone

print "#Calc Band Energy"
ene = {}
dSC = {}
for k in BrillouinZone:
    (kx, ky) = (pi*k[0], pi*k[1])
    ene[k] = -2.0*(cos(kx)+cos(ky)) -4.0*tp*cos(kx)*cos(ky)
    dSC[k] = SC*(cos(kx)-cos(ky))

#icount = 0
etmp = []
bandA=[]
bandB=[]
xi={}
for k in AFBrillouinZone:
    (kx,ky) = k
    kQ = VecSum(k,QVector)  

    xi1 = 0.5*(ene[k]-ene[kQ])
    xi2 = 0.5*(ene[k]+ene[kQ])

    xi[k] = xi1

    eneA = xi2-sqrt(xi1*xi1+AF*AF)
    eneB = xi2+sqrt(xi1*xi1+AF*AF)
    eneAp =  sqrt(eneA*eneA + dSC[k]*dSC[k])
    eneAm = -sqrt(eneA*eneA + dSC[k]*dSC[k])
    eneBp =  sqrt(eneB*eneB + dSC[k]*dSC[k])
    eneBm = -sqrt(eneB*eneB + dSC[k]*dSC[k])
    
    bandA.append([kx,ky,eneA.real])
    bandB.append([kx,ky,eneB.real])
    
    #print k,eneA.real
    
    etmp.append( eneAp.real )
    etmp.append( eneAm.real )
    etmp.append( eneBp.real )
    etmp.append( eneBm.real )
etmp.sort()
print "\n"
#print bandA

print "#Calc Total Energy & Chemical Potential"
TotalEne = 0.0
for i in range(2*Ne):
  TotalEne = TotalEne + etmp[i]
#TotalEne = 2.0*TotalEne
mu=etmp[2*Ne-1]+etmp[2*Ne]
mu = mu*0.5
print mu
print "\n"


print "#Calc Pairing Orbital f_ij"
A = {}
B = {}
C = {}
for k in AFBrillouinZone:
    (kx,ky) = k
    kQ = VecSum(k,QVector)  

    xi1 = 0.5*(ene[k]-ene[kQ])
    xi2 = 0.5*(ene[k]+ene[kQ])

    if (xi1*xi1+AF*AF == 0.0):
      uk = 1.0 
      vk = 0.0 
    else:
      uk = sqrt(0.5*(1.0-xi1/sqrt(xi1*xi1+AF*AF)))
      vk = sqrt(0.5*(1.0+xi1/sqrt(xi1*xi1+AF*AF)))

    ea = xi2 - sqrt(xi1*xi1+AF*AF)-mu
    eb = xi2 + sqrt(xi1*xi1+AF*AF)-mu
    
    #sc = sSC
    sc = dSC[k]

    if(sc == 0.0):
      if(ea.real > 0.0):
        phia = 0.000
      else:
        sc = 1e-7
        phia = sc/(ea+sqrt(ea*ea+sc*sc))
      if(eb.real > 0.0):
        phib = 0.000
      else:
        sc = 1e-7
        phib = -sc/(eb+sqrt(eb*eb+sc*sc))
    else:
      phia =  sc/(ea+sqrt(ea*ea+sc*sc))
      phib = -sc/(eb+sqrt(eb*eb+sc*sc))

    A[k] =  uk*uk*phia-vk*vk*phib
    B[k] = -vk*vk*phia+uk*uk*phib
    C[k] =  (phia+phib)*uk*vk


num=0
for i in range(Nsite):
    ri=indexToPosition(i)
    for j in range(Nsite):
        rj=indexToPosition(j)
        pair[i][j] = 0.0
        xij = float(ri[0]-rj[0])
        yij = float(ri[1]-rj[1])
        QRi = pi*ri[0] + pi*ri[1]
        QRj = pi*rj[0] + pi*rj[1]
        for k in AFBrillouinZone:
          (kx, ky) = (pi*k[0], pi*k[1])
          #kQ = VecSum(k,QVector)  
          pair[i][j] += A[k]*exp(1J*kx*xij + 1J*ky*yij)
          pair[i][j] += B[k]*exp(1J*(kx+pi)*xij + 1J*(ky+pi)*yij)
          pair[i][j] += C[k]*exp(1J*kx*xij + 1J*ky*yij)*(exp(1J*pi*float(ri[0]+ri[1]))-exp(-1J*pi*float(rj[0]+rj[1])))
        if num < abs(pair[i][j]):
          num = abs(pair[i][j])


print "#Output Initial Wave Function for mVMC"
jastrow     = open('zjastrowidx.def','r')
data = jastrow.readline()
data = jastrow.readline().split()
Njastrow = int(data[1])
jastrow.close()

gut     = open('zgutzwilleridx.def','r')
data = gut.readline()
data = gut.readline().split()
Ngut = int(data[1])
gut.close()

dh2     = open('zdoublonholon2siteidx.def','r')
data = dh2.readline()
data = dh2.readline().split()
NDH2 = int(data[1])
dh2.close()

dh4     = open('zdoublonholon4siteidx.def','r')
data = dh4.readline()
data = dh4.readline().split()
NDH4 = int(data[1])
dh4.close()

orbit     = open('zorbitalidx.def','r')
data = orbit.readline()
data = orbit.readline().split()
Nidx = int(data[1])
data = orbit.readline()
data = orbit.readline()
data = orbit.readline()

OrbIdx = [[[ []] for j in range(Nsite)]\
                  for i in range(Nsite)]
OrbSgn = [[[ []] for j in range(Nsite)]\
                  for i in range(Nsite)]
PairWF = [[ []] for i in range(Nidx)]

loop = Nsite*Nsite
for idx in range(loop):
    data = orbit.readline().split()
    ns = int(data[0])
    mt = int(data[1])
    OrbIdx[ns][mt] = int(data[2])
    if APFlag == 1:
      OrbSgn[ns][mt] = float(data[3])
    else:
      OrbSgn[ns][mt] = 1.0
orbit.close()

for n in range(Nsite):
  for m in range(Nsite):
    PairWF[OrbIdx[n][m]]=OrbSgn[n][m]*pair[n][m]/num

ofile = open("zqp_init.dat",'w')
ofile.write("{0:.15f} {1:.15f} ".format(TotalEne/Nsite,0.0))
for idx in range(1+Ngut+Njastrow+6*NDH2+10*NDH4):
  ofile.write("{0:.15f} {1:.15f} ".format(0.0,0.0))
for idx in range(Nidx):
  ofile.write("{0:.15f} {1:.15f} ".format(PairWF[idx].real,0.0))
ofile.close()
