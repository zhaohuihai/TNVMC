/*
 * pfupdate_two.h
 *
 *  Created on: 2015年3月22日
 *      Author: zhaohuihai
 */

#ifndef SRC_PFUPDATE_TWO_H_
#define SRC_PFUPDATE_TWO_H_

void CalculateNewPfMTwo(const int mk, const int t, const int mi, const int s,
                        double *pfMNew, const int *eleIdx,
                        const int qpStart, const int qpEnd, double *buffer);
void CalculateNewPfMTwo2(const int ma, const int s, const int mb, const int t,
                         double *pfMNew, const int *eleIdx,
                         const int qpStart, const int qpEnd);

//void CalculateNewPfMTwo_

void calculateNewPfMTwo_child(const int ma, const int s, const int mb, const int t,
                              double *pfMNew, const int *eleIdx,
                              const int qpStart, const int qpEnd, const int qpidx,
                              double *vec_a, double *vec_b);
void UpdateMAllTwo(const int ma, const int s, const int mb, const int t,
                   const int raOld, const int rbOld,
                   const int *eleIdx, const int qpStart, const int qpEnd);
void updateMAllTwo_child(const int ma, const int s, const int mb, const int t,
                         const int raOld, const int rbOld,
                         const int *eleIdx, const int qpStart, const int qpEnd, const int qpidx,
                         double *vecP, double *vecQ, double *vecS, double *vecT);

double update_norm1(const int ma, const int s, const int mb, const int t,
		const int *eleIdx, const int qpStart, const int qpEnd, const int qpidx) ;

#endif /* SRC_PFUPDATE_TWO_H_ */
