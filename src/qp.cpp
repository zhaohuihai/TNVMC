/*-------------------------------------------------------------
 * Variational Monte Carlo
 * Quantum Projection
 *-------------------------------------------------------------
 * by Satoshi Morita and Hui-Hai Zhao
 *-------------------------------------------------------------*/

#include <vector>

#include "vmcmain.h"

#include "workspace.h"
#include "gauleg.h"
#include "legendrepoly.h"
#include "projection.h"

#include "qp.h"

using namespace std ;

/* calculate SPGLCos, SPGLSin and QPFullWeight */
void InitQPWeight() {
  int i,j,idx;
  double *beta;
  double *weight;
  double w;

  RequestWorkSpaceDouble(2*NSPGaussLeg);
  beta   = GetWorkSpaceDouble(NSPGaussLeg);
  weight = GetWorkSpaceDouble(NSPGaussLeg);

  if(NSPGaussLeg==1) {
    beta[0] = 0.0;
    weight[0] = 1.0;
    SPGLCos[0] = 1.0;
    SPGLSin[0] = 0.0;
    SPGLCosSin[0] = 0.0;
    SPGLCosCos[0] = 1.0;
    SPGLSinSin[0] = 0.0;

    for(j=0;j<NMPTrans;j++) {
      QPFixWeight[j] = ParaQPTrans[j]; // [double1]
    }

  } else {
    GaussLeg(0, M_PI, beta, weight, NSPGaussLeg);

    #pragma omp parallel for default(shared) private(i,j,w,idx)
    for(i=0;i<NSPGaussLeg;i++) {
      SPGLCos[i] = cos(0.5 * beta[i]);
      SPGLSin[i] = sin(0.5 * beta[i]);
      SPGLCosSin[i] = SPGLCos[i]*SPGLSin[i];
      SPGLCosCos[i] = SPGLCos[i]*SPGLCos[i];
      SPGLSinSin[i] = SPGLSin[i]*SPGLSin[i];
      
      w = 0.5*sin(beta[i])*weight[i]*LegendrePoly(cos(beta[i]), NSPStot);
      
      for(j=0;j<NMPTrans;j++) {
        idx = i + j*NSPGaussLeg;
        QPFixWeight[idx] = w * ParaQPTrans[j];
      }
    }
  }

  UpdateQPWeight();

  ReleaseWorkSpaceDouble();
  return;
}

/* Calculate logarithm of inner product <phi|L|x> */
double CalculateLogIP(double * const pfM, const int qpStart, const int qpEnd, MPI_Comm comm)
{
  const int qpNum = qpEnd-qpStart;
  double ip=0.0, ip2;
  int qpidx;
  int size;
  MPI_Comm_size(comm,&size);

  #pragma loop noalias
  for(qpidx=0;qpidx<qpNum;qpidx++) {
    ip += QPFullWeight[qpidx+qpStart] * pfM[qpidx];
  }
  if(size>1) {
    MPI_Allreduce(&ip, &ip2, 1, MPI_DOUBLE, MPI_SUM, comm);
    ip = ip2;
  }

//  cout << ip << endl ;
//  exit(0) ;

  return log(fabs(ip));
}

double CalculateLogWeight(double * const pfM, const int *eleNum, FatTreeTensorNetwork& fttn,
		const int qpStart, const int qpEnd, MPI_Comm comm) {
	const int qpNum = qpEnd-qpStart;
  int size;
  MPI_Comm_size(comm,&size);

  // calculate weights of correlation factors and FTTN
  vector<int> projCnt(NProj, 1.0) ;
  vector<double> ProjWeights(NMPTrans, 1.0) ;
  vector<double> FTTNweights(NMPTrans, 1.0) ;
  vector<int> eleNum_shift(2*Nsite, -1) ;
  for ( int i = 0 ; i < NMPTrans; i ++ ) {
  	int * mqp = QPTrans[i] ;
  	eleNum_shift = createSftEleNum(mqp, eleNum) ;
  	MakeProjCnt(&(projCnt[0]), &(eleNum_shift[0])) ;
  	ProjWeights.at(i) = ProjVal(&(projCnt[0])) ;

  	FTTNweights.at(i) = fttn.compute_weight(eleNum_shift) ;
  }

  double ip=0.0, ip2;
  for(int qpidx=0;qpidx<qpNum;qpidx++) {
  	int mpidx = (qpidx + qpStart) / NSPGaussLeg ; // index for momentum projection
  	ip += QPFullWeight[qpidx + qpStart] * pfM[qpidx] * ProjWeights.at(mpidx) * FTTNweights.at(mpidx) ;
  }
  if(size>1) {
    MPI_Allreduce(&ip, &ip2, 1, MPI_DOUBLE, MPI_SUM, comm);
    ip = ip2;
  }
  return log(fabs(ip));
}

double CalculateLogWeight_symCorFactor(const double logIp, const int *eleNum, FatTreeTensorNetwork& fttn,
		const int qpStart, const int qpEnd, MPI_Comm comm) {
	const int qpNum = qpEnd-qpStart;
  int size;
  MPI_Comm_size(comm,&size);

  double ip=0.0, ip2;
  // calculate weights of correlation factors and FTTN
  vector<int> projCnt(NProj, 1.0) ;
  double ProjWeight ;
  double FTTNweight ;
  vector<int> eleNum_shift(2*Nsite, -1) ;
  for ( int i = 0 ; i < NMPTrans; i ++ ) {
  	int * mqp = QPTrans[i] ;
  	eleNum_shift = createSftEleNum(mqp, eleNum) ;
  	MakeProjCnt(&(projCnt[0]), &(eleNum_shift[0])) ;
  	ProjWeight = ProjVal(&(projCnt[0])) ;
  	FTTNweight = fttn.compute_weight(eleNum_shift) ;
  	ip += exp(logIp) * ProjWeight * FTTNweight ;
  }
  if(size>1) {
    MPI_Allreduce(&ip, &ip2, 1, MPI_DOUBLE, MPI_SUM, comm);
    ip = ip2;
  }
  return log(fabs(ip));
}

/* Calculate inner product <phi|L|x> */
double CalculateIP(double * const pfM, const int qpStart, const int qpEnd, MPI_Comm comm) {
  const int qpNum = qpEnd-qpStart;
  double ip=0.0, ip2;
  int qpidx;
  int size;
  MPI_Comm_size(comm,&size);

  #pragma loop noalias
  for(qpidx=0;qpidx<qpNum;qpidx++) {
    ip += QPFullWeight[qpidx+qpStart] * pfM[qpidx];
  }
  if(size>1) {
    MPI_Allreduce(&ip, &ip2, 1, MPI_DOUBLE, MPI_SUM, comm);
    ip = ip2;
  }
  return ip;
}

double CalculateWeight(double * const pfM, const int *eleNum, FatTreeTensorNetwork& fttn,
		const int qpStart, const int qpEnd, MPI_Comm comm) {
	const int qpNum = qpEnd-qpStart;
  int size;
  MPI_Comm_size(comm,&size);

  // calculate weights of correlation factors and FTTN
  vector<int> projCnt(NProj, 1.0) ;
  vector<double> ProjWeights(NMPTrans, 1.0) ;
  vector<double> FTTNweights(NMPTrans, 1.0) ;
  vector<int> eleNum_shift(2*Nsite, -1) ;
  for ( int i = 0 ; i < NMPTrans; i ++ ) {
  	int * mqp = QPTrans[i] ;
  	eleNum_shift = createSftEleNum(mqp, eleNum) ;
  	MakeProjCnt(&(projCnt[0]), &(eleNum_shift[0])) ;
  	ProjWeights.at(i) = ProjVal(&(projCnt[0])) ;

  	FTTNweights.at(i) = fttn.compute_weight(eleNum_shift) ;
  }

  double ip=0.0, ip2;
  for(int qpidx=0;qpidx<qpNum;qpidx++) {
  	int mpidx = (qpidx + qpStart) / NSPGaussLeg ; // index for momentum projection
  	ip += QPFullWeight[qpidx + qpStart] * pfM[qpidx] * ProjWeights.at(mpidx) * FTTNweights.at(mpidx) ;
  }
  if(size>1) {
    MPI_Allreduce(&ip, &ip2, 1, MPI_DOUBLE, MPI_SUM, comm);
    ip = ip2;
  }
  return ip ;
}

double CalculateWeight_symCorFactor(const double pfa, const int *eleNum, FatTreeTensorNetwork& fttn,
		const int qpStart, const int qpEnd, MPI_Comm comm) {
	const int qpNum = qpEnd-qpStart;
	int size ;
	MPI_Comm_size(comm,&size);

	double ip=0.0, ip2;
  // calculate weights of correlation factors and FTTN
  vector<int> projCnt(NProj, 1.0) ;
  double ProjWeight ;
  double FTTNweight ;
  vector<int> eleNum_shift(2*Nsite, -1) ;
  for ( int i = 0 ; i < NMPTrans; i ++ ) {
  	int * mqp = QPTrans[i] ;
  	eleNum_shift = createSftEleNum(mqp, eleNum) ;
  	MakeProjCnt(&(projCnt[0]), &(eleNum_shift[0])) ;
  	ProjWeight = ProjVal(&(projCnt[0])) ;
  	FTTNweight = fttn.compute_weight(eleNum_shift) ;
  	ip += pfa * ProjWeight * FTTNweight ;
  }
  if(size>1) {
    MPI_Allreduce(&ip, &ip2, 1, MPI_DOUBLE, MPI_SUM, comm);
    ip = ip2;
  }
  return ip;
}

std::vector<int> createSftEleNum(const int* mqp, const int* eleNum) {
	vector<int> eleNum_shift(2*Nsite, -1) ;
	int siteNo_old, siteNo_new ;
	for ( int j = 0; j < Nsite; j ++ ) {
		siteNo_new = j ;
		siteNo_old = mqp[j] ;
//		siteNo_new = mqp[j] ;
//		siteNo_old = j ;

		eleNum_shift.at(siteNo_new) = eleNum[siteNo_old] ; // up spin
		eleNum_shift.at(siteNo_new + Nsite) = eleNum[siteNo_old + Nsite] ; // down spin
	}
	return eleNum_shift ;
}

void UpdateQPWeight() {
  int i,j,offset;
  double tmp;

  if(FlagOptTrans>0) {
    for(i=0;i<NOptTrans;i++) {
      offset = i*NQPFix;
      tmp = OptTrans[i];
      for(j=0;j<NQPFix;j++) {
        QPFullWeight[offset+j] = tmp * QPFixWeight[j];
      }
    }
  } else {
    for(j=0;j<NQPFix;j++) {
      QPFullWeight[j] = QPFixWeight[j];
    }
  }

  return;
}
