/*
 * fat_tree_tensor_network.h
 *
 *  Created on: 2016年2月25日
 *      Author: zhaohuihai
 */

#ifndef SRC_FAT_TREE_TENSOR_NETWORK_H_
#define SRC_FAT_TREE_TENSOR_NETWORK_H_

#include <vector>
#include "tree_node.h"
// represent the wave function by fat tree tensor network
// height depth
//    2     0               o
//                         / \
//                        /   \
//                       /     \
//    1     1           o       o
//                     / \     / \
//    0     2         o   o   o   o
//                   / \ / \ / \ / \
//                  o   o   o   o
class FatTreeTensorNetwork
{
private:
	int _FTTN_inNodeRank ; // rank of internal nodes
	int _FTTN_NleafSites ; // number of sites in every leaf node
	bool _countSign ;
	int _length_x ;
	int _length_y ;
	int _unitCell_x ;
	int _unitCell_y ;
	int _latSymProj_x ; // x direction of lattice symmetry projection
	int _latSymProj_y ; // y direction of lattice symmetry projection
	int _rotSymProj ; // rotational symmetry projection

	int _nsites ;  // number of sites in the lattice
	int _nleaves ; // number of leaf nodes
	int _nInterNodes ; // number of internal nodes
	int _nnodes ; // total number of nodes
	std::vector<int> _nnnodes_height ; // number of nodes on each height of tree
	int _treeHeight ; // Height of tree
	int _siteDim ; // dim of physical index
	int _bondDim ; // dim of virtual bond
	int _numel ;   // number of total elements
	double* _elem ; // pointer to the first element of the FTTN
	std::vector<bool> _ti ; // whether a node is a translated image of another node
	std::vector<int> _firstCell ; // first unit cell nodes
	std::vector<int> _tiNode ; // if a translated image, the Node defining the image's parent
	std::vector<int> _parent ; // parent of each node
	std::vector< std::vector<int> >  _children ; // children of each node
	std::vector<TreeNode> _nodes ;

	std::vector< int > _eleNum ; //
	std::vector< int > _new_eleNum ;

	std::vector< int > _offsets ; // positions of each Node variables in the ordering of all FTTN variables

//	std::vector< std::vector< std::vector<double> > > _parentEnvs ;
//	std::vector< std::vector< std::vector<double> > > _leftEnvs ;
//	std::vector< std::vector< std::vector<double> > > _rightEnvs ;
	std::vector< std::vector< std::vector<double> > > _branches ;


//	std::vector< std::vector< std::vector<double> > > _new_parentEnvs ;
//	std::vector< std::vector< std::vector<double> > > _new_leftEnvs ;
//	std::vector< std::vector< std::vector<double> > > _new_rightEnvs ;
	std::vector< std::vector< std::vector<double> > > _new_branches ;

	std::vector< std::vector< int > > _changed_branches_idx ;

	double _weight ;
	double _new_weight ;

	int count_nnodes(const int length_x, const int length_y, int& treeHeight) ;
	void set_parent_children() ;
	void set_ti() ;
	void set_ti_3() ; // set translational invariant nodes
	void set_ti_5() ; // set translational invariant nodes
	void createNodes(double* &val, int &nval) ;
	void createNodes_3(double* &val, int &nval) ;
	void createNodes_5(double* &val, int &nval) ;
	std::vector<int> createSiteVec(int firstSite) ;
	std::vector<int> createPlaquetteSiteVec(int firstSite) ;
	std::vector<int> createCrossSiteVec(int firstSite) ;

	std::vector<int> createSftEleNum(const int* eleNum,
			const int sft_x, const int sft_y, const int irot) ;
	int createSftSite(const int ri, const int sft_x, const int sft_y, const int irot) ;
	std::vector<int> createSftSites(const std::vector<int>& old_sites,
			const int sft_x, const int sft_y, const int irot) ;

	int getPhysInd(const int* eleNum, const int i) ;
public:
	FatTreeTensorNetwork(const int FTTN_inNodeRank, const bool countSign,
											 const int length_x, const int length_y,
											 const int unitCell_x, const int unitCell_y,
											 const int latSymProj_x, const int latSymProj_y, const int rotSymProj,
											 const int siteDim, const int bondDim, double* elem) ;
	FatTreeTensorNetwork(const int FTTN_inNodeRank, const int FTTN_NleafSites, const bool countSign,
											 const int length_x, const int length_y,
											 const int unitCell_x, const int unitCell_y,
											 const int latSymProj_x, const int latSymProj_y, const int rotSymProj,
											 const int siteDim, const int bondDim, double* elem) ;

	void initialize(const int *eleNum) ;
	void init_env(const std::vector<int>& eleNum, std::vector< std::vector<double> >& parentEnv,
			 std::vector< std::vector<double> >& leftEnv, std::vector< std::vector<double> >& rightEnv) ;
	void init_branch(const std::vector<int>& eleNum, std::vector< std::vector<double> >& branch) ;
//	void initialize_physCal(const int *eleNum) ;
	void init_env_physCal(const std::vector<int>& eleNum, std::vector< std::vector<double> >& parentEnv,
			 std::vector< std::vector<double> >& leftEnv, std::vector< std::vector<double> >& rightEnv) ;
	void clear_new_branches() ;

	void update() ;
	int nnodes() const { return _nnodes ; }
	int nleaves() const { return _nleaves ; }
	int nInterNodes() const { return _nInterNodes ; }
	std::vector<bool> ti() const { return _ti ; }
	std::vector<int> firstCell() const { return _firstCell; }
	std::vector<TreeNode> nodes() const { return _nodes ; }
	void assign(int nodeNo, int dimFile, std::vector<double>& nodeFile) ;
	//---------------------------------------------------------------
	double compute_weight(const int* eleNum) ;
	double compute_weight(const std::vector<int>& eleNum) ;
	double compute_weight_LxL(const std::vector<int>& eleNum) ;

	double recompute_weight(const std::vector<int>& new_eleNum, const std::vector<int>& changed_sites) ;
	double recompute_weight_LxL(const std::vector<int>& new_eleNum,
			const std::vector<int>& changed_sites,
			std::vector< std::vector<double> >& leftEnv, std::vector< std::vector<double> >& rightEnv,
			std::vector< std::vector<double> >& new_leftEnv, std::vector< std::vector<double> >& new_rightEnv) ;
	double recompute_weight_LxL_fromLeaf(const std::vector<int>& new_eleNum,
			const std::vector<int>& changed_sites,
			std::vector< std::vector<double> >& branch, std::vector< std::vector<double> >& new_branch) ;
	double recompute_weight_LxL_fromLeaf(const std::vector<int>& new_eleNum,
			const std::vector<int>& changed_sites,
			std::vector< std::vector<double> >& branch, std::vector< std::vector<double> >& new_branch,
			std::vector< int >& changed_branch_idx) ;

	std::vector<double> contractBranch(const std::vector<int>& eleNum, int subRootNo) ;
	std::vector<double> contractBranch(const std::vector<int>& eleNum, int subRootNo,
			std::vector< std::vector<double> >& parentEnv,
			std::vector< std::vector<double> >& leftEnv, std::vector< std::vector<double> >& rightEnv) ;

	void computeParentEnvs(int subRootNo, std::vector< std::vector<double> >& parentEnv,
			std::vector< std::vector<double> >& leftEnv, std::vector< std::vector<double> >& rightEnv) ;

	std::vector<double> recontractBranch(const std::vector<int>& new_eleNum, int subRootNo,
			const std::vector<int>& changed_sites, bool& needRecal,
			std::vector< std::vector<double> >& leftEnv, std::vector< std::vector<double> >& rightEnv,
			std::vector< std::vector<double> >& new_leftEnv, std::vector< std::vector<double> >& new_rightEnv) ;

	double weight() const { return _weight ; }
	double computeSign(const int ri, const int rj, const int s) ; // fermion sign
	// compute <x'|fttn> / <x|fttn>
	double computeRatio(const int ri, const int rj, const int s) ;
	double computeRatio(const int ri, const int rj, const int s,
											const int rk, const int rl, const int t) ;
	// compute log(<x'|SBS> / <x|SBS>)
	double computeLogRatio(const int ri, const int rj, const int s) ;
	double computeLogRatio(const int ri, const int rj, const int s,
												 const int rk, const int rl, const int t) ;

	void derivative(double *der, const int* eleNum) ;
	void derivative(double *der, const int* eleNum, const double weight) ;
	void derivative_LxL(double *der, const std::vector<int>& eleNum) ;
	void derivative_LxL(double *der, const std::vector<int>& eleNum,
			std::vector< std::vector<double> >& parentEnv,
			std::vector< std::vector<double> >& leftEnv, std::vector< std::vector<double> >& rightEnv) ;

	std::vector<double> contractBranchEnv(const std::vector<int>& eleNum, int branchRootNo) ;
	std::vector<double> computeEnvLeft(std::vector<double>& N,  std::vector<double>& tRight) ;
	std::vector<double> computeEnvRight(std::vector<double>& N,  std::vector<double>& tLeft) ;

	std::vector<double> computeBranchRoot(std::vector<double>& E, std::vector<double>& N) ;
	void leafDerivative(double *der, const int* eleNum,
											TreeNode& leafNode, std::vector<double>& E) ;
	void leafDerivative(double *der, const std::vector<int>& eleNum,
			                TreeNode& leafNode, std::vector<double>& E) ;
	void interNodeDerivative(double *der, std::vector<double>& E16,
			std::vector<double>& lf0, std::vector<double>& lf1,std::vector<double>& lf4, std::vector<double>& lf5,
			int offset) ;
	void interNodeDerivative(double *der, std::vector<double>& E16,
			std::vector<double>& lf0, std::vector<double>& lf1, int offset) ;
	void rootNodeDerivative(double *der,
			std::vector<double>& t16, std::vector<double>& t17,std::vector<double>& t18, std::vector<double>& t19,
			int offset) ;
	void rootNodeDerivative(double *der, std::vector<double>& t28, std::vector<double>& t29, int offset) ;
	//---------------------------------------------------------------
	void rescale(double maxElem) ;
	void info() ;
	void disp() ;
	void dispVec(std::vector<int> vec) ;
};



#endif /* SRC_FAT_TREE_TENSOR_NETWORK_H_ */
