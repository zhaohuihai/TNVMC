/*
 * fat_tree_tensor_network.cpp
 *
 *  Created on: 2016-2-26
 *      Author: zhaohuihai
 */

#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <assert.h>
#include <mpi.h>
#include "vmctime.h"
#include "fat_tree_tensor_network.h"

using namespace std ;

FatTreeTensorNetwork::FatTreeTensorNetwork(
		const int FTTN_inNodeRank, const bool countSign,
		const int length_x, const int length_y,
		const int unitCell_x, const int unitCell_y,
		const int latSymProj_x, const int latSymProj_y, const int rotSymProj,
		const int siteDim, const int bondDim, double* elem)
{
	_FTTN_inNodeRank = FTTN_inNodeRank ; // rank 3 or 5
	_countSign = countSign ;
	_length_x = length_x ;
	_length_y = length_y ;
	_unitCell_x = unitCell_x ;
	_unitCell_y = unitCell_y ;
	_latSymProj_x = latSymProj_x ;
	_latSymProj_y = latSymProj_y ;
	_rotSymProj = rotSymProj ;
	//---------------------------------
	_nsites = _length_x * _length_y ;
	_siteDim = siteDim ;
	_bondDim = bondDim ;
	_nnodes = count_nnodes(length_x, length_y, _treeHeight) ;
	_nInterNodes = _nnodes - _nleaves - 1 ;
	set_parent_children() ;

	set_ti() ;
	_nodes.clear() ;
	double* val = elem ;
	int nval = 0 ; // count the total number of variational parameters
	createNodes(val, nval) ;
	_numel = nval ;
	_elem = elem ;
}

FatTreeTensorNetwork::FatTreeTensorNetwork(
		const int FTTN_inNodeRank, const int FTTN_NleafSites, const bool countSign,
		const int length_x, const int length_y,
		const int unitCell_x, const int unitCell_y,
		const int latSymProj_x, const int latSymProj_y, const int rotSymProj,
		const int siteDim, const int bondDim, double* elem) {
	_FTTN_inNodeRank = FTTN_inNodeRank ; // rank 3 or 5
	_FTTN_NleafSites = FTTN_NleafSites ;
	_countSign = countSign ;
	_length_x = length_x ;
	_length_y = length_y ;
	_unitCell_x = unitCell_x ;
	_unitCell_y = unitCell_y ;
	_latSymProj_x = latSymProj_x ;
	_latSymProj_y = latSymProj_y ;
	_rotSymProj = rotSymProj ;
	//---------------------------------
	_nsites = _length_x * _length_y ;
	_siteDim = siteDim ;
	_bondDim = bondDim ;
	_nnodes = count_nnodes(length_x, length_y, _treeHeight) ;
	_nInterNodes = _nnodes - _nleaves - 1 ;
	set_parent_children() ;

	set_ti() ;
	_nodes.clear() ;
	double* val = elem ;
	int nval = 0 ; // count the total number of variational parameters
	createNodes(val, nval) ;
	_numel = nval ;
	_elem = elem ;
}

int FatTreeTensorNetwork::count_nnodes(const int length_x, const int length_y,
		int& treeHeight) {
	int r = _FTTN_inNodeRank - 1 ; // reduce factor
	treeHeight = 0 ;
	_nnnodes_height.clear() ;

	int lx, ly ;
	if ( length_x / 2 == 1 ) lx = 1 ;
	else lx = length_x ;
	if ( length_y / 2 == 1 ) ly = 1 ;
	else ly = length_y ;
	_nleaves = lx * ly ;
	int nnodes_eachLevel = _nleaves ; // number of nodes at leaf level
	_nnnodes_height.push_back(nnodes_eachLevel) ;
	int n = 0 ;
	while ( nnodes_eachLevel >= 1 ) {
		n += nnodes_eachLevel ;
		treeHeight ++ ;

		nnodes_eachLevel /= r ;
		_nnnodes_height.push_back(nnodes_eachLevel) ;
	}
	//*****************debug***********************
//	for ( int i = 0; i < treeHeight; i ++ ) {
//		cout << "height: " << i << ": " << _nnnodes_height.at(i) << endl ;
//	}
	//*********************************************
	return n ;
}

void FatTreeTensorNetwork::set_parent_children() {
	int nnodes_eachLevel = _nleaves ; // number of nodes at leaf level
	int lx = _length_x ; // higher layer x
	int ly = _length_y ; //

	_parent.assign(_nnodes, -1) ;
	_children.resize(_nnodes) ;
	int n_offset = 0 ; // number of nodes below level i
	int xc, yc, xp, yp ;
	int lxp, lyp ; // lattice size of parent level: level (i + 1)
	for (int i = 0; i < _treeHeight-1; i ++) { //
		if ( i % 2 == 0 ) {
			lxp = lx / 2 ;
			lyp = ly ;
		} else {
			lxp = lx ;
			lyp = ly / 2 ;
		}
		for ( int j = 0; j < _nnnodes_height.at(i); j ++ ) {
			// coordinate of child node at level i
			xc = j % lx ;
			yc = j / lx ;

			// find the coordinate of parent node at level (i + 1)
			if ( i % 2 == 0 ){ // even level: coarse-graining along x direction
				xp = xc / 2 ;
				yp = yc ;
			} else { // odd level: coarse-graining along y direction
				xp = xc ;
				yp = yc / 2 ;
			}
			// node number
			int n = n_offset + j ;
			// parent node number
			int np = n_offset + _nnnodes_height.at(i) + (xp + yp * lxp) ;
			_parent.at(n) = np ;
//			cout << "node " << n << ", parent node: " << _parent.at(n) << endl ;
		}
//		cout << endl ;
		n_offset += _nnnodes_height.at(i) ;
		if ( i % 2 == 0 ) lx /= 2 ;
		else ly /= 2 ;
	}

	// find children of each node except leaf nodes
	for ( int i = 0; i < _nnodes; i ++ ) { // loop for all possible parent nodes
		vector<int> cNodes;
		cNodes.clear() ;
		for ( int j = 0; j <  _nnodes; j ++ ) { // find children of node i
			if ( _parent.at(j) == i ) cNodes.push_back(j) ;
		}
		_children.at(i) = cNodes ;
	}
	//**********************debug********************************
//	for ( int i = 0 ; i < _nnodes; i ++ ) {
//		cout << "node " << i << " children: ";
//		for ( int j = 0; j < _children.at(i).size(); j ++ ) {
//			cout << _children.at(i).at(j) << ", " ;
//		}
//		cout << endl ;
//	}

}

void FatTreeTensorNetwork::set_ti() {
	if ( _FTTN_inNodeRank == 3 ) set_ti_3() ;
	else if ( _FTTN_inNodeRank == 5 ) set_ti_5() ;
}

void FatTreeTensorNetwork::set_ti_3() {
	_ti.assign(size_t(_nnodes), true) ; //
	_tiNode.assign(size_t(_nnodes), -1) ;
	_firstCell.clear() ;

	int lx, ly, uclx, ucly ;
	if ( _length_x / 2 == 1 ) {
		lx = 1 ;
		uclx = 1 ;
	}
	else {
		lx = _length_x ;
		uclx = _unitCell_x ;
	}
	if ( _length_y / 2 == 1 ) {
		ly = 1 ;
		ucly = 1 ;
	}
	else {
		ly = _length_y ;
		ucly = _unitCell_y ;
	}
	//-------------------------------------------------------------------------
	int nUCsites ; // number of sites in a unit cell
	int n_offset = 0 ;
	for ( int i = 0; i < _treeHeight; i ++ ) {
		nUCsites = uclx * ucly ;
		for ( int j = 0 ; j < nUCsites; j ++ ) {
			int x = j % uclx ;
			int y = j / uclx ;
			int n = x + y * lx + n_offset ; // node number
			_ti[n] = false ; // false for first unit cell node
			_firstCell.push_back(n) ;
		}
//		cout << "site number of level " << i << ": " << lx * ly << endl ;
		n_offset += lx * ly ;

		if ( i % 2 == 0 ) { // coarse-grain along x direction
			uclx = max(uclx / 2, 1) ;
			lx = max(lx / 2, 1) ;
		} else if ( i % 2 == 1) { // coarse-grain along y direction
			ucly = max(ucly / 2, 1) ;
			ly = max(ly / 2, 1) ;
		}
	}
	//=========================================================================
	if ( _length_x / 2 == 1 ) {
		lx = 1 ;
		uclx = 1 ;
	}
	else {
		lx = _length_x ;
		uclx = _unitCell_x ;
	}
	if ( _length_y / 2 == 1 ) {
		ly = 1 ;
		ucly = 1 ;
	}
	else {
		ly = _length_y ;
		ucly = _unitCell_y ;
	}
	n_offset = 0 ;
	int uc_offset = 0 ;
	for ( int i = 0; i < _treeHeight; i ++ ) {
		for( int iy = 0; iy < ly; iy ++ ) {
			for ( int ix = 0; ix < lx; ix ++ ) {
				int n = ix + iy * lx + n_offset ; // node number
				if ( _ti[n] == true ) { // ti sites
					int ux = ix % uclx ;
					int uy = iy % ucly ;
					_tiNode[n] = _firstCell[ux + uy * uclx + uc_offset] ;
				}
			}
		}
		n_offset += lx * ly ;
		uc_offset += uclx * ucly ;

		if ( i % 2 == 0 ) { // coarse-grain along x direction
			uclx = max(uclx / 2, 1) ;
			lx = max(lx / 2, 1) ;
		} else if ( i % 2 == 1) { // coarse-grain along y direction
			ucly = max(ucly / 2, 1) ;
			ly = max(ly / 2, 1) ;
		}
	}
	//**********************************************************
//	for ( int i = 0; i < _nnodes; i ++ ) {
//		cout << i << "-th node, " ;
//		cout << _ti[i] << ", " ;
//		cout << _tiNode[i] << endl ;
//	}
//	for ( int i = 0; i < _firstCell.size(); i ++ ) {
//		cout << _firstCell[i] << endl ;
//	}
//	exit(0) ;
}

void FatTreeTensorNetwork::set_ti_5() {
	_ti.assign(size_t(_nnodes), true) ; //
	_tiNode.assign(size_t(_nnodes), -1) ;
	_firstCell.clear() ;

	int lx = _length_x ;
	int ly = _length_y ;
	int uclx = _unitCell_x ;
	int ucly = _unitCell_y ;
	int nUCsites = _unitCell_x * _unitCell_y ; // number of sites in a unit cell
	int n0 = 0 ; // number of nodes counter
	for ( int i = 0; i < _treeHeight; i ++ ) {
		for ( int j = 0 ; j < nUCsites; j ++ ) { // loop to find nodes in the first unit cell
			int x = j % uclx ;
			int y = j / uclx ;
			int n = x + y * lx + n0 ; // node number
			_ti[n] = false ; // false for first unit cell node
			_firstCell.push_back(n) ;
		}
		n0 += lx*ly ;
		if ( uclx > 1 ) uclx /= 2 ;
		if ( ucly > 1 ) ucly /= 2 ;
		nUCsites = uclx * ucly ;
		lx /= 2 ;
		ly /= 2 ;
	}
	//---------------------------------------------------------
	lx = _length_x ;
	ly = _length_y ;
	uclx = _unitCell_x ;
	ucly = _unitCell_y ;
	n0 = 0 ;
	int ucn0 = 0 ;
	for ( int i = 0; i < _treeHeight; i ++ ) {
		// at every height of tree
		for( int iy = 0; iy < ly; iy ++ ) {
			for ( int ix = 0; ix < lx; ix ++ ) {
				int n = ix + iy * lx + n0 ; // node number
				if ( _ti[n] == true ) { // ti sites
					int ux = ix % uclx ;
					int uy = iy % ucly ;
					_tiNode[n] = _firstCell[ux + uy * uclx + ucn0] ;
				}
			}
		}
		n0 += lx * ly ;
		ucn0 += uclx * ucly ;
		if ( uclx > 1 ) uclx /= 2 ;
		if ( ucly > 1 ) ucly /= 2 ;
		lx /= 2 ;
		ly /= 2 ;
	}
	//************************debug*******************************
//	for ( int i = 0; i < _nnodes; i ++ ) {
//		cout << _ti[i] << ", " ;
//		cout << _tiNode[i] << endl ;
//	}
//	exit(0) ;
}

void FatTreeTensorNetwork::createNodes(double* &val, int &nval) {
	if ( _FTTN_inNodeRank == 3 ) createNodes_3(val, nval) ;
	else if ( _FTTN_inNodeRank == 5 ) createNodes_5(val , nval) ;
}

void FatTreeTensorNetwork::createNodes_3(double* &val, int &nval) {
	int nelem_node ;
	double* val0 = val ; // the pointer to the 1st element of parameters
	_nodes.resize(_nnodes) ;
	int lx, ly, uclx, ucly ;
	if ( _length_x / 2 == 1 ) {
		lx = 1 ;
		uclx = 1 ;
	}
	else {
		lx = _length_x ;
		uclx = _unitCell_x ;
	}
	if ( _length_y / 2 == 1 ) {
		ly = 1 ;
		ucly = 1 ;
	}
	else {
		ly = _length_y ;
		ucly = _unitCell_y ;
	}

	int nUCsites = uclx * ucly ; // number of sites in a unit cell
	int offset = 0 ;
	// create leaf nodes
	for ( int i = 0; i < nUCsites; i ++ ) { // first unit cell at leaf level
		int nodeNo = _firstCell.at(i) ; // At leaf level, the node No. is the first site of each leaf
		vector<int> leafNodeSites = createSiteVec(nodeNo) ;
		_nodes.at(nodeNo) = TreeNode(_nsites, leafNodeSites, _siteDim, _bondDim, _parent.at(nodeNo), val, offset) ;
		nelem_node = _nodes.at(nodeNo).nelements() ;
		offset += nelem_node ;
		val += nelem_node ;
		nval += nelem_node ;
	}
	double* ti_val ;
	int ti_offset ;
	for ( int i = 0; i < (lx * ly); i ++ ) {
		if ( _ti[i] == true ) { // translational invariant node
			vector<int> leafNodeSites = createSiteVec(i) ;
			int n = _tiNode.at(i) ;
			ti_offset = _nodes.at(n).offset() ;
			ti_val = _nodes.at(n).elem() ;
			_nodes.at(i) = TreeNode(_nsites, leafNodeSites, _siteDim, _bondDim, _parent.at(i), ti_val, ti_offset) ;
		}
	}
	//------------------------------------------------------------------------------------------
	// create internal nodes
	for ( int i = nUCsites; i < (_firstCell.size() - 1); i ++ ) {
		int nodeNo = _firstCell.at(i) ;
		_nodes.at(nodeNo) = TreeNode(_bondDim, _parent.at(nodeNo), _children.at(nodeNo), val, offset) ;
		nelem_node = _nodes.at(nodeNo).nelements() ;
		offset += nelem_node ;
		val += nelem_node ;
		nval += nelem_node ;
	}
	for ( int i = (lx * ly); i < _nnodes-1; i ++ ) {
		if ( _ti[i] == true ) { // translational invariant node
			int n = _tiNode.at(i) ; // node number
			ti_offset = _nodes.at(n).offset() ;
			ti_val = _nodes.at(n).elem() ;
			_nodes.at(i) = TreeNode(_bondDim, _parent.at(i), _children.at(i), ti_val, ti_offset) ;
		}
	}
	//------------------------------------------------------------------------------------------
	// create root node
	int nodeNo = _firstCell.at(_firstCell.size() - 1) ;
	_nodes.at(nodeNo) = TreeNode(_bondDim,  _parent.at(nodeNo), _children.at(nodeNo), val, offset) ;
	nelem_node = _nodes.at(nodeNo).nelements() ;
	offset += nelem_node ;
	val += nelem_node ;
	nval += nelem_node ;
	//**************************debug**********************************
//	for ( int i = 0 ; i < (lx * ly); i ++ ) {
//		for ( int j = 0 ; j < _nodes.at(i).size() ; j ++)
//			cout << _nodes.at(i).sites().at(j) << ", " ;
//		cout << endl ;
//		if ( _ti[i] == true ) cout << "ti nodes of first site: " << _tiNode.at(i) << endl ;
//		std::cout << _nodes.at(i).elem() - val0 << std::endl << endl ;
//	}
//	cout << nval << endl ;
//	exit(0) ;
//	for ( int i = 0; i < _nodes.size(); i ++ ) {
//		_nodes.at(i).info() ;
//	}
}

void FatTreeTensorNetwork::createNodes_5(double* &val, int &nval) {
	int nelem_node ;
	double* val0 = val ; // the pointer to the 1st element of parameters
	_nodes.resize(_nnodes) ;
	int uclx = _unitCell_x ;
	int ucly = _unitCell_y ;
	int nUCsites = _unitCell_x * _unitCell_y ; // number of sites in a unit cell
	int offset = 0 ;
	// create leaf nodes
	for ( int i = 0; i < nUCsites; i ++ ) { // first unit cell at leaf level
		int nodeNo = _firstCell.at(i) ; // At leaf level, the node No. is the first site of each leaf
		vector<int> leafNodeSites = createSiteVec(nodeNo) ;
		_nodes.at(nodeNo) = TreeNode(_nsites, leafNodeSites, _siteDim, _bondDim, val, offset) ;
		nelem_node = _nodes.at(nodeNo).nelements() ;
		offset += nelem_node ;
		val += nelem_node ;
		nval += nelem_node ;
	}
	double* ti_val ;
	int ti_offset ;
	for ( int i = 0; i < _nsites; i ++ ) {
		if ( _ti[i] == true ) { // translational invariant node
			vector<int> leafNodeSites = createSiteVec(i) ;
			int n = _tiNode.at(i) ;
			ti_offset = _nodes.at(n).offset() ;
			ti_val = _nodes.at(n).elem() ;
			_nodes.at(i) = TreeNode(_nsites, leafNodeSites, _siteDim, _bondDim, ti_val, ti_offset) ;
		}
	}
	// create internal nodes
	for ( int i = nUCsites; i < (_firstCell.size() - 1); i ++ ) {
		int nodeNo = _firstCell.at(i) ;
		_nodes.at(nodeNo) = TreeNode(_bondDim, val, offset, 5) ;
		nelem_node = _nodes.at(nodeNo).nelements() ;
		offset += nelem_node ;
		val += nelem_node ;
		nval += nelem_node ;
	}
	for ( int i = _nsites; i < _nnodes-1; i ++ ) {
		if ( _ti[i] == true ) { // translational invariant node
			int n = _tiNode.at(i) ; // node number
			ti_offset = _nodes.at(n).offset() ;
			ti_val = _nodes.at(n).elem() ;
			_nodes.at(i) = TreeNode(_bondDim, ti_val, ti_offset, 5) ;
		}
	}
	// create root node
	int nodeNo = _firstCell.at(_firstCell.size() - 1) ;
	_nodes.at(nodeNo) = TreeNode(_bondDim, val, offset, 4) ;
	nelem_node = _nodes.at(nodeNo).nelements() ;
	offset += nelem_node ;
	val += nelem_node ;
	nval += nelem_node ;
	//**************************debug**********************************
//	for ( int i = 0 ; i < _nsites; i ++ ) {
//		for ( int j = 0 ; j < _nodes.at(i).size() ; j ++)
//			cout << _nodes.at(i).sites().at(j) << ", " ;
//		cout << endl ;
//		if ( _ti[i] == true ) cout << "ti nodes of first site: " << _tiNode.at(i) << endl ;
//		std::cout << _nodes.at(i).elem() - val0 << std::endl << endl ;
//	}
}

vector<int> FatTreeTensorNetwork::createSiteVec(int firstSite) {
	if ( _FTTN_NleafSites == 4 ) return createPlaquetteSiteVec(firstSite) ;
	else if ( _FTTN_NleafSites == 5 ) return createCrossSiteVec(firstSite) ;
	else {
		cout << "Unable to create leaf node with " << _FTTN_NleafSites << " sites" << endl ;
		exit(0) ;
	}
}


vector<int> FatTreeTensorNetwork::createPlaquetteSiteVec(int firstSite) {
	int inode = 0 ;
	int isite ;
	int tx, ty ;
	// firstSite = ix + iy*_length_x
	int ix = firstSite % _length_x ;
	int iy = firstSite / _length_x ;
	vector<int> node_sites(2 * 2, 0) ;
	for ( int cy = 0; cy < 2; cy ++ ) {
		for ( int cx = 0; cx < 2; cx ++ ) {
			tx = (cx + ix)%_length_x; // periodic boundary
			ty = (cy + iy)%_length_y; // periodic boundary
			isite = tx + ty * _length_x ;
			node_sites.at(inode) = isite ;
			inode ++ ;
		}
	}
	return node_sites ;
}

//     3
//  1  0  2
//     4
vector<int> FatTreeTensorNetwork::createCrossSiteVec(int firstSite) {
	// firstSite = ix + iy*_length_x
	int ix = firstSite % _length_x ;
	int iy = firstSite / _length_x ;
	vector<int> node_sites(5, 0) ;
	node_sites.at(0) = firstSite ;

	int isite ;
	int tx, ty ;

	tx = ( ix - 1 + _length_x)%_length_x ; // periodic boundary
	ty = iy ;
	isite = tx + ty * _length_x ;
	node_sites.at(1) = isite ;

	tx = ( ix + 1 )%_length_x ; // periodic boundary
	ty = iy ;
	isite = tx + ty * _length_x ;
	node_sites.at(2) = isite ;

	tx = ix ;
	ty = (iy - 1 + _length_y)%_length_y ;
	isite = tx + ty * _length_x ;
	node_sites.at(3) = isite ;

	tx = ix ;
	ty = (iy + 1)%_length_y ;
	isite = tx + ty * _length_x ;
	node_sites.at(4) = isite ;
	//--------------------------------------------
//	dispVec(node_sites) ;
	//--------------------------------------------
	return node_sites ;
}

std::vector<int> FatTreeTensorNetwork::createSftEleNum(const int* eleNum,
		const int sft_x, const int sft_y, const int irot) {
	int nsites2 = _nsites * 2 ;
	vector<int> eleNum_shift(nsites2, -1) ; // store the shifted electron configuration

	int siteNo_old, siteNo_new ;
	int jx, jy, jxr, jyr ;
	// create shifted electron configuration
	for ( int iy = 0; iy < _length_y; iy ++ ) {
		for ( int ix = 0; ix < _length_x; ix ++ ) {
			siteNo_old = ix + iy * _length_x ;
			jx = (ix + sft_x) % _length_x ;
			jy = (iy + sft_y) % _length_y ;
			for ( int j = 0; j < irot; j ++ ) {
				jxr = - jy + (_length_x - 1) ;
				jyr = jx ;
				jx = jxr ;
				jy = jyr ;
			}
			siteNo_new = jx + jy * _length_x ;

			eleNum_shift.at(siteNo_new) = eleNum[siteNo_old] ; // up spin
			eleNum_shift.at(siteNo_new + _nsites) = eleNum[siteNo_old + _nsites] ; // down spin
//						cout << siteNo_old << ",  " << siteNo_new << endl ;
//			cout << siteNo_old << ",  " ;
		}
//		cout << endl ;
	}
	return eleNum_shift ;
}

int FatTreeTensorNetwork::createSftSite(const int old_site, const int sft_x,
		const int sft_y, const int irot) {
	int ix = old_site % _length_x ;
	int iy = old_site / _length_x ;
	int jx = (ix + sft_x) % _length_x ;
	int jy = (iy + sft_y) % _length_y ;
	for ( int j = 0; j < irot; j ++ ) {
		int jxr = - jy + (_length_x - 1) ;
		int jyr = jx ;
		jx = jxr ;
		jy = jyr ;
	}
	int new_site = jx + jy * _length_x ;
	return new_site ;
}

vector<int> FatTreeTensorNetwork::createSftSites(const vector<int>& old_sites,
			const int sft_x, const int sft_y, const int irot) {
	vector<int> new_sites = old_sites ;

	for ( int i = 0 ; i < old_sites.size(); i ++ ) {
		int ix = old_sites[i] % _length_x ;
		int iy = old_sites[i] / _length_x ;
		int jx = (ix + sft_x) % _length_x ;
		int jy = (iy + sft_y) % _length_y ;
		for ( int j = 0; j < irot; j ++ ) {
			int jxr = - jy + (_length_x - 1) ;
			int jyr = jx ;
			jx = jxr ;
			jy = jyr ;
		}
		new_sites[i] = jx + jy * _length_x ;
	}
	return new_sites ;
}

void FatTreeTensorNetwork::assign(int nodeNo, int dimFile, vector<double>& nodeFile) {
	if ( dimFile == 0 ) return ;
	_nodes.at(nodeNo).assign(dimFile, nodeFile) ;
}

//-------------------------------------------------------------------------

void FatTreeTensorNetwork::initialize(const int *eleNum) {
	int nsites2 = _nsites * 2 ;
	_eleNum = std::vector< int >(nsites2, -1) ; //
	for (int i = 0; i < nsites2; i ++) {
		_eleNum.at(i) = eleNum[i] ;
	}
	//--------------------------------------------------------
	int nsyms = _rotSymProj * _latSymProj_y * _latSymProj_x ;
//	_parentEnvs.resize(nsyms) ;
//	_leftEnvs.resize(nsyms) ;
//	_rightEnvs.resize(nsyms) ;
	_branches.resize(nsyms) ;
	_changed_branches_idx.resize(nsyms) ;

	_weight = 0.0 ;
	int n = 0 ;
	int leftChildNodeNo, rightChildNodeNo ;
	for ( int irot = 0; irot < _rotSymProj; irot ++) { // rotation
		for (int sft_y = 0 ; sft_y < _latSymProj_y ; sft_y ++) { // shift along y
			for (int sft_x = 0 ; sft_x < _latSymProj_x ; sft_x ++) { // shift along x
				vector<int> eleNum_shift = createSftEleNum(eleNum, sft_x, sft_y, irot) ;
//				init_env(eleNum_shift, _parentEnvs[n], _leftEnvs[n], _rightEnvs[n]) ;
				init_branch(eleNum_shift, _branches[n]) ;
				if ( _bondDim == 0 ) _weight += 1.0 ;
				else {
					int rootNodeNo = _nnodes - 1 ;
//					_weight += _nodes[rootNodeNo].contractRoot(_leftEnvs[n][rootNodeNo], _rightEnvs[n][rootNodeNo]) ;

					leftChildNodeNo  = _children[rootNodeNo][0] ;
					rightChildNodeNo = _children[rootNodeNo][1] ;
					_weight += _nodes[rootNodeNo].contractRoot(_branches[n][leftChildNodeNo], _branches[n][rightChildNodeNo]) ;
				}
				n++ ;
			}
		}
	}
	_new_branches = _branches ;
}

void FatTreeTensorNetwork::init_env(const std::vector<int>& eleNum,
		std::vector< vector<double> >& parentEnv,
		std::vector< vector<double> >& leftEnv, std::vector< vector<double> >& rightEnv) {
	if ( _bondDim == 0 ) return ; // no FTTN in the wave function
	parentEnv.resize(_nnodes) ;
	leftEnv.resize(_nnodes) ;
	rightEnv.resize(_nnodes) ;
	//
	int rootNodeNo = _nnodes - 1 ;
	int leftChildNo = _children.at(rootNodeNo).at(0) ;
	int rightChildNo = _children.at(rootNodeNo).at(1) ;
	//
	leftEnv.at(rootNodeNo) = contractBranch(eleNum, leftChildNo, parentEnv, leftEnv, rightEnv) ;
	rightEnv.at(rootNodeNo) = contractBranch(eleNum, rightChildNo, parentEnv, leftEnv, rightEnv) ;
}

void FatTreeTensorNetwork::init_branch(const std::vector<int>& eleNum,
		 vector< vector<double> >& branch) {
	if ( _bondDim == 0 ) return ; // no FTTN in the wave function
	branch.resize(_nnodes) ;
	for ( int i = 0 ; i < _nleaves; i ++ ) {
		branch[i] = _nodes[i].get_leaf(eleNum) ;
//		_nodes[i].get_leaf(eleNum, branch[i]) ;
	}
	//-----------------------------------------------------------
	int leftChildNodeNo, rightChildNodeNo ;
	for ( int i = _nleaves; i < _nnodes - 1; i ++ ) { // internal nodes
		leftChildNodeNo  = _children[i][0] ;
		rightChildNodeNo = _children[i][1] ;
		branch[i] = _nodes[i].contractLeaves(branch[leftChildNodeNo], branch[rightChildNodeNo]) ;
	}
}

//void FatTreeTensorNetwork::initialize_physCal(const int *eleNum) {
//	int nsites2 = _nsites * 2 ;
//	_eleNum = std::vector< int >(nsites2, -1) ; //
//	for (int i = 0; i < nsites2; i ++) {
//		_eleNum.at(i) = eleNum[i] ;
//	}
//	//--------------------------------------------------------
//	int nsyms = _rotSymProj * _latSymProj_y * _latSymProj_x ;
//	_parentEnvs.resize(nsyms) ;
//	_leftEnvs.resize(nsyms) ;
//	_rightEnvs.resize(nsyms) ;
//	_branches.resize(nsyms) ;
//
//	_weight = 0.0 ;
//	int n = 0 ;
//	int leftChildNodeNo, rightChildNodeNo ;
//	for ( int irot = 0; irot < _rotSymProj; irot ++) { // rotation
//		for (int sft_y = 0 ; sft_y < _latSymProj_y ; sft_y ++) { // shift along y
//			for (int sft_x = 0 ; sft_x < _latSymProj_x ; sft_x ++) { // shift along x
//				vector<int> eleNum_shift = createSftEleNum(eleNum, sft_x, sft_y, irot) ;
//				init_env_physCal(eleNum_shift, _parentEnvs.at(n), _leftEnvs.at(n), _rightEnvs.at(n)) ;
//				init_branch(eleNum_shift, _branches[n]) ;
//				if ( _bondDim == 0 ) _weight += 1.0 ;
//				else {
//					int rootNodeNo = _nnodes - 1 ;
//					leftChildNodeNo  = _children[rootNodeNo][0] ;
//					rightChildNodeNo = _children[rootNodeNo][1] ;
//					_weight += _nodes[rootNodeNo].contractRoot(_branches[n][leftChildNodeNo], _branches[n][rightChildNodeNo]) ;
//				}
//				n++ ;
//			}
//		}
//	}
//	_new_branches = _branches ;
//}

void FatTreeTensorNetwork::init_env_physCal(const vector<int>& eleNum,
		vector< vector<double> >& parentEnv,
		 vector< vector<double> >& leftEnv, vector< vector<double> >& rightEnv) {
	if ( _bondDim == 0 ) return ; // no FTTN in the wave function
	parentEnv.resize(_nnodes) ;
	leftEnv.resize(_nnodes) ;
	rightEnv.resize(_nnodes) ;
	//
	int rootNodeNo = _nnodes - 1 ;
	int leftChildNo = _children.at(rootNodeNo).at(0) ;
	int rightChildNo = _children.at(rootNodeNo).at(1) ;
	//
	leftEnv.at(rootNodeNo) = contractBranch(eleNum, leftChildNo, parentEnv, leftEnv, rightEnv) ;
	rightEnv.at(rootNodeNo) = contractBranch(eleNum, rightChildNo, parentEnv, leftEnv, rightEnv) ;

	computeParentEnvs(rootNodeNo, parentEnv, leftEnv, rightEnv) ;
}

void FatTreeTensorNetwork::clear_new_branches() {
	int nsyms = _rotSymProj * _latSymProj_y * _latSymProj_x ;
	_new_branches.resize(nsyms) ;
	for ( int i = 0; i < nsyms; i ++ ) {
		_new_branches[i].resize(_nnodes) ;
		for ( int j = 0; j < _nnodes; j ++ ) {
			_new_branches[i][j].clear() ;
		}
	}
}

void FatTreeTensorNetwork::update() {
	_eleNum = _new_eleNum ;
	_weight = _new_weight ;

//	_parentEnvs = _new_parentEnvs ;
//	_leftEnvs   = _new_leftEnvs ;
//	_rightEnvs  = _new_rightEnvs ;
//	_branches   = _new_branches ;

	int idx ;
	for ( int i = 0; i < _changed_branches_idx.size(); i ++ ) {
		for ( int j = 0; j < _changed_branches_idx[i].size() ; j ++ ) {
			idx = _changed_branches_idx[i][j] ;
			_branches[i][idx] = _new_branches[i][idx] ;
		}
	}
}

double FatTreeTensorNetwork::compute_weight(const int* eleNum) {
	if ( _bondDim == 0 ) return 1.0 ; // no FTTN in the wave function
	double weight = 0.0 ;

	for ( int irot = 0; irot < _rotSymProj; irot ++) { // rotation
		for (int sft_y = 0 ; sft_y < _latSymProj_y ; sft_y ++) { // shift along y
			for (int sft_x = 0 ; sft_x < _latSymProj_x ; sft_x ++) { // shift along x
				vector<int> eleNum_shift = createSftEleNum(eleNum, sft_x, sft_y, irot) ;
				weight += compute_weight_LxL(eleNum_shift) ;
			}
		}
//		cout << "==================================================" << endl ;
	}
//	weight /= (double)(_latSymProj_y * _latSymProj_x * _rotSymProj) ;
	return weight ;
}

double FatTreeTensorNetwork::compute_weight_LxL(const std::vector<int>& eleNum) {
	// 从根节点递归求解
	int rootNodeNo = _nnodes - 1 ;
	int leftChildNo = _children.at(rootNodeNo).at(0) ;
	int rightChildNo = _children.at(rootNodeNo).at(1) ;
	// tensor from left branch
	vector<double> leftBranch = contractBranch(eleNum, leftChildNo) ;
	// tensor from right branch
	vector<double> rightBranch = contractBranch(eleNum, rightChildNo) ;

	double weight = _nodes.at(rootNodeNo).contractRoot(leftBranch, rightBranch) ;
	return weight ;
}

double FatTreeTensorNetwork::recompute_weight(const vector<int>& new_eleNum,
		const vector<int>& changed_sites) {
	double new_weight = 0.0 ;
	int Nsyms = _rotSymProj * _latSymProj_y * _latSymProj_x ;
	vector< vector<int> > changed_sites_symTrans(Nsyms) ;
	vector< vector<int> > eleNum_shift(Nsyms) ;
	int n = 0 ;
	for ( int irot = 0; irot < _rotSymProj; irot ++) { // rotation
		for (int sft_y = 0 ; sft_y < _latSymProj_y ; sft_y ++) { // shift along y
			for (int sft_x = 0 ; sft_x < _latSymProj_x ; sft_x ++) { // shift along x
				changed_sites_symTrans[n] = createSftSites(changed_sites, sft_x, sft_y, irot) ;
				eleNum_shift[n] = createSftEleNum(&(new_eleNum[0]), sft_x, sft_y, irot) ;
				n ++ ;
			}
		}
	}

	vector<double> weights(Nsyms, 0.0) ;
#pragma omp parallel for
	for ( int i = 0; i < Nsyms; i ++ ) {
//		weights[i] = recompute_weight_LxL_fromLeaf(eleNum_shift[i], changed_sites_symTrans[i],
//				_branches[i], _new_branches[i] );
		weights[i] = recompute_weight_LxL_fromLeaf(eleNum_shift[i], changed_sites_symTrans[i],
				_branches[i], _new_branches[i], _changed_branches_idx[i] );

	}
	for ( int i = 0; i < Nsyms; i ++ ) {
		new_weight += weights[i] ;
	}
	//----------------------------------------------------------------------------------------
	return new_weight ;
}

//compute weight of eleNum with an electron with spin s hops from ri to rj
double FatTreeTensorNetwork::recompute_weight_LxL(const vector<int>& new_eleNum,
		  const vector<int>& changed_sites,
			vector< vector<double> >& leftEnv, vector< vector<double> >& rightEnv,
			vector< vector<double> >& new_leftEnv, vector< vector<double> >& new_rightEnv) {

	int rootNodeNo = _nnodes - 1 ;
	int leftChildNo = _children.at(rootNodeNo).at(0) ;
	int rightChildNo = _children.at(rootNodeNo).at(1) ;
	// false: needless to recalculate; true: need to recalculate
	bool needRecal_left = false, needRecal_right = false ;
	new_leftEnv.at(rootNodeNo) = recontractBranch(new_eleNum, leftChildNo, changed_sites,
			needRecal_left, leftEnv, rightEnv, new_leftEnv, new_rightEnv) ;
	new_rightEnv.at(rootNodeNo) =recontractBranch(new_eleNum, rightChildNo, changed_sites,
			needRecal_right, leftEnv, rightEnv, new_leftEnv, new_rightEnv) ;

	double weight = _nodes.at(rootNodeNo).contractRoot(new_leftEnv.at(rootNodeNo), new_rightEnv.at(rootNodeNo)) ;
	//------------------------------------------------------------------------------------------------------------
	return weight ;
}

// avoid using recursive function
double FatTreeTensorNetwork::recompute_weight_LxL_fromLeaf(const vector<int>& new_eleNum,
		const vector<int>& changed_sites,
		vector< vector<double> >& branch, vector< vector<double> >& new_branch) {
	// whether need to update the branch
	vector<bool> needUpdate(_nnodes, false) ;
	bool isInc ;
	int parentNodeNo, leftChildNodeNo, rightChildNodeNo ;
	for ( int i = 0; i < _nleaves; i ++ ) {
		isInc = _nodes[i].isIncluded(changed_sites) ;
		if ( isInc ) { // include changed site(s)
			needUpdate[i] = true ; // need to update the leaf tensor
			parentNodeNo = _parent[i] ;
			needUpdate[parentNodeNo] = true ; // need to update the parent of node i
			new_branch[i] = _nodes[i].get_leaf(new_eleNum) ;
//			_nodes[i].get_leaf(new_eleNum, new_branch[i]) ;
		}
	}
	//-----------------------------------------------------------
	for ( int i = _nleaves; i < _nnodes - 1; i ++ ) { // internal nodes
		if ( needUpdate[i] ) { // if need update, its parent also needs update
			parentNodeNo = _parent[i] ;
			needUpdate[parentNodeNo] = true ; // need to update the parent of node i
			leftChildNodeNo = _children[i][0] ;
			rightChildNodeNo = _children[i][1] ;

			new_branch[i] = _nodes[i].contractLeaves(new_branch[leftChildNodeNo], new_branch[rightChildNodeNo]) ;
		}
	}
	//-----------------------------------------------------------
	int i = _nnodes - 1 ; // root node number
	leftChildNodeNo = _children[i][0] ;
	rightChildNodeNo = _children[i][1] ;
	double weight  = _nodes[i].contractRoot(new_branch[leftChildNodeNo], new_branch[rightChildNodeNo]) ;
	return weight ;
}

double FatTreeTensorNetwork::recompute_weight_LxL_fromLeaf(const std::vector<int>& new_eleNum,
		const std::vector<int>& changed_sites,
		std::vector< vector<double> >& branch, vector< vector<double> >& new_branch,
		std::vector< int >& changed_branch_idx) {
	// whether need to update the branch
	vector<bool> needUpdate(_nnodes, false) ;
	changed_branch_idx.clear() ;

	bool isInc ;
	int parentNodeNo, leftChildNodeNo, rightChildNodeNo ;
	for ( int i = 0; i < _nleaves; i ++ ) {
		isInc = _nodes[i].isIncluded(changed_sites) ;
		if ( isInc ) { // include changed site(s)
			needUpdate[i] = true ; // need to update the leaf tensor
			changed_branch_idx.push_back(i) ;
			parentNodeNo = _parent[i] ;
			needUpdate[parentNodeNo] = true ; // need to update the parent of node i
			new_branch[i] = _nodes[i].get_leaf(new_eleNum) ;
//			_nodes[i].get_leaf(new_eleNum, new_branch[i]) ;
		}
	}
	//-----------------------------------------------------------
	vector<double> leftBranch, rightBranch ;
	vector<double> buff(_bondDim * _bondDim) ;
	for ( int i = _nleaves; i < _nnodes - 1; i ++ ) { // internal nodes
		if ( needUpdate[i] ) { // if need update, its parent also needs update
			parentNodeNo = _parent[i] ;
			changed_branch_idx.push_back(i) ;
			needUpdate[parentNodeNo] = true ; // need to update the parent of node i
			leftChildNodeNo = _children[i][0] ;
			rightChildNodeNo = _children[i][1] ;

			if ( needUpdate[leftChildNodeNo] ) leftBranch = new_branch[leftChildNodeNo] ;
			else leftBranch = branch[leftChildNodeNo] ;
			if ( needUpdate[rightChildNodeNo] ) rightBranch = new_branch[rightChildNodeNo] ;
			else rightBranch = branch[rightChildNodeNo] ;

//			new_branch[i] = _nodes[i].contractLeaves(leftBranch, rightBranch, buff) ;
			_nodes[i].contractLeaves(leftBranch, rightBranch, new_branch[i], buff) ;
		}
	}
	//-----------------------------------------------------------
	int i = _nnodes - 1 ; // root node number
	leftChildNodeNo = _children[i][0] ;
	rightChildNodeNo = _children[i][1] ;
	if ( needUpdate[leftChildNodeNo] ) leftBranch = new_branch[leftChildNodeNo] ;
	else leftBranch = branch[leftChildNodeNo] ;
	if ( needUpdate[rightChildNodeNo] ) rightBranch = new_branch[rightChildNodeNo] ;
	else rightBranch = branch[rightChildNodeNo] ;
	double weight  = _nodes[i].contractRoot(leftBranch, rightBranch) ;
	return weight ;
}

vector<double> FatTreeTensorNetwork::contractBranch(const vector<int>& eleNum, int subRootNo) {

	int leftChildNodeNo = _children.at(subRootNo).at(0) ;
	int rightChildNodeNo = _children.at(subRootNo).at(1) ;

	vector<double> leftBranch, rightBranch ;
	if ( _nodes.at(leftChildNodeNo).isLeaf() ) {
		leftBranch = _nodes.at(leftChildNodeNo).get_leaf(eleNum) ;
//		_nodes.at(leftChildNodeNo).get_leaf(eleNum, leftBranch) ;
	} else {
		leftBranch = contractBranch(eleNum, leftChildNodeNo) ;
	}
	if ( _nodes.at(rightChildNodeNo).isLeaf() ) {
		rightBranch = _nodes.at(rightChildNodeNo).get_leaf(eleNum) ;
//		_nodes.at(rightChildNodeNo).get_leaf(eleNum, rightBranch) ;
	} else {
		rightBranch = contractBranch(eleNum, rightChildNodeNo) ;
	}
	return _nodes.at(subRootNo).contractLeaves(leftBranch, rightBranch) ;
}

vector<double> FatTreeTensorNetwork::contractBranch(const vector<int>& eleNum,
		int subRootNo, vector< vector<double> >& parentEnv,
		std::vector< vector<double> >& leftEnv, std::vector< vector<double> >& rightEnv) {
	int leftChildNodeNo = _children.at(subRootNo).at(0) ;
	int rightChildNodeNo = _children.at(subRootNo).at(1) ;

	vector<double> leftBranch, rightBranch ;
	if ( _nodes[leftChildNodeNo].isLeaf() ) {
		leftBranch = _nodes[leftChildNodeNo].get_leaf(eleNum) ;
//		_nodes[leftChildNodeNo].get_leaf(eleNum, leftBranch) ;
		leftEnv.at(subRootNo) = leftBranch ;
//		leftEnv.at(subRootNo) = _nodes.at(leftChildNodeNo).get_leaf(eleNum) ;
	} else {
		leftBranch = contractBranch(eleNum, leftChildNodeNo,  parentEnv, leftEnv, rightEnv) ;
		leftEnv.at(subRootNo) = leftBranch ;
//		leftEnv.at(subRootNo) = contractBranch(eleNum, leftChildNodeNo,  parentEnv, leftEnv, rightEnv) ;
	}
	if ( _nodes[rightChildNodeNo].isLeaf() ) {
		rightBranch = _nodes[rightChildNodeNo].get_leaf(eleNum) ;
//		_nodes[rightChildNodeNo].get_leaf(eleNum, rightBranch) ;
		rightEnv.at(subRootNo) = rightBranch ;
//		rightEnv.at(subRootNo) = _nodes.at(rightChildNodeNo).get_leaf(eleNum) ;
	} else {
		rightBranch = contractBranch(eleNum, rightChildNodeNo, parentEnv, leftEnv, rightEnv) ;
		rightEnv.at(subRootNo) = rightBranch ;
//		rightEnv.at(subRootNo) = contractBranch(eleNum, rightChildNodeNo, parentEnv, leftEnv, rightEnv) ;
	}
//	return _nodes.at(subRootNo).contractLeaves(leftEnv.at(subRootNo), rightEnv.at(subRootNo)) ;
	return _nodes[subRootNo].contractLeaves(leftBranch, rightBranch) ;
}

void FatTreeTensorNetwork::computeParentEnvs(int subRootNo, vector< vector<double> >& parentEnv,
		 vector< vector<double> >& leftEnv, vector< vector<double> >& rightEnv) {
	vector<double> Tup ; // env of nodes below subRootNo
	if ( _parent.at(subRootNo) == -1 )  { // subRootNo is the root node No.
		Tup =  _nodes.at(subRootNo).get_tensor() ;
	}
	else {
		vector<double> pEnv = parentEnv.at(subRootNo) ;
		vector<double> NT = _nodes.at(subRootNo).get_tensor() ;
		Tup = computeBranchRoot(pEnv, NT) ; // Tup(l,r)
	}
	//--------------------------------------------------------------------------------
	if ( _children.at(subRootNo).size() == 0 ) { // no child node
		return ;
	} else {
		int leftChildNo = _children.at(subRootNo).at(0) ;
		parentEnv.at(leftChildNo) = computeEnvLeft(Tup, rightEnv.at(subRootNo)) ;
		if (!_nodes.at(leftChildNo).isLeaf()) { // if leftChildNo is not leaf node
			computeParentEnvs(leftChildNo, parentEnv, leftEnv, rightEnv) ;
		}

		int rightChildNo = _children.at(subRootNo).at(1) ;
		parentEnv.at(rightChildNo) = computeEnvRight(Tup, leftEnv.at(subRootNo)) ;
		if ( !_nodes.at(rightChildNo).isLeaf() ) { // if rightChildNo is not leaf node
			computeParentEnvs(rightChildNo, parentEnv, leftEnv, rightEnv) ;
		}
	}
}

vector<double> FatTreeTensorNetwork::recontractBranch(const std::vector<int>& new_eleNum,
		int subRootNo, const vector<int>& changed_sites, bool& needRecal,
		std::vector< vector<double> >& leftEnv, std::vector< vector<double> >& rightEnv,
		std::vector< vector<double> >& new_leftEnv, std::vector< vector<double> >& new_rightEnv) {

	int leftChildNodeNo = _children[subRootNo][0] ;
	int rightChildNodeNo = _children[subRootNo][1] ;

	bool needRecal_left = false, needRecal_right = false ;
	bool isInc ;

	vector<double> leftBranch, rightBranch ;
	//------------------------------------------------------------------------------
	if ( _nodes[leftChildNodeNo].isLeaf() ) {
		isInc = _nodes[leftChildNodeNo].isIncluded(changed_sites) ;

		if ( isInc ) { // include changed site(s)
			leftBranch = _nodes[leftChildNodeNo].get_leaf(new_eleNum) ;
//			_nodes[leftChildNodeNo].get_leaf(new_eleNum, leftBranch) ;
			needRecal_left = true ;
			new_leftEnv[subRootNo] = leftBranch ;
		} else leftBranch = leftEnv[subRootNo] ;
	}
	else { // not a leaf
		leftBranch = recontractBranch(new_eleNum, leftChildNodeNo, changed_sites,
				needRecal_left, leftEnv, rightEnv, new_leftEnv, new_rightEnv) ;
		if ( needRecal_left ) {
			new_leftEnv[subRootNo] = leftBranch ;
		}
	}
	//------------------------------------------------------------------------------
	if ( _nodes[rightChildNodeNo].isLeaf() ) {
		isInc = _nodes[rightChildNodeNo].isIncluded(changed_sites) ;
		if ( isInc ) {
			rightBranch = _nodes[rightChildNodeNo].get_leaf(new_eleNum) ;
//			_nodes[rightChildNodeNo].get_leaf(new_eleNum, rightBranch) ;
			needRecal_right = true ;
			new_rightEnv[subRootNo] = rightBranch ;
		} else rightBranch = rightEnv[subRootNo] ;
	}
	else {
		rightBranch = recontractBranch(new_eleNum, rightChildNodeNo, changed_sites,
				needRecal_right, leftEnv, rightEnv, new_leftEnv, new_rightEnv) ;
		if ( needRecal_right ) {
			new_rightEnv[subRootNo] = rightBranch ;
		}
	}
	//------------------------------------------------------------------------------
	needRecal = (needRecal_left || needRecal_right) ;
	if ( needRecal ) {
		vector<double> T = _nodes[subRootNo].contractLeaves(leftBranch, rightBranch) ;
		return T ;
	}
	else { // do not recalculate
		int parentNodeNo = _parent[subRootNo] ;
		if ( _children[parentNodeNo][0] == subRootNo ) { // left
			return leftEnv[parentNodeNo] ;
		} else { // right
			return rightEnv[parentNodeNo] ;
		}
	}
}

double FatTreeTensorNetwork::compute_weight(const std::vector<int>& eleNum) {
	return compute_weight(&eleNum.at(0)) ;
}

double FatTreeTensorNetwork::computeSign(const int ri, const int rj, const int s) {
	int nsites2 = _nsites * 2 ;
	std::vector<int> eleSiteNum(nsites2, -1) ;
	for ( int in = 0; in < _nsites; in ++ ) {
		for ( int is = 0; is < 2; is ++ ) { // spin
			eleSiteNum.at(is + 2 * in) = _eleNum.at(in + _nsites * is) ;
		}
	}
	std::vector<int> diff_sites(2, -1) ;
	diff_sites[0] = s + 2 * ri ;
	diff_sites[1] = s + 2 * rj ;

	double fermiSgn = 1.0 ;
	int I = std::min(diff_sites[0], diff_sites[1]) ;
	int J = std::max(diff_sites[0], diff_sites[1]) ;
	for (int i = I + 1; i < J; i++) {
		fermiSgn *= (1 - 2 * eleSiteNum.at(i)) ;
	}
	return fermiSgn ;
}

double FatTreeTensorNetwork::computeRatio(const int ri, const int rj, const int s) {
	if ( _bondDim == 0 ) return 1.0 ; // no FTTN in the wave function
//	_new_leftEnvs = _leftEnvs ;
//	_new_rightEnvs = _rightEnvs ;
//	_new_branches = _branches ;

	vector<int> changed_sites(2, -1) ;
	changed_sites.at(0) = ri ;
	changed_sites.at(1) = rj ;

	_new_eleNum = _eleNum ;
	int rsi = ri + s * _nsites ;
	int rsj = rj + s * _nsites ;
	_new_eleNum.at(rsi) = 0 ;
	_new_eleNum.at(rsj) = 1 ;
	_new_weight = recompute_weight(_new_eleNum, changed_sites) ;

	double ratio = _new_weight / _weight ;
	if ( _countSign ) { ratio *= computeSign(ri, rj, s) ; }
	return ratio ;
}

// An electron with spin s hops from ri to rj.
// An electron with spin t hops from rk to rl.
double FatTreeTensorNetwork::computeRatio(const int ri, const int rj, const int s,
																					const int rk, const int rl, const int t) {
	if ( _bondDim == 0 ) return 1.0 ; // no PEPS in the wave function
//	_new_leftEnvs = _leftEnvs ;
//	_new_rightEnvs = _rightEnvs ;
//	_new_branches = _branches ;

	vector<int> changed_sites(4, -1) ;
	changed_sites.at(0) = ri ;
	changed_sites.at(1) = rj ;
	changed_sites.at(2) = rk ;
	changed_sites.at(3) = rl ;

	_new_eleNum = _eleNum ;
	int rsi = ri + s * _nsites ;
	int rsj = rj + s * _nsites ;
	int rtk = rk + t * _nsites ;
	int rtl = rl + t * _nsites ;
	_new_eleNum.at(rsi) = 0 ;
	_new_eleNum.at(rsj) = 1 ;
	_new_eleNum.at(rtk) = 0 ;
	_new_eleNum.at(rtl) = 1 ;

	_new_weight = recompute_weight(_new_eleNum, changed_sites) ;
	return (_new_weight / _weight) ;
}

//An electron with spin s hops from ri to rj
double FatTreeTensorNetwork::computeLogRatio(const int ri, const int rj, const int s) {
	if ( _bondDim == 0 ) return 0.0 ;
//	_new_leftEnvs = _leftEnvs ;
//	_new_rightEnvs = _rightEnvs ;
//	_new_branches = _branches ;

	vector<int> changed_sites(2, -1) ;
	changed_sites.at(0) = ri ;
	changed_sites.at(1) = rj ;

	_new_eleNum = _eleNum ;
	int rsi = ri + s * _nsites ;
	int rsj = rj + s * _nsites ;
	_new_eleNum.at(rsi) = 0 ;
	_new_eleNum.at(rsj) = 1 ;
	_new_weight = recompute_weight(_new_eleNum, changed_sites) ;
	//-----------------------------------------------------
	double logRatio = std::log(std::fabs( _new_weight / _weight )) ;
	return logRatio ;
}

// An electron with spin s hops from ri to rj.
// An electron with spin t hops from rk to rl.
double FatTreeTensorNetwork::computeLogRatio(const int ri, const int rj, const int s,
																						 const int rk, const int rl, const int t) {
	if ( _bondDim == 0 ) return 0.0 ;
//	_new_leftEnvs = _leftEnvs ;
//	_new_rightEnvs = _rightEnvs ;
//	_new_branches = _branches ;

	vector<int> changed_sites(4, -1) ;
	changed_sites.at(0) = ri ;
	changed_sites.at(1) = rj ;
	changed_sites.at(2) = rk ;
	changed_sites.at(3) = rl ;

	_new_eleNum = _eleNum ;
	int rsi = ri + s * _nsites ;
	int rsj = rj + s * _nsites ;
	int rtk = rk + t * _nsites ;
	int rtl = rl + t * _nsites ;
	_new_eleNum.at(rsi) = 0 ;
	_new_eleNum.at(rsj) = 1 ;
	_new_eleNum.at(rtk) = 0 ;
	_new_eleNum.at(rtl) = 1 ;

	_new_weight = recompute_weight(_new_eleNum, changed_sites) ;
	double logRatio = std::log(std::fabs( _new_weight / _weight )) ;
	return logRatio ;
}

void FatTreeTensorNetwork::derivative(double *der, const int* eleNum) {
	if ( _bondDim == 0 ) return ;
	// initialize all derivative value to 0.0
	xscal(_numel, 0.0, der) ;

//	int n = 0 ;
//	for ( int irot = 0; irot < _rotSymProj; irot ++) { // rotation
//		for (int sft_y = 0 ; sft_y < _latSymProj_y ; sft_y ++) {
//			for (int sft_x = 0 ; sft_x < _latSymProj_x ; sft_x ++) {
//				vector<int> eleNum_shift = createSftEleNum(eleNum, sft_x, sft_y, irot) ;
//				derivative_LxL(der, eleNum_shift, _parentEnvs[n], _leftEnvs[n], _rightEnvs[n]) ;
//				//----------------------------------------------------
//				n++ ;
//			}
//		}
//	}
//---------------------------------------------------------------------------------------
	int Nsyms = _rotSymProj * _latSymProj_y * _latSymProj_x ;
	vector< vector<int> > eleNum_shift(Nsyms) ;
	int n = 0 ;
	for ( int irot = 0; irot < _rotSymProj; irot ++)  // rotation
		for (int sft_y = 0 ; sft_y < _latSymProj_y ; sft_y ++)  // shift along y
			for (int sft_x = 0 ; sft_x < _latSymProj_x ; sft_x ++) { // shift along x
				eleNum_shift[n] = createSftEleNum(eleNum, sft_x, sft_y, irot) ;
				n ++ ;
			}
	vector< vector<double> > der_shift(Nsyms) ;

#pragma omp parallel for
	for ( int i = 0; i < Nsyms; i ++ ) {
		der_shift[i].assign(_numel, 0.0) ;

		vector< vector<double> > parentEnv, leftEnv, rightEnv ;
		init_env_physCal(eleNum_shift[i], parentEnv, leftEnv, rightEnv) ;
		derivative_LxL(&(der_shift[i][0]), eleNum_shift[i], parentEnv, leftEnv, rightEnv) ;

//		derivative_LxL(&(der_shift[i][0]), eleNum_shift[i], _parentEnvs[i], _leftEnvs[i], _rightEnvs[i]) ;
	}

	for ( int i = 0; i < Nsyms; i ++ ) {
		xaxpy(_numel, 1.0, &(der_shift[i][0]), der) ;
	}
	//======================================debug======================================
//	for ( int i = 0 ; i < _numel; i ++ ) {
//		if (der[i] != 0) cout << i << ", " << der[i] << endl ;
//	}
//	exit(0) ;
}

void FatTreeTensorNetwork::derivative(double *der, const int* eleNum, const double weight) {
	_weight = weight ;
	derivative(der, eleNum) ;
}

void FatTreeTensorNetwork::derivative_LxL(double *der, const std::vector<int>& eleNum) {
	// derivatives of leaf nodes
	for ( int i = 0; i < _nleaves; i ++ ) {
		vector<double> E = contractBranchEnv(eleNum, i) ;
		leafDerivative(der, eleNum, _nodes.at(i), E) ;
	}
	// derivatives of internal nodes
	for ( int i =  _nleaves; i <  (_nnodes - 1); i ++ ) {
		vector<double> E = contractBranchEnv(eleNum, i) ;

		int leftChildNo = _children.at(i).at(0) ;
		vector<double> leftLeaf ;
		if ( _nodes.at(leftChildNo).isLeaf() ) {
			leftLeaf = _nodes.at(leftChildNo).get_leaf(eleNum) ;
//			_nodes.at(leftChildNo).get_leaf(eleNum, leftLeaf) ;
		} else {
			leftLeaf = contractBranch(eleNum, leftChildNo) ;
		}

		int rightChildNo = _children.at(i).at(1) ;
		vector<double>  rightLeaf ;
		if ( _nodes.at(rightChildNo).isLeaf() ) {
			rightLeaf = _nodes.at(rightChildNo).get_leaf(eleNum) ;
//			_nodes.at(rightChildNo).get_leaf(eleNum, rightLeaf) ;
		} else {
			rightLeaf = contractBranch(eleNum, rightChildNo) ;
		}
		interNodeDerivative(der, E, leftLeaf, rightLeaf, _nodes.at(i).offset()) ;
	}
	// derivatives of root node
	int leftChildNo = _children.at(_nnodes - 1).at(0) ;
 	vector<double> leftLeaf = contractBranch(eleNum, leftChildNo) ;
 	int rightChildNo = _children.at(_nnodes - 1).at(1) ;
 	vector<double> rightLeaf = contractBranch(eleNum, rightChildNo) ;
	rootNodeDerivative(der, leftLeaf, rightLeaf, _nodes.at(_nnodes - 1).offset()) ;
}

void FatTreeTensorNetwork::derivative_LxL(double *der, const std::vector<int>& eleNum,
		std::vector< vector<double> >& parentEnv,
		std::vector< vector<double> >& leftEnv, std::vector< vector<double> >& rightEnv) {
	// derivatives of leaf nodes
	for ( int i = 0; i < _nleaves; i ++ ) {
		leafDerivative(der, eleNum, _nodes.at(i), parentEnv.at(i)) ;
	}
	// derivatives of internal nodes
	for ( int i = _nleaves; i < (_nnodes - 1); i ++ ) {
		interNodeDerivative(der, parentEnv.at(i), leftEnv.at(i), rightEnv.at(i), _nodes.at(i).offset()) ;
	}
	// derivatives of root node
	rootNodeDerivative(der, leftEnv.at(_nnodes - 1), rightEnv.at(_nnodes - 1), _nodes.at(_nnodes - 1).offset()) ;
}

// environment
vector<double> FatTreeTensorNetwork::contractBranchEnv(const vector<int>& eleNum, int branchRootNo) {
	assert(branchRootNo != (_nnodes - 1) ) ; // branchRootNo should not be the root of the tree
	// parent node number of branchRootNo
	int pNodeNo = _parent.at(branchRootNo) ;

	vector<double> Env ;
	if ( _parent.at(pNodeNo) == -1 ) { // pNodeNo is the root node No.
		Env =  _nodes.at(pNodeNo).get_tensor() ;
	} else {
		// env of upper branch
		Env = contractBranchEnv(eleNum, pNodeNo) ;
		vector<double> NT = _nodes.at(pNodeNo).get_tensor() ; // parent Node Tensor
		Env = computeBranchRoot(Env, NT) ;
	}
	//--------------------------------------------------------------------------------
	if (_children.at(pNodeNo).at(0) == branchRootNo ) { // left branch of its parent
		vector<double> rightBranch ;
		int sibBranchNo = _children.at(pNodeNo).at(1) ;
		if ( _nodes.at(sibBranchNo).isLeaf() ) {
			rightBranch = _nodes.at(sibBranchNo).get_leaf(eleNum) ;
//			_nodes.at(sibBranchNo).get_leaf(eleNum, rightBranch) ;
		} else {
			rightBranch = contractBranch(eleNum, sibBranchNo) ;
		}
		Env = computeEnvLeft(Env, rightBranch) ;
	} else { // right branch of its parent
		vector<double> leftBranch ;
		int sibBranchNo = _children.at(pNodeNo).at(0) ;
		if ( _nodes.at(sibBranchNo).isLeaf() ) {
			leftBranch = _nodes.at(sibBranchNo).get_leaf(eleNum) ;
//			_nodes.at(sibBranchNo).get_leaf(eleNum, leftBranch) ;
		} else {
			leftBranch = contractBranch(eleNum, sibBranchNo) ;
		}
		Env = computeEnvRight(Env, leftBranch) ;
	}
	return Env ;
}

// EN(l) = sum{r}_[N(l,r)*tRight(r)]
vector<double> FatTreeTensorNetwork::computeEnvLeft(vector<double>& N,  vector<double>& tRight) {
	vector<double> EN(_bondDim) ;
	xgemv('N', _bondDim, _bondDim, N, tRight, EN) ;
	return EN ;
}

// EN(r) = sum{l}_[N(l,r)*tLeft(l)]
vector<double> FatTreeTensorNetwork::computeEnvRight(vector<double>& N,  vector<double>& tLeft) {
	vector<double> EN(_bondDim) ;
	xgemv('T', _bondDim, _bondDim, N, tLeft, EN) ;
	return EN ;
}

// contract environment vector with its connected node tensor
// EN(l,r) = sum{u}_[N(u,(l,r)) * E(u)]
vector<double> FatTreeTensorNetwork::computeBranchRoot(vector<double>& E, vector<double>& N) {
	int m = _bondDim ;
	int n = _bondDim * _bondDim ;
	vector<double> EN(n) ;
	xgemv('T', m, n, N, E, EN) ;
	return EN ; // E(l,r)
}

void FatTreeTensorNetwork::leafDerivative(double *der, const int* eleNum,
																				  TreeNode& leafNode, vector<double>& E) {
	vector<int> sites = leafNode.sites() ;
	int physInd ;
	int idx = 0 ;
	int base = _bondDim ;
	for ( int i = 0; i < sites.size(); i ++ ) {
		int iLattice = sites.at(i) ;
		physInd = getPhysInd(eleNum, iLattice) ;
		idx += physInd * base ;
		base *= _siteDim ;
	}
	int offset = idx + leafNode.offset() ;

	double* node_der = der + offset ;
	double invWeight = 1.0 / _weight ;
	for ( int j = 0; j < _bondDim; j ++ ) {
		node_der[j] += E[j] * invWeight ;
	}
	//=======================debug========================
//	double a = 0 ;
//	for ( int j = 0; j < _bondDim; j ++  ) {
//		a += E[j] * leafNode.get_leaf(eleNum)[j] ;
//	}
//	cout << "weight error: " << fabs((a - _weight)/_weight) << endl ;
}

void FatTreeTensorNetwork::leafDerivative(double *der, const std::vector<int>& eleNum,
                                          TreeNode& leafNode, vector<double>& E) {
	leafDerivative(der, &eleNum[0], leafNode, E) ;
}

void FatTreeTensorNetwork::interNodeDerivative(double *der, vector<double>& E16,
		vector<double>& lf0, vector<double>& lf1,vector<double>& lf4, vector<double>& lf5,
		int offset) {
	int idx = 0 ;
	// (a16,a0,a1,a4,a5)
	for ( int a0 = 0; a0 < _bondDim; a0 ++ )
		for ( int a1 = 0; a1 < _bondDim; a1 ++ )
			for ( int a4 = 0; a4 < _bondDim; a4 ++ )
				for ( int a5 = 0; a5 < _bondDim; a5 ++ )
					for ( int a16 = 0; a16 < _bondDim; a16 ++ ) {
						idx = a16 + a0 * _bondDim + a1 * _bondDim * _bondDim +
								a4 * _bondDim * _bondDim * _bondDim +
								a5 * _bondDim * _bondDim * _bondDim * _bondDim ;
						idx += offset ;

						der[idx] += E16[a16] * lf0[a0] * lf1[a1] * lf4[a4] * lf5[a5] / _weight ;
					}
}

void FatTreeTensorNetwork::interNodeDerivative(double *der, vector<double>& E16,
		vector<double>& lf0, vector<double>& lf1, int offset) {
	int idx = 0 ;
	double invWeight = 1.0 / _weight ;
	int bondDim2 = _bondDim * _bondDim ;
	double * derNode = der + offset ;

	double E_left, E_right ;
	int i_left, i_right ;
	for ( int a0 = 0; a0 < _bondDim; a0 ++ ) {
		E_left = lf0[a0] * invWeight;
		i_left = a0 * _bondDim ;
		for ( int a1 = 0; a1 < _bondDim; a1 ++ ) {
			E_right = lf1[a1] ;
			i_right = a1 * bondDim2 ;
			for ( int a16 = 0; a16 < _bondDim; a16 ++ ) {
				idx = a16 + i_left + i_right ;
				derNode[idx] += E16[a16] * E_left * E_right ;
			}
		}
	}

	//=======================debug========================
//	double a = 0 ;
//	double* elemNode = _elem + offset ;
//	for ( int a0 = 0; a0 < _bondDim; a0 ++ )
//		for ( int a1 = 0; a1 < _bondDim; a1 ++ )
//			for ( int a16 = 0; a16 < _bondDim; a16 ++ ) {
//				idx = a16 + a0 * _bondDim + a1 * bondDim2 ;
//				a += derNode[idx] * elemNode[idx] ;
//			}
//	cout << "a: " << a << endl ;
//	exit(0) ;
}

void FatTreeTensorNetwork::rootNodeDerivative(double *der,
		vector<double>& t16, vector<double>& t17,vector<double>& t18, vector<double>& t19,
		int offset) {
	int idx = 0 ;
	// (a16,a17,a18,a19)
	for ( int a16 = 0; a16 < _bondDim; a16 ++ )
		for ( int a17 = 0; a17 < _bondDim; a17 ++ )
			for ( int a18 = 0; a18 < _bondDim; a18 ++ )
				for ( int a19 = 0; a19 < _bondDim; a19 ++ ) {
					idx = a16 + a17 * _bondDim + a18 * _bondDim * _bondDim +
							a19 * _bondDim * _bondDim * _bondDim ;
					idx += offset ;
					der[idx] += t16[a16] * t17[a17] * t18[a18] * t19[a19] / _weight ;
				}
}

void FatTreeTensorNetwork::rootNodeDerivative(double *der,
		vector<double>& t28, vector<double>& t29, int offset) {
	int idx = 0 ;
	double invWeight = 1.0 / _weight ;
	// (a28,a29)
	for ( int a28 = 0; a28 < _bondDim; a28 ++ )
		for ( int a29 = 0; a29 < _bondDim; a29 ++ ) {
					idx = a28 + a29 * _bondDim ;
					idx += offset ;
					der[idx] += t28[a28] * t29[a29] * invWeight ;
				}
	//=======================debug========================
//	double a = 0 ;
//	for ( int a28 = 0; a28 < _bondDim; a28 ++ )
//		for ( int a29 = 0; a29 < _bondDim; a29 ++ ) {
//			idx = a28 + a29 * _bondDim ;
//			idx += offset ;
//			a += t28[a28] * t29[a29] * _elem[idx] ;
//		}
//	cout << "weight error: " << fabs((a - _weight)/_weight) << endl ;
//	exit(0) ;
}

//-------------------------------------------------------------------------
int FatTreeTensorNetwork::getPhysInd(const int* eleNum, const int i) {
	int physInd ;
	int upInd, downInd ;
	upInd = eleNum[i] ;
	downInd = eleNum[i + _nsites] ;
	physInd = upInd + downInd * 2 ;
//	std::cout << "physInd: " << physInd << std::endl ;
	if ( physInd < 0 || physInd >= _siteDim) {
		std::cout << "FatTreeTensorNetwork::getPhysInd error: " ;
		std::cout << "physInd = " << physInd << ", which is out of range." << std::endl ;
		exit(0) ;
	}
	return physInd ;
}

void FatTreeTensorNetwork::rescale(double maxElem) {
	for ( int i = 0 ; i < _nnodes; i ++ ) {
		if ( _ti[i] == false ) {
			_nodes.at(i).rescale(maxElem) ;
		}
	}
}

void FatTreeTensorNetwork::disp() {
	for ( int i = 0; i < _nnodes; i ++ ) {
		_nodes.at(i).disp() ;
	}
}

void FatTreeTensorNetwork::dispVec(vector<int> vec) {
	for ( int i = 0; i < vec.size(); i ++ ) {
		cout << vec.at(i) << ", " ;
	}
	cout << endl ;
}
