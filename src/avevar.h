/*
 * avevar.h
 *
 *  Created on: 2015年3月22日
 *      Author: zhaohuihai
 */

#ifndef SRC_AVEVAR_H_
#define SRC_AVEVAR_H_

void StoreOptData(int sample);
void OutputOptData();

#endif /* SRC_AVEVAR_H_ */
