/*
 * correlator_product_state.cpp
 *
 *  Created on: 2016-1-13
 *      Author: zhaohuihai
 */

#include <math.h>
#include <stdlib.h>
#include <iomanip>
#include "correlator_product_state.h"

using namespace std ;

CorrelatorProductState::CorrelatorProductState(
		const bool latticeCorr, const bool allPairCorr, const bool absorbCorr,
		const int length_x, const int length_y,
		const int unitCell_x, const int unitCell_y,
		const int corr_x, const int corr_y,
		const bool countSign, const int siteDim, double* elem)
{
	_latticeCorr = latticeCorr ;
	_allPairCorr = allPairCorr ;
	_absorbCorr = absorbCorr ;
	_length_x = length_x ;
	_length_y = length_y ;
	_unitCell_x = unitCell_x ;
	_unitCell_y = unitCell_y ;
	_corr_x = corr_x ;
	_corr_y = corr_y ;
	_countSign = countSign ;
	//---------------------------------
	_nsites = _length_x * _length_y ;
	_siteDim = siteDim ;
	set_ti() ;

	_ncorrelators = 0 ; // initial number of correlators
	_correlators.clear() ;

//	int isite ;
//	int i = 0 ; // cps index
//	int tx, ty ;
//	int icorr ;
	double* val = elem ;
	int nval = 0 ; // count the total number of variational parameters
	if ( _latticeCorr ) {
//		for ( int iy = 0; iy < _length_y; iy ++ ) {
//			for ( int ix = 0; ix < _length_x; ix ++ ) {
//
//				vector<int> corr_sites(_corr_x * _corr_y, 0) ;
//				icorr = 0 ;
////				cout << "CPS sites in each correlator: " << endl ;
//				for ( int cy = 0; cy < _corr_y; cy ++ ) {
//					for ( int cx = 0; cx < _corr_x; cx ++ ) {
//						tx = (cx + ix)%_length_x;
//						ty = (cy + iy)%_length_y;
//						isite = tx + ty * _length_x ;
//						corr_sites.at(icorr) = isite ;
//						icorr ++ ;
////						cout << isite << ", " ;
//					}
//				}
////				cout << endl << val << endl ;
//				_correlators.push_back(Correlator(_nsites, corr_sites, _siteDim, val)) ;
//				val += _correlators[i].nelements() ;
//				nval += _correlators[i].nelements() ;
//				i ++ ;
//			}
//		}
		createLatticeCPS(val, nval) ;
	}
	if ( _allPairCorr ) {
		createAllPairCPS(val, nval) ;
	}
	_ncorrelators = _correlators.size() ;
	_numel = nval ;
	//*********************debug*********************
//	for ( int i = 0 ; i < _ncorrelators; i ++ ) {
//		for ( int j = 0 ; j < _correlators.at(i).size(); j ++ ) {
//			cout << _correlators.at(i).site().at(j) << ", " ;
//		}
//		cout << endl ;
////		if ( _ti[i] == true ) cout << "ti correlator of first site: " << _tiSite.at(i) << endl ;
//		std::cout << _correlators.at(i).elem() << std::endl << endl ;
//	}
//	exit(0) ;
}

void CorrelatorProductState::set_ti()
{
	_ti.assign(size_t(_nsites), true) ;
	_tiSite.assign(size_t(_nsites), -1) ;

	int nTIsites = _unitCell_x * _unitCell_y ;
	_firstCell.assign(size_t(nTIsites), -1) ;
	for ( int i = 0; i < _firstCell.size(); i ++ ) {
		int x = i % _unitCell_x ;
		int y = i / _unitCell_x ;
		int n = x + y * _length_x ; // site number in whole lattice
		_ti[n] = false ; // false for first unit cell sites
		_firstCell[i] = n ;
//		cout << _firstCell[i] << ", " ;
	}
//	cout << endl ;
	//-------------------------------------------------------
	for ( int iy = 0; iy < _length_y; iy ++ ) {
		for ( int ix = 0; ix < _length_x; ix ++ ) {
			int n = ix + iy * _length_x ;
			if ( _ti[n] == true ) { // ti sites
				int ux = ix % _unitCell_x ;
				int uy = iy % _unitCell_y ;
				_tiSite[n] = _firstCell[ ux + uy * _unitCell_x ] ;
			}
		}
	}
	//*************************debug*************************
//	for ( int iy = 0; iy < _length_y; iy ++ ) {
//		for ( int ix = 0; ix < _length_x; ix ++  ) {
//			std::cout << _ti[ix + iy * _length_x] << ", " ;
//		}
//		std::cout << std::endl ;
//	}
//	std::cout << "======================================" << std::endl ;
//	for ( int iy = 0; iy < _length_y; iy ++ ) {
//		for ( int ix = 0; ix < _length_x; ix ++  ) {
//			std::cout << setw(3) << _tiSite[ix + iy * _length_x] << ", " ;
//		}
//		std::cout << std::endl ;
//	}
//	exit(0) ;
}

void CorrelatorProductState::createLatticeCPS(double* &val, int &nval)
{
	int nelem_corr ;
	_correlators.resize(_nsites) ;
	for ( int i = 0; i < _firstCell.size(); i ++ ) {
		int siteNo = _firstCell.at(i) ;
		vector<int> corr_sites = createPlaquetteSiteVec(siteNo) ;
		_correlators.at(siteNo) = Correlator(_nsites, corr_sites, _siteDim, val) ;
		nelem_corr = pow((double)_siteDim, (double)corr_sites.size()) ;
		val += nelem_corr ;
		nval += nelem_corr ;
	}
	double* ti_val ;
	for ( int i = 0; i < _nsites; i ++ ) {
		if ( _ti[i] == true ) { // translational invariant site tensors
			vector<int> corr_sites = createPlaquetteSiteVec(i) ;
			ti_val = _correlators.at( _tiSite.at(i) ).elem() ;
			_correlators.at(i) = Correlator(_nsites, corr_sites, _siteDim, ti_val) ;
		}
	}
	//*********************debug*********************
//	for ( int i = 0 ; i < _nsites; i ++ ) {
//		for ( int j = 0 ; j < _correlators.at(i).size(); j ++ ) {
//			cout << _correlators.at(i).site().at(j) << ", " ;
//		}
//		cout << endl ;
//		if ( _ti[i] == true ) cout << "ti correlator of first site: " << _tiSite.at(i) << endl ;
//		std::cout << _correlators.at(i).elem() << std::endl << endl ;
//	}
//	exit(0) ;
}

void CorrelatorProductState::createAllPairCPS(double* &val, int &nval)
{
	int nelem_corr ;
	for ( int n1 = 0; n1 < _nsites; n1 ++ ) {
		int x1 = n1 % _length_x ;
		int y1 = n1 / _length_x ;
		for ( int n2 = (n1 + 1); n2 < _nsites; n2 ++ ) {
			int x2 = n2 % _length_x ;
			int y2 = n2 / _length_x ;
			int xDistance = measure_linear_distance(_length_x, x1, x2) ;
			int yDistance = measure_linear_distance(_length_y, y1, y2) ;
			if ( (xDistance < _corr_x) && (yDistance < _corr_y) && _absorbCorr) continue ;

			vector<int> corr_sites(2, 0) ;
			corr_sites.at(0) = n1 ;
			corr_sites.at(1) = n2 ;
			_correlators.push_back(Correlator(_nsites, corr_sites, _siteDim, val)) ;
			nelem_corr = pow((double)_siteDim, (double)corr_sites.size()) ;
			val += nelem_corr ;
			nval += nelem_corr ;
		}
	}
}

std::vector<int> CorrelatorProductState::createPlaquetteSiteVec(int firstSite)
{
	int icorr = 0 ;
	int isite ;
	int tx, ty ;
	// firstSite = ix + iy*_length_x
	int ix = firstSite % _length_x ;
	int iy = firstSite / _length_x ;
	vector<int> corr_sites(_corr_x * _corr_y, 0) ;
	for ( int cy = 0; cy < _corr_y; cy ++ ) {
		for ( int cx = 0; cx < _corr_x; cx ++ ) {
			tx = (cx + ix)%_length_x;
			ty = (cy + iy)%_length_y;
			isite = tx + ty * _length_x ;
			corr_sites.at(icorr) = isite ;
			icorr ++ ;
//				cout << isite << ", " ;
		}
	}
	return corr_sites ;
}

void CorrelatorProductState::initialize(const int *eleNum)
{
	int nsites2 = _nsites * 2 ;
	_eleNum = std::vector< int >(nsites2, -1) ; //
	for (int i = 0; i < nsites2; i ++) {
		_eleNum.at(i) = eleNum[i] ;
	}

	_weights = std::vector< double >(_ncorrelators, 0.0) ;
	for (int i = 0; i < _ncorrelators; i ++) {
		_weights.at(i) = _correlators.at(i).compute_weight(eleNum) ;
	}
}

void CorrelatorProductState::update()
{
	if ( _ncorrelators == 0 ) return ;

	for (int i = 0; i < _changed_eleInd.size(); i ++){
		_eleNum.at( _changed_eleInd.at(i) ) = _changed_eleNum.at(i) ;
	}
	for ( int i = 0; i < _changed_cpsInd.size(); i ++ ) {
		_weights.at( _changed_cpsInd.at(i) ) = _changed_weights.at(i) ;
	}
}

double CorrelatorProductState::computeSign(const int ri, const int rj, const int s)
{
	int nsites2 = _nsites * 2 ;
	std::vector<int> eleSiteNum(nsites2, -1) ;
	for ( int in = 0; in < _nsites; in ++ ) {
		for ( int is = 0; is < 2; is ++ ) { // spin
			eleSiteNum.at(is + 2 * in) = _eleNum.at(in + _nsites * is) ;
		}
	}
	std::vector<int> diff_sites(2, -1) ;
	diff_sites[0] = s + 2 * ri ;
	diff_sites[1] = s + 2 * rj ;

//	cout << ri << ", " << rj << ", " << s << endl ;
//	cout << diff_sites[0] << ", " << diff_sites[1] << endl ;
	double fermiSgn = 1.0 ;
	int I = std::min(diff_sites[0], diff_sites[1]) ;
	int J = std::max(diff_sites[0], diff_sites[1]) ;
	for (int i = I + 1; i < J; i++) {
		fermiSgn *= (1 - 2 * eleSiteNum.at(i)) ;
	}
//	cout << "sign: " << fermiSgn << endl ;
//	cout << endl ;
//	exit(0) ;
	return fermiSgn ;
}

double CorrelatorProductState::computeRatio(
		const int ri, const int rj, const int s)
{
	if ( _ncorrelators == 0 ) return 1.0 ;

	_changed_eleInd = std::vector< int >(2, -1) ;
	int rsi = ri + s * _nsites ;
	int rsj = rj + s * _nsites ;
	_changed_eleInd.at(0) = rsi ;
	_changed_eleInd.at(1) = rsj ;

	_changed_eleNum = std::vector< int >(2, -1) ;
	_changed_eleNum.at(0) = 0 ;
	_changed_eleNum.at(1) = 1 ;

	find_changed() ;
	double ratio = 1.0 ;
	double old_weight, new_weight ;
	for ( int i = 0; i < _changed_cpsInd.size(); i ++ ) {
		old_weight = _weights.at( _changed_cpsInd.at(i) ) ;
		new_weight = _changed_weights.at(i) ;
		ratio *= ( new_weight / old_weight ) ;
	}

	if ( _countSign ) {
		ratio *= computeSign(ri, rj, s) ;
	}
	return ratio ;
}

double CorrelatorProductState::computeRatio(
		const int ri, const int rj, const int s,
		const int rk, const int rl, const int t)
{
	if ( _ncorrelators == 0 ) return 1.0 ;
	_changed_eleInd = std::vector< int >(4, -1) ;
	int rsi = ri + s * _nsites ;
	int rsj = rj + s * _nsites ;
	int rtk = rk + t * _nsites ;
	int rtl = rl + t * _nsites ;
	_changed_eleInd.at(0) = rsi ;
	_changed_eleInd.at(1) = rsj ;
	_changed_eleInd.at(2) = rtk ;
	_changed_eleInd.at(3) = rtl ;

	_changed_eleNum = std::vector< int >(4, -1) ;
	_changed_eleNum.at(0) = 0 ;
	_changed_eleNum.at(1) = 1 ;
	_changed_eleNum.at(2) = 0 ;
	_changed_eleNum.at(3) = 1 ;

	find_changed() ;
	double ratio = 1.0 ;
	double old_weight, new_weight ;
	for ( int i = 0; i < _changed_cpsInd.size(); i ++ ) {
		old_weight = _weights.at( _changed_cpsInd.at(i) ) ;
		new_weight = _changed_weights.at(i) ;
		ratio *= ( new_weight / old_weight ) ;
	}
	return ratio ;
}

double CorrelatorProductState::computeLogRatio(
		const int ri, const int rj, const int s)
{
	if ( _ncorrelators == 0 ) return 0.0 ;
	_changed_eleInd = std::vector< int >(2, -1) ;
	int rsi = ri + s * _nsites ;
	int rsj = rj + s * _nsites ;
	_changed_eleInd.at(0) = rsi ;
	_changed_eleInd.at(1) = rsj ;

	_changed_eleNum = std::vector< int >(2, -1) ;
	_changed_eleNum.at(0) = 0 ;
	_changed_eleNum.at(1) = 1 ;
	//============================
//	for (int i = 0; i < _nsites*2; i ++)
//		std::cout << "_eleNum[" << i << "]: " << _eleNum.at(i) << std::endl ;
//	std::cout << "ri: " << ri << ", rj: " << rj << ", s: " << s << std::endl ;
	//============================
	find_changed() ;
	double logRatio = 0.0 ;
	double old_weight, new_weight ;
	for ( int i = 0; i <  _changed_cpsInd.size(); i ++) {
//		std::cout << "changed mps: " << _changed_mpsInd.at(i) << std::endl ;
		old_weight = _weights.at( _changed_cpsInd.at(i) ) ;
		new_weight = _changed_weights.at(i) ;
		logRatio += log(fabs( new_weight / old_weight )) ;
	}
	return logRatio ;
}

// An electron with spin s hops from ri to rj.
// An electron with spin t hops from rk to rl.
double CorrelatorProductState::computeLogRatio(
		const int ri, const int rj, const int s,
		const int rk, const int rl, const int t)
{
	if ( _ncorrelators == 0 ) return 0.0 ;
	_changed_eleInd = std::vector< int >(4, -1) ;
	int rsi = ri + s * _nsites ;
	int rsj = rj + s * _nsites ;
	int rtk = rk + t * _nsites ;
	int rtl = rl + t * _nsites ;
	_changed_eleInd.at(0) = rsi ;
	_changed_eleInd.at(1) = rsj ;
	_changed_eleInd.at(2) = rtk ;
	_changed_eleInd.at(3) = rtl ;

	_changed_eleNum = std::vector< int >(4, -1) ;
	_changed_eleNum.at(0) = 0 ;
	_changed_eleNum.at(1) = 1 ;
	_changed_eleNum.at(2) = 0 ;
	_changed_eleNum.at(3) = 1 ;

	find_changed() ;
	double logRatio = 0.0 ;
	double old_weight, new_weight ;
	for ( int i = 0; i <  _changed_cpsInd.size(); i ++) {
		old_weight = _weights.at( _changed_cpsInd.at(i) ) ;
		new_weight = _changed_weights.at(i) ;
		logRatio += log(fabs( new_weight / old_weight )) ;
	}
	return logRatio ;
}

// find changed correlators
void CorrelatorProductState::find_changed()
{
	if ( _ncorrelators == 0 ) return ;
	_changed_cpsInd.clear() ;
	_changed_weights.clear() ;

	// change electron number
	vector<int> saved_eleNum(_changed_eleInd.size(), -1) ;
	for ( int k = 0; k < _changed_eleInd.size(); k ++ ) {
		saved_eleNum[k] = _eleNum.at(_changed_eleInd[k]) ;
		_eleNum.at(_changed_eleInd[k]) = _changed_eleNum[k] ;
	}
	//-----------------------------------
	int siteInd ;
	bool changed ;
	for (int i = 0; i < _correlators.size(); i ++) { // loop for all correlators
		changed = false ;
		for ( int j = 0 ; j < _correlators.at(i).size(); j ++ ) { // loop for all sites indices in an correlator
			for (int k = 0; k < _changed_eleInd.size(); k ++) {
				siteInd = _changed_eleInd.at(k) % _nsites ;
				if (_correlators[i].site()[j] == siteInd)
				{
					_changed_cpsInd.push_back(i) ;
					_changed_weights.push_back( _correlators[i].compute_weight(_eleNum) ) ;
					changed = true ;
					break ;
				}
			}
			if (changed == true) break ;
		}
	}
	//----------------------------------
	// revert electron number changing
	for (int k = 0; k < _changed_eleInd.size(); k ++ )
	{
		_eleNum.at(_changed_eleInd[k]) = saved_eleNum[k] ;
	}
}

void CorrelatorProductState::derivative(double *der, const int* eleNum)
{
	if ( _ncorrelators == 0 ) return ;

	double* cor_der = der ; // pointer to derivative of every correlator
	// initialize all derivative value to 0.0
	for ( int i = 0; i < _numel; i ++ ) der[i] = 0.0 ;
	int offset ;
	// compute derivative for every correlator
	for ( int k = 0; k < _ncorrelators; k ++ ) {
		offset = _correlators.at(k).elem() - _correlators.at(0).elem() ;
//		cout << "correlator " << k << ", offset: " << offset << endl ;
		cor_der = der + offset ;
		_correlators.at(k).derivative(cor_der, eleNum) ;
	}
}

void CorrelatorProductState::derivative(double *der_val, int *der_ind, int ind_offset, const int* eleNum)
{
	if ( _ncorrelators == 0 ) return ;
//	cout << "_ncorrelators: " << _ncorrelators << endl ;
	int offset ;
	for ( int k = 0; k < _ncorrelators; k ++ ) {
		_correlators.at(k).derivative(der_val[k], der_ind[k], eleNum) ;
		offset = _correlators.at(k).elem() - _correlators.at(0).elem() + ind_offset ;
		der_ind[k] += offset ;
	}
}

void CorrelatorProductState::rescale(double maxElem)
{
	for ( int i = 0; i < _ncorrelators; i ++ )
		if ( _ti[i] == false ) {
			_correlators.at(i).rescale(maxElem) ;
		}
}

void CorrelatorProductState::info()
{
	cout << "number of sites: " << _nsites << endl ;
	std::cout << "number of correlators:  " << _ncorrelators << std::endl ;
	for (int i = 0; i < _ncorrelators; i ++) {
		_correlators.at(i).info() ;
	}
}

void CorrelatorProductState::disp()
{
	cout << "display cps elements" << endl ;
	for (int i = 0; i < _ncorrelators; i ++) {
		_correlators.at(i).disp() ;
	}
}

//======================================================================

int count_nPairCorrelators(const bool absorbCorr, const int length_x, const int length_y,
											     const int _unitCell_x, const int _unitCell_y,
		                       const int _corr_x, const int _corr_y)
{
	int nsites = length_x * length_y ;
	int ncorr = nsites * (nsites - 1) / 2 ;
//	int ncorr = 0 ;
	for ( int n1 = 0; n1 < nsites; n1 ++ ) { // n1=x1 + y1*length_x
		int x1 = n1 % length_x ;
		int y1 = n1 / length_x ;
		for ( int n2 = (n1 + 1); n2 < nsites; n2 ++ ) { // n2=x2 + y2*length_x
			int x2 = n2 % length_x ;
			int y2 = n2 / length_x ;

			int xDistance = measure_linear_distance(length_x, x1, x2) ;
			int yDistance = measure_linear_distance(length_y, y1, y2) ;
			if ( (xDistance < _corr_x) && (yDistance < _corr_y) && absorbCorr)
				ncorr -- ;
//			if ( (xDistance >= _corr_x) || (yDistance >= _corr_y) ) {
//				cout << n1 << ", " << n2 << endl ;
//				ncorr ++ ;
//			}
		}
	}
//	cout << ncorr << endl ;
//	exit(0) ;
	return ncorr ;
}

int measure_linear_distance(const int length_x, const int x1, const int x2)
{
	int d1 = abs(x1 - x2) ;
	int d2 = length_x - d1 ;
	if ( d1 > d2 ) return d2 ;
	else return d1 ;
}
