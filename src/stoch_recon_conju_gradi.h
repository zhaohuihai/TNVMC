/*
 * stoch_recon_conju_gradi.h
 *
 *  Created on: 2015-6-28
 *      Author: zhaohuihai
 */

#ifndef SRC_STOCH_RECON_CONJU_GRADI_H_
#define SRC_STOCH_RECON_CONJU_GRADI_H_

#include "vmcmain.h"

class StochReconConjuGradi
{
private:
	double _stepWidth ;
	int _nSmat ; // dim of overlap matrix
	double _diag_shift ; // shift the diagonal value of S matrix

	std::vector<double> _Os ; // [_nSmat * NVMCSample] O(k,i)
	std::vector<int> _Os_ind ;
	std::vector<double> _O ;  // [_nSmat] <O(k)>
	std::vector<double> _sDiag ; // [_nSmat]
	std::vector<double> _sDiag_shift ; // [_nSmat] parameter dependent shift

	std::vector<double> _x_vec ; // array for storing the solution to the linear equation
	std::vector<double> _b_vec ; // array for storing the b vector for the linear equation
	std::vector<double> _y_vec ; // array for storing the vector resulting from the matrix's action on the direction vector
	std::vector<double> _d_vec ; // array for storing the congugate gradient direction vector
	std::vector<double> _r_vec ; // array for use in computing the overlap's diagonal and solving the linear equation

	std::vector<double> _OOd;         //
	std::vector<double> _process_y;   //
	std::vector<double> _Od ; // [NVMCSample]

	bool _cg_converged;          // flag to tell when conjugate gradient iterations have converged

public:
	// constructor
	StochReconConjuGradi(MPI_Comm comm, double stepWidth, double lowBound, int& info) ;
	// destructor
	~StochReconConjuGradi() {}

	// initialize
	void initialize(std::vector<int>& smatToParaIdx,
			std::vector<double>& sDiagElm, std::vector<double>& sDiagElm_SD, const double stepWidth) ;

	double computeSdiag_SD(MPI_Comm comm, double OOOO, double OOO, double OO, double O) ;
	void output_Sdiag() ;
	void output_Sdiag(std::vector<double>& sDiagElm, std::vector<double>& sDiagElm_SD) ;
	void output_CG_error(std::vector<double>& CGerr) ;
	void findMaxMin(int n, std::vector<double>& sDiagElm, double& sDiagMax, double& sDiagMin) ;
  //-----------------------------------------------------------------------------------
  // Solves a system of linear equations A x = b using the conjugate gradient method.
	//
  //                             Using preconditioner M, the equations become
  //
  //                             (M A M) v = (M b)           v = M^(-1) x
  //
  //                             which is solved for v by conjugate gradient.
  //
  //                             x is then computed as x = M v.
  //
  //                             A must be a positive definite hermitian matrix.
  //                             M must be an invertable hermitian matrix.
  //
  //                             Presumably, the ratio of largest to smallest
  //                             eigenvalues is smaller for (M A M) than for A.
  //-----------------------------------------------------------------------------------
	bool conjugate_gradient(const int n, 			 // dimension of the system
				const int m,       // maximum number of iterations
				double * const x , // size n. on entry, a guess for x. on exit, the solution vector x.
				double * const b , // size n.  on entry, the vector b.    on exit, contents are overwritten.
				double * const y , // size n.  vector used as workspace.  on exit, contents are overwritten.
				double * const d , // size n.  vector used as workspace.  on exit, contents are overwritten.
				double * const r , // size n.  vector used as workspace.  on exit, contents are overwritten.
				MPI_Comm comm);

	bool conjugate_gradient_restart(const int n, 			 // dimension of the system
				const int m,       // maximum number of iterations
				double * const x , // size n. on entry, a guess for x. on exit, the solution vector x.
				double * const b , // size n.  on entry, the vector b.    on exit, contents are overwritten.
				double * const q , // size n.  vector used as workspace.  on exit, contents are overwritten.
				double * const d , // size n.  vector used as workspace.  on exit, contents are overwritten.
				double * const r , // size n.  vector used as workspace.  on exit, contents are overwritten.
				MPI_Comm comm);
	bool preconditioned_conjugate_gradient_restart(const int n, // dimension of the system
			const int m, // maximum number of iterations
			double * const x , // size n. on entry, a guess for x. on exit, the solution vector x.
			double * const b , // size n.  on entry, the vector b.    on exit, contents are overwritten.
			double * const q , // size n.  vector used as workspace.  on exit, contents are overwritten.
			double * const d , // size n.  vector used as workspace.  on exit, contents are overwritten.
			double * const r , // size n.  vector used as workspace.  on exit, contents are overwritten.
			MPI_Comm comm) ;

	// function to perform y = A d  (A == S)
	void operate_by_A(const int n, double * const d, double * const y, MPI_Comm comm) ;
	// function to perform d = M r, where M is preconditioning operator
	void operate_by_M(const int n, double * const r, double * const d, MPI_Comm comm) ;
	// function to perform d = M^(-1) r, where M is preconditioning operator
	void operate_by_M_inv(const int n, double * const r, double * const d, MPI_Comm comm) ;
	// function to check if the cojugate gradient iterations have converged
	bool converged(const double residual, const double residual0, MPI_Comm comm) ;
	bool converged(const double residual, MPI_Comm comm) ;
	bool x_converged(double alpha, double * d, double * x, MPI_Comm comm) ;
};



#endif /* SRC_STOCH_RECON_CONJU_GRADI_H_ */

//===========================================================================================


