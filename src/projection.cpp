/*-------------------------------------------------------------
 * Variational Monte Carlo
 * Gutzwiller-Jastrow Projection and DH correlation factors
 *-------------------------------------------------------------
 * by Satoshi Morita
 *-------------------------------------------------------------*/

#include "projection.h"

using namespace std ;

void MakeProjCnt(int *projCnt, const int *eleNum) {
  const int *n0=eleNum; // up spin electron
  const int *n1=eleNum+Nsite; // down spin electron
  int idx,offset;
  int ri,rj;
  int xi,xj,xn,xm;
  int r0,r1,r2,r3;
  const int *dh;
  /* optimization for Kei */
  const int nProj=NProj;
  const int nSite=Nsite;

  /* initialization */
  for(idx=0;idx<nProj;idx++) projCnt[idx] = 0;

  /* Gutzwiller factor */
  if(NGutzwillerIdx>0) {
    for(ri=0;ri<nSite;ri++) {
      projCnt[ GutzwillerIdx[ri] ] += n0[ri]*n1[ri];
    }
  }

  /* Jastrow factor exp(sum {v_ij * (ni-1) * (nj-1)}) */
  if(NJastrowIdx>0) {
    offset = NGutzwillerIdx;
    for(ri=0;ri<nSite;ri++) {
      xi = n0[ri]+n1[ri]-1;
      if(xi==0) continue;
      for(rj=ri+1;rj<nSite;rj++) {
        xj = n0[rj]+n1[rj]-1;
        idx = offset + JastrowIdx[ri][rj];
        projCnt[idx] += xi*xj;
      }
    }
  }

  /* 2-site doublon-holon correlation factor */
  if(NDoublonHolon2siteIdx>0) {
    offset = NGutzwillerIdx + NJastrowIdx;
    #pragma omp parallel for default(shared) private(xn,dh,ri,xi,r0,r1,xm,idx)
    for(xn=0;xn<NDoublonHolon2siteIdx;xn++) {
      dh=DoublonHolon2siteIdx[xn];
      for(ri=0;ri<nSite;ri++) {
        xi = n0[ri]+n1[ri];
        if(xi==1) continue;
        xi = xi/2; /* xi=0: holon, xi=1: doublon */

        r0 = dh[2*ri];
        r1 = dh[2*ri+1];
        if(xi==0) {
          xm = n0[r0]*n1[r0] + n0[r1]*n1[r1]; /* count doublons on r0 and r1 */
        } else { /* doublon */
          xm = (1-n0[r0])*(1-n1[r0]) 
            + (1-n0[r1])*(1-n1[r1]); /* count holons on r0 and r1 */
        }

        idx = offset + xn + (xi+2*xm)*NDoublonHolon2siteIdx;
        projCnt[idx] += 1;
      }
    }
  }

  /* 4-site doublon-holon correlation factor */
  if(NDoublonHolon4siteIdx>0) {
    offset = NGutzwillerIdx + NJastrowIdx + 6*NDoublonHolon2siteIdx;
    #pragma omp parallel for default(shared) private(xn,dh,ri,xi,r0,r1,r2,r3,xm,idx)
    for(xn=0;xn<NDoublonHolon4siteIdx;xn++) {
      dh=DoublonHolon4siteIdx[xn];
      for(ri=0;ri<nSite;ri++) {
        xi = n0[ri]+n1[ri];
        if(xi==1) continue;
        xi = xi/2; /* xi=0: holon, xi=1: doublon */

        r0 = dh[4*ri];
        r1 = dh[4*ri+1];
        r2 = dh[4*ri+2];
        r3 = dh[4*ri+3];
        if(xi==0) {
          /* count doublons on r0, r1, r2, r3 */
          xm= n0[r0]*n1[r0] + n0[r1]*n1[r1]
            + n0[r2]*n1[r2] + n0[r3]*n1[r3];
        } else { /* doublon */
          /* count holons on r0, r1, r2, r3 */
          xm= (1-n0[r0])*(1-n1[r0]) + (1-n0[r1])*(1-n1[r1])
            + (1-n0[r2])*(1-n1[r2]) + (1-n0[r3])*(1-n1[r3]);
        }
        
        idx = offset + xn + (xi+2*xm)*NDoublonHolon4siteIdx;
        projCnt[idx] += 1;
      }
    }
  }

  return;
}

double computeLogProj(const int *eleNum) {
	vector<int> projCnt(NProj, 1.0) ;

	double ProjWeight = 0.0 ;

	for (int sft_y = 0 ; sft_y < latSymProj_y ; sft_y ++) { // shift along y
		for (int sft_x = 0 ; sft_x < latSymProj_x ; sft_x ++) { // shift along x
			vector<int> eleNum_shift = createSftEleNum(eleNum, sft_x, sft_y) ;
			MakeProjCnt(&(projCnt[0]), &(eleNum_shift[0])) ;
			ProjWeight += ProjVal(&(projCnt[0])) ;
		}
	}
	ProjWeight /= (double)(latSymProj_y * latSymProj_x) ;
	return log(ProjWeight) ;
}

double computeProj(const int *eleNum) {
	vector<int> projCnt(NProj, 1.0) ;

	double ProjWeight = 0.0 ;

	for (int sft_y = 0 ; sft_y < latSymProj_y ; sft_y ++) { // shift along y
		for (int sft_x = 0 ; sft_x < latSymProj_x ; sft_x ++) { // shift along x
			vector<int> eleNum_shift = createSftEleNum(eleNum, sft_x, sft_y) ;
			MakeProjCnt(&(projCnt[0]), &(eleNum_shift[0])) ;
			ProjWeight += ProjVal(&(projCnt[0])) ;
		}
	}
	ProjWeight /= (double)(latSymProj_y * latSymProj_x) ;
	return ProjWeight ;
}

/* An electron with spin s hops from ri to rj. */
void UpdateProjCnt(const int ri, const int rj, const int s,
                   int *projCntNew, const int *projCntOld,
                   const int *eleNum) {
  const int *n0=eleNum;
  const int *n1=eleNum+Nsite;
  int idx,offset;
  int rk;
  int xi,xn,xm;
  int r0,r1,r2,r3;
  const int *dh;
  /* optimization for Kei */
  const int nProj=NProj;
  const int nSite=Nsite;

  if(projCntNew!=projCntOld) {
    for(idx=0;idx<nProj;idx++) projCntNew[idx] = projCntOld[idx];
  }
  if(ri==rj) return;

  if(NGutzwillerIdx>0){
    idx = GutzwillerIdx[ri];
    projCntNew[idx] -= n0[ri]+n1[ri];
    idx = GutzwillerIdx[rj];
    projCntNew[idx] += n0[rj]*n1[rj];
  }

  if(NJastrowIdx>0){
    offset = NGutzwillerIdx; 
    /* update [ri][rj] */
    if(ri<rj) idx = offset + JastrowIdx[ri][rj];
    else  idx = offset + JastrowIdx[rj][ri];
    projCntNew[idx] += n0[ri]+n1[ri]-n0[rj]-n1[rj]+1;
    /* update [ri][rk] (rk != ri, rj) */
    for(rk=0;rk<nSite;rk++) {
      if(rk==rj) continue;
      if(rk==ri) continue;
      if(rk>ri) idx = offset + JastrowIdx[ri][rk];
      else idx = offset + JastrowIdx[rk][ri]; 
      projCntNew[idx] -= n0[rk]+n1[rk]-1;
    }
    /* update [rj][rk] (rk != ri, rj) */
    for(rk=0;rk<nSite;rk++) {
      if(rk==ri) continue;
      if(rk==rj) continue;
      if(rk>rj) idx = offset + JastrowIdx[rj][rk];
      else idx = offset + JastrowIdx[rk][rj]; 
      projCntNew[idx] += n0[rk]+n1[rk]-1;
    }
  }

  if(NDoublonHolon2siteIdx==0 && NDoublonHolon4siteIdx==0) return;

  offset = NGutzwillerIdx + NJastrowIdx;
  for(idx=offset;idx<nProj;idx++) projCntNew[idx] = 0;

  /* 2-site doublon-holon correlation factor */
  offset = NGutzwillerIdx + NJastrowIdx;
  #pragma omp parallel for default(shared) private(xn,dh,rk,xi,r0,r1,xm,idx)
  for(xn=0;xn<NDoublonHolon2siteIdx;xn++) {
    dh=DoublonHolon2siteIdx[xn];
    for(rk=0;rk<Nsite;rk++) {
      xi = n0[rk]+n1[rk];
      if(xi==1) continue;
      xi = xi/2; /* xi=0: holon, xi=1: doublon */
      
      r0 = dh[2*rk];
      r1 = dh[2*rk+1];
      if(xi==0) {
        xm = n0[r0]*n1[r0] + n0[r1]*n1[r1]; /* count doublons on r0 and r1 */
      } else { /* doublon */
        xm = (1-n0[r0])*(1-n1[r0]) 
          + (1-n0[r1])*(1-n1[r1]); /* count holons on r0 and r1 */
      }
      
      idx = offset + xn + (xi+2*xm)*NDoublonHolon2siteIdx;
      projCntNew[idx] += 1;
      
    }
  }

  /* 4-site doublon-holon correlation factor */
  offset = NGutzwillerIdx + NJastrowIdx + 6*NDoublonHolon2siteIdx;
  #pragma omp parallel for default(shared) private(xn,dh,rk,xi,r0,r1,r2,r3,xm,idx)
  for(xn=0;xn<NDoublonHolon4siteIdx;xn++) {
    dh=DoublonHolon4siteIdx[xn];
    for(rk=0;rk<Nsite;rk++) {
      xi = n0[rk]+n1[rk];
      if(xi==1) continue;
      xi = xi/2; /* xi=0: holon, xi=1: doublon */
      
      r0 = dh[4*rk];
      r1 = dh[4*rk+1];
      r2 = dh[4*rk+2];
      r3 = dh[4*rk+3];
      if(xi==0) {
        /* count doublons on r0, r1, r2, r3 */
        xm= n0[r0]*n1[r0] + n0[r1]*n1[r1]
          + n0[r2]*n1[r2] + n0[r3]*n1[r3];
      } else { /* doublon */
        /* count holons on r0, r1, r2, r3 */
        xm= (1-n0[r0])*(1-n1[r0]) + (1-n0[r1])*(1-n1[r1])
          + (1-n0[r2])*(1-n1[r2]) + (1-n0[r3])*(1-n1[r3]);
      }
      
      idx = offset + xn + (xi+2*xm)*NDoublonHolon4siteIdx;
      projCntNew[idx] += 1;
    }
  }
  
  return;

}

std::vector<int> createSftEleNum(const int* eleNum,const int sft_x, const int sft_y) {
	int nsites = length_x * length_y ;
	int nsites2 = nsites * 2 ;
	vector<int> eleNum_shift(nsites2, -1) ; // store the shifted electron configuration

	int siteNo_old, siteNo_new ;
	int jx, jy, jxr, jyr ;
	// create shifted electron configuration
	for ( int iy = 0; iy < length_y; iy ++ ) {
		for ( int ix = 0; ix < length_x; ix ++ ) {
			siteNo_new = ix + iy * length_x ;
			jx = (ix + sft_x) % length_x ;
			jy = (iy + sft_y) % length_y ;
			siteNo_old = jx + jy * length_x ;

			eleNum_shift.at(siteNo_new) = eleNum[siteNo_old] ; // up spin
			eleNum_shift.at(siteNo_new + nsites) = eleNum[siteNo_old + nsites] ; // down spin
//			cout << siteNo_new << ",  " << siteNo_old << endl ;
//			cout << siteNo_old << ",  " ;
		}
//		cout << endl ;
	}
//	cout << "------------------" <<endl ;
	return eleNum_shift ;
}
