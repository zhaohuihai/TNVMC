/*
 * stoch_recon_conju_gradi_spars.cpp
 *
 *  Created on: 2016年1月23日
 *      Author: zhaohuihai
 */

#include "vmcmain.h"
#include "sfmt/SFMT.h"
#include "workspace.h"
#include "vmctime.h"
#include "safempi.h"
#include "blas_lapack.hpp"
#include "stoch_recon_conju_gradi_spars.h"

using namespace std ;

StochReconConjuGradiSpars::StochReconConjuGradiSpars(
		MPI_Comm comm, double stepWidth, double lowBound, int& info)
{
	_stepWidth = stepWidth ;
	_nSmat = NPara ;
	_nSmat_sparse = SROptSize_sparse - 1 ;

	info = 0 ;
	initialize() ;

	int rank, size;
  MPI_Comm_rank(comm,&rank);
  MPI_Comm_size(comm,&size);
  conjugate_gradient_restart(_nSmat,
			                       max_cg_iter,
			                       &_x_vec.at(0),
			                       &_b_vec.at(0),
			                       &_y_vec.at(0),
			                       &_d_vec.at(0),
			                       &_r_vec.at(0),
			                       comm) ;
  FILE *fp;
  double rmax ; // the max parameter change
  int simax ;
  // print zqp_SRinfo.dat
  rmax = _x_vec[0]; simax=0;
  for(int si=0;si<_nSmat;si++) {
    if(fabs(rmax) < fabs(_x_vec[si])) {
      rmax = _x_vec[si]; simax=si;
    }
  }
  int optNum=0,cutNum=0;
  int sDiagMax=0,sDiagMin=0;
  if ( rank == 0 ) {
  	fprintf(FileSRinfo, "%5d %5d %5d %5d % .5e % .5e % .5e %5d\n",
  			NPara,_nSmat,optNum,cutNum,sDiagMax,sDiagMin,rmax, simax);
  	// check inf and nan
  	for ( int si = 0; si < _nSmat; si ++ ) {
  		if ( !isfinite( _x_vec[si] ) ) {
  			fprintf(stderr, "StcOpt: r[%d]=%.10lf\n",si,_x_vec[si]);
  			info = 1 ;
  			break ;
  		}
  	}
  }
  double randCoef ;
  // update variatonal parameters
  for ( int si = 0; si < _nSmat; si ++ ) {
//  	cout << si << ": " << _x_vec[si] << endl ;
  	randCoef = genrand_real2() * (1 - lowBound) + lowBound ;
  	Para[si] += randCoef * _x_vec[si] ;
  }
//  exit(0) ;
}

void StochReconConjuGradiSpars::initialize()
{
	_Os_val.assign(size_t(_nSmat_sparse * NVMCSample), 0.0) ; // O(k,i)
	_Os_ind.assign(size_t(_nSmat_sparse * NVMCSample), 0.0) ;
	_O.assign(size_t(_nSmat), 0.0) ; // <O(k)>
	_sDiag.assign(size_t(_nSmat), 0.0) ; //

	_x_vec.assign(size_t(_nSmat), 0.0) ; // solution vector
	_b_vec.assign(size_t(_nSmat), 0.0) ; // b vector
	_y_vec.assign(size_t(_nSmat), 0.0) ;
	_d_vec.assign(size_t(_nSmat), 0.0) ;
	_r_vec.assign(size_t(_nSmat), 0.0) ;

	_OOd.assign(size_t(_nSmat), 0.0) ;
	_process_y.assign(size_t(_nSmat), 0.0) ;

	_Od.assign(size_t(NVMCSample), 0.0) ;
	//======================================================
//	int sIdx ;
//	int offset ;
	for (int i = 0; i < NVMCSample; i ++) {
//		offset = i * SROptSize_sparse ;
		for ( int si = 0; si < _nSmat_sparse; si ++ ) { // remove first row of SROptO_Store
//			sIdx = si + offset ;
			_Os_val.at(si + i * _nSmat_sparse) = SROptO_Store[si + 1 + i * SROptSize_sparse] ;
			_Os_ind.at(si + i * _nSmat_sparse) = SROptO_ind[si + 1 + i * SROptSize_sparse] - 1;
//			_Os_ind.at(si + i * _nSmat_sparse) = SROptO_ind[si + 1 + i * SROptSize_sparse] ;
		}
	}
	//======================================================
	for ( int si = 0; si < _nSmat; si ++ ) {
		_O[si] = SROptO[si + 1] ;
		_b_vec[si] = -_stepWidth * 2.0 * (SROptHO[si + 1] - SROptHO[0] * SROptO[si + 1]) ;
	}
}

bool StochReconConjuGradiSpars::conjugate_gradient_restart(const int n, 			 // dimension of the system
		const int m,       // maximum number of iterations
		double * const x , // size n. on entry, a guess for x. on exit, the solution vector x.
		double * const b , // size n.  on entry, the vector b.    on exit, contents are overwritten.
		double * const q , // size n.  vector used as workspace.  on exit, contents are overwritten.
		double * const d , // size n.  vector used as workspace.  on exit, contents are overwritten.
		double * const r , // size n.  vector used as workspace.  on exit, contents are overwritten.
		MPI_Comm comm)
{
  int myrank, nproc;
  MPI_Comm_rank(comm,&myrank);
  MPI_Comm_size(comm,&nproc);

  double delta = 100.0 ;
  double alpha, beta ;

  // initialize the residual as r = b - A*x
  operate_by_A(n, x, r, comm) ; // r = A * x
  xscal(n, -1.0, r) ; // r -> -r
  // r = b + r
  xaxpy(n, 1.0, b, r) ; // r = b - A*x
  //***********************************
  //***********************************
  // initialize the search direction as d = r
  xcopy(n, r, d) ;

  // compute the initial residual norm
  delta = xdot(n, r, r) ;

  // iteratively compute x
  int iter = 0 ;
  for ( ; iter < m; iter ++) {
  	// check if we are finished
  	const bool conv = converged(fabs(sqrt(delta)), comm) ;
  	if (conv) break ;

  	// compute q = A d
  	operate_by_A(n, d, q, comm) ;
//  	exit(0) ;
  	// compute alpha
  	alpha = delta / xdot(n, d, q);

  	// update x
  	xaxpy(n, alpha, d, x) ; // x = alpha*d + x
  	// update the residual
  	if (((iter+1) % 20) == 0) {
  		operate_by_A(n, x, r, comm) ; // r = A * x
  		xscal(n, -1.0, r) ; // r -> -r
  		xaxpy(n, 1.0, b, r) ; // r = b - A*x
  	} else {
  		xaxpy(n, -alpha, q, r) ; // r = r - alpha * q
  	}
  	// compute beta
  	beta = xdot(n, r, r) / delta ;

  	// compute new residual norm
  	delta = beta * delta ;
//  	if ( myrank == 0 ) std::cout << "delta: " << delta << std::endl ;
  	// compute new search direction
  	xscal(n, beta, d) ;
  	xaxpy(n, 1.0, r, d) ; // d = r + d
//  	if ( myrank == 0 ) cout << iter << ": " << x[0] << endl ;
  }
//  if (myrank == 0) cout << x[0] << endl ;

  // check if the method converged
  const bool conv= converged(fabs(sqrt(delta)), comm) ;
  if (conv && myrank == 0) {
  	cout << "conjugate gradient converged in " <<  iter << " iterations." << endl ;
  }
  else if ( myrank == 0 ) {
  	cout << "conjugate gradient did not converge after " << iter <<  " iterations." << endl ;
  }

  return conv ;
}

// function to perform y = A d  (A == S = <OO>-<O><O>)
void StochReconConjuGradiSpars::operate_by_A(const int n,
		double * const d, double * const y, MPI_Comm comm)
{
	int rank, nproc ;
	MPI_Comm_rank(comm, &rank) ;
  MPI_Comm_size(comm, &nproc);
	// broadcast the trial vector to all processes
	MPI_Bcast(d, n, MPI_DOUBLE, 0, comm) ;
	// zero the result vector
	xscal(n, 0.0, y) ;

	// compute action of each visited configuration's overlap matrix on the search direction
	xscal(n, 0.0, &_process_y.at(0)) ;
	xscal(n, 0.0, &_OOd.at(0)) ;
	xscal(NVMCSample, 0.0, &_Od.at(0)) ;
	//-------------------------------------
	int offset ;
	// Od(j) = sum{l}_[Os(l,j)*d(l)]
	//	xgemv('T', n, NVMCSample, &_Os.at(0), d, &_Od.at(0)) ;
	// Od(j) = sum{l}_[_Os_val(l,j)*d(_Os_ind(l,j) )]
	for ( int j = 0; j < NVMCSample; j ++ ) {
		offset = j * _nSmat_sparse ;
		for ( int l = 0; l < _nSmat_sparse; l ++ ) {
			_Od[j] +=  _Os_val[l + offset] * d[ _Os_ind[l + offset] ] ;
//			cout << "ind: " << _Os_ind[l + offset] << ", " << d[ _Os_ind[l + offset] ] << endl ;
		}
//		cout << _Od[j] << endl ;
	}
	// process_y(k) = sum{j}_[Os(k,j)*Od(j)]
//	xgemv('N', n, NVMCSample, &_Os.at(0), &_Od.at(0), &_process_y.at(0)) ;
	// process_y(_Os_ind(k,j)) = sum{j}_[_Os_val(k,j)*Od(j)]
	for ( int k = 0; k < _nSmat_sparse; k ++) {
		for ( int j = 0; j < NVMCSample; j ++ ) {
			_process_y[ _Os_ind[k + j * _nSmat_sparse] ] += _Os_val[ k + j * _nSmat_sparse ] * _Od[j] ;
		}
	}
	//-------------------------------------
	MPI_Barrier(comm) ;
	// reduce actions from all processes on the root process
	SafeMpiAllReduce(&_process_y.at(0), y, n, comm) ;
//	if (rank == 0)
//		for ( int k = 0; k < _nSmat; k ++ ) cout << k << ", " << y[k] << endl ;
//	exit(0) ;
	// y = y / (NVMCSample * nproc)
	double a = 1.0 / (double)(NVMCSample * nproc) ;
	xscal(n, a, y) ;
	//----------------------------------
	// coef = sum{l}_[<O(l)>*d(l)]
	double coef = xdot(n, &_O.at(0), d) ;
	coef = 0.0 - coef ;
	// y(k) = - coef * <O(k)> + y(k)
	xaxpy(n, coef, &_O.at(0), y) ;

	// add diagonal shift on root process
	if (rank == 0) {
		// y(k) = DSROptStaDel * d(k) + y(k)
		for (int k = 0; k < n; k ++) {
//			y[k] += DSROptStaDel * _sDiag[k] * d[k] ;
			y[k] += DSROptStaDel * d[k] ;
		}
	}
}

// function to check if the cojugate gradient iterations have converged
bool StochReconConjuGradiSpars::converged(const double residual, MPI_Comm comm)
{
	int myrank ;
	MPI_Comm_rank(comm, &myrank) ;
	if (myrank == 0) {
//		cout << "residual: " << residual << endl ;
		_cg_converged = (residual < cg_thresh) ;
	}
//	MPI_Bcast(&_cg_converged, 1, MPI_C_BOOL, 0, comm) ;
	MPI_Bcast(&_cg_converged, 1, MPI::BOOL, 0, comm) ;
	return _cg_converged ;
}
