/*
 * correlator_product_state.h
 *
 *  Created on: 2016-1-13
 *      Author: zhaohuihai
 */

#ifndef SRC_CORRELATOR_PRODUCT_STATE_H_
#define SRC_CORRELATOR_PRODUCT_STATE_H_

#include <vector>
#include "correlator.h"

class CorrelatorProductState
{
private:
	bool _latticeCorr ;
	bool _allPairCorr ;
	bool _absorbCorr ;
	int _length_x ;
	int _length_y ;
	int _unitCell_x ;
	int _unitCell_y ;
	int _corr_x ; // correlator size in x direction
	int _corr_y ; // correlator size in y direction

	bool _countSign ;
	int _nsites ; // number of sites in the lattice
	int _siteDim ;             // dim of physical index
	int _numel ;               // number of total elements
	std::vector<bool> _ti ;    // whether a tensor is a translated image of another tensor
	std::vector<int> _firstCell ; // first unit cell
	std::vector<int> _tiSite ; // if a translated image, the site defining the image's parent
	int _ncorrelators ;
	std::vector<Correlator> _correlators ;
	std::vector< int > _eleNum ; //
	std::vector< double > _weights ; // vector of every weight <eleNum|cps>
	// Changed electron indices. spin up and spin down electrons are stored separately
	std::vector< int > _changed_eleInd ;
	// electron number of changed electron indices.
	std::vector< int > _changed_eleNum ;

	// CPSs with electron number changed
	std::vector< int > _changed_cpsInd ;
	// weights of CPSs with electron number changed
	std::vector< double > _changed_weights ;

	void set_ti() ; // set translational invariant sites
	void createLatticeCPS(double* &val, int &nval) ;
	void createAllPairCPS(double* &val, int &nval) ;
	std::vector<int> createPlaquetteSiteVec(int firstSite) ;
	void count_ncorrelators(const int length_x, const int length_y) ;
	void find_changed() ;

public:
	CorrelatorProductState(const bool latticeCorr, const bool allPairCorr, const bool absorbCorr,
			const int length_x, const int length_y,
			const int _unitCell_x, const int _unitCell_y,
			const int _corr_x, const int _corr_y,
			const bool _countSign, const int siteDim, double* elem) ;
	void initialize(const int *eleNum) ;
	void update() ;
	//----------------------------------------------------------------------
	Correlator& operator [] (const int i) { return _correlators[i] ; }
	Correlator& at          (const int i) { return _correlators.at(i) ; }
	//----------------------------------------------------------------------
	double computeSign(const int ri, const int rj, const int s) ; // fermion sign
	// compute <x'|SBS> / <x|SBS>
	double computeRatio(const int ri, const int rj, const int s) ;
	double computeRatio(const int ri, const int rj, const int s,
											const int rk, const int rl, const int t) ;
	// compute log(<x'|SBS> / <x|SBS>)
	double computeLogRatio(const int ri, const int rj, const int s) ;
	double computeLogRatio(const int ri, const int rj, const int s,
												 const int rk, const int rl, const int t) ;
	void derivative(double *der, const int* eleNum) ;
	void derivative(double *der_val, int *der_ind, int ind_offset, const int* eleNum) ;
	//------------------------------------------------
	void rescale(double maxElem) ;
	void info() ;
	void disp() ;
};

int count_nPairCorrelators(const bool absorbCorr, const int length_x, const int length_y,
											     const int _unitCell_x, const int _unitCell_y,
		                       const int _corr_x, const int _corr_y) ;
int measure_linear_distance(const int length_x, const int x1, const int x2) ;

#endif /* SRC_CORRELATOR_PRODUCT_STATE_H_ */
