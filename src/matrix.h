/*
 * matrix.h
 *
 *  Created on: 2015-3-22
 *      Author: zhaohuihai
 */

#ifndef SRC_MATRIX_H_
#define SRC_MATRIX_H_

int CalculateMAll(const int *eleIdx, const int qpStart, const int qpEnd);
int calculateMAll_child(const int *eleIdx, const int qpStart, const int qpEnd, const int qpidx,
                        double *bufM, int *iwork, double *work, int lwork);

double calculatePfaffian(const int *eleIdx, const int qpStart, const int qpEnd, const int qpidx);


#define D_PfLimit 1.0e-100

//int M_DGETRF(int *m, int *n, double *a, int *lda, int *ipiv, int *info);
//int M_DGETRI(int *n, double *a, int *lda, int *ipiv, double *work, int *lwork, int *info);
//int M_DSKPFA(const char *uplo, const char *mthd, const int *n,
//             double *a, const int *lda, double *pfaff, int *iwork,
//             double *work, const int *lwork, int *info);

extern "C"
{
	void dskpfa_(const char *, const char *, const int *,
      				 double *, const int *, double *, int *,
							 double *, const int *, int *);
  void dskpf10_(const char *, const char *, const int *,
                double *, const int *, double *, int *,
                double *, const int *, int *);
  void dgesvd_(char *, char *, int *, int *, double *, int *, double *,
  		         double *, int *, double *, int *, double *, int *, int *);
}

int getLWork() ;

double compute_condNum(double *A, int m) ;

// compute 1-norm of a square matrix
double compute_norm1(const double *A, const int m) ;
double compute_norm1(const double *A, const int m, const int qpidx) ;

double compute_condNum_norm1(const int *eleIdx,
		const int qpStart, const int qpEnd, const int qpidx, double* invM) ;

void output_array(const double* a, const int len, const std::string filename) ;

#endif /* SRC_MATRIX_H_ */
