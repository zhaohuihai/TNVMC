/*
 * readdef.h
 *
 *  Created on: 2015年3月22日
 *      Author: zhaohuihai
 */

#ifndef SRC_READDEF_H_
#define SRC_READDEF_H_

int ReadDefFileError(char *defname);
int ReadDefFileNInt(char *xNameListFile, MPI_Comm comm);

int countSBStensor() ;
void verifyDef() ;

int ReadDefFileIdxPara(char *xNameListFile, MPI_Comm comm);

void setOptFlag(int* optFlag, int n, bool opt) ;

#endif /* SRC_READDEF_H_ */
