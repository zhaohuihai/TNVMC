/*
 * workspace.h
 *
 *  Created on: 2015年3月22日
 *      Author: zhaohuihai
 */

#ifndef SRC_WORKSPACE_H_
#define SRC_WORKSPACE_H_

extern int NumWorkSpaceInt;
extern int *WorkSpaceInt;
extern int *WorkSpaceIntNow;

extern int NumWorkSpaceDouble;
extern double *WorkSpaceDouble;
extern double *WorkSpaceDoubleNow;

extern int NumWorkSpaceThreadInt;
extern int **WorkSpaceThreadInt;
extern int **WorkSpaceThreadIntNow;

extern int NumWorkSpaceThreadDouble;
extern double **WorkSpaceThreadDouble;
extern double **WorkSpaceThreadDoubleNow;

void initializeWorkSpaceAll();
void FreeWorkSpaceAll();

void RequestWorkSpaceInt(int n);
int* GetWorkSpaceInt(int n);
void ReleaseWorkSpaceInt();

void RequestWorkSpaceDouble(int n);
double* GetWorkSpaceDouble(int n);
void ReleaseWorkSpaceDouble();

void RequestWorkSpaceThreadInt(int n);
int* GetWorkSpaceThreadInt(int n);
void ReleaseWorkSpaceThreadInt();

void RequestWorkSpaceThreadDouble(int n);
double* GetWorkSpaceThreadDouble(int n);
void ReleaseWorkSpaceThreadDouble();

#endif /* SRC_WORKSPACE_H_ */
