/*-------------------------------------------------------------
 * Variational Monte Carlo
 * timer program
 * "-lrt" option is needed for clock_gettime().
 *-------------------------------------------------------------
 * by Satoshi Morita
 *-------------------------------------------------------------*/

#include "vmcmain.h"

#include "vmctime.h"

using namespace std;

void OutputTime(int step) {
  time_t tx;
  double pHop,pEx;

  tx = time(NULL);
  if(step==0) {
    fprintf(FileTime, "%05d  acc_hop acc_ex  n_hop    n_ex     : %s", step, ctime(&tx));
  } else {
    pHop = (Counter[0] == 0) ? 0.0 : (double)Counter[1] / (double)Counter[0];
    pEx  = (Counter[2] == 0) ? 0.0 : (double)Counter[3] / (double)Counter[2];
    fprintf(FileTime, "%05d  %.5lf %.5lf %-8d %-8d : %s", step, pHop,pEx,
            Counter[0], Counter[2], ctime(&tx));
  }
}

void InitTimer() {
  int i;
  for(i=0;i<NTimer;i++) Timer[i]=0.0;
  for(i=0;i<NTimer;i++) TimerStart[i]=0.0;
  return;
}

void StartTimer(int n) {
	TimerStart[n]=MPI_Wtime();
  return;
}

void StopTimer(int n) {
  Timer[n] += MPI_Wtime() - TimerStart[n];
  return;
}

void OutputTimerParaOpt() {
  char fileName[D_FileNameMax];
  FILE *fp;
  sprintf(fileName, "%s_HitachiTimer.dat", CDataFileHead); 
  fp = fopen(fileName, "w");

  fprintf(fp,"All                         [0] %12.5lf\n",Timer[0]);
  fprintf(fp,"Initialization              [1] %12.5lf\n",Timer[1]);
  fprintf(fp,"  read options              [10] %12.5lf\n",Timer[10]);
  fprintf(fp,"  ReadDefFile               [11] %12.5lf\n",Timer[11]);
  fprintf(fp,"  SetMemory                 [12] %12.5lf\n",Timer[12]);
  fprintf(fp,"  InitParameter             [13] %12.5lf\n",Timer[13]);
  fprintf(fp,"VMCParaOpt                  [2] %12.5lf\n",Timer[2]);
  fprintf(fp,"  VMCMakeSample             [3] %12.5lf\n",Timer[3]);
  fprintf(fp,"    makeInitialSample       [30] %12.5lf\n",Timer[30]);
  fprintf(fp,"    make candidate          [31] %12.5lf\n",Timer[31]);
  fprintf(fp,"    hopping update          [32] %12.5lf\n",Timer[32]);
  fprintf(fp,"      UpdateProjCnt         [60] %12.5lf\n",Timer[60]);
  fprintf(fp,"      CalculateNewPfM2      [61] %12.5lf\n",Timer[61]);
  fprintf(fp,"      CalculateLogIP        [62] %12.5lf\n",Timer[62]);
  fprintf(fp,"      UpdateMAll            [63] %12.5lf\n",Timer[63]);
  fprintf(fp,"      TNS LogRatio 1 e      [64] %12.5lf\n",Timer[64]);
  fprintf(fp,"        prepare             [641] %12.5lf\n",Timer[641]);
  fprintf(fp,"        contraction         [642] %12.5lf\n",Timer[642]);
  fprintf(fp,"        recomputeweight     [643] %12.5lf\n",Timer[643]);
  fprintf(fp,"        contractLeaves      [644] %12.5lf\n",Timer[644]);
  fprintf(fp,"        get branch          [645] %12.5lf\n",Timer[645]);
  fprintf(fp,"        store new branch    [646] %12.5lf\n",Timer[646]);
  fprintf(fp,"        isIncluded          [647] %12.5lf\n",Timer[647]);

  fprintf(fp,"    exchange update         [33] %12.5lf\n",Timer[33]);
  fprintf(fp,"      UpdateProjCnt         [65] %12.5lf\n",Timer[65]);
  fprintf(fp,"      CalculateNewPfMTwo2   [66] %12.5lf\n",Timer[66]);
  fprintf(fp,"      CalculateLogIP        [67] %12.5lf\n",Timer[67]);
  fprintf(fp,"      UpdateMAllTwo         [68] %12.5lf\n",Timer[68]);
  fprintf(fp,"      TNS LogRatio 2 e      [69] %12.5lf\n",Timer[69]);
  fprintf(fp,"    recal PfM and InvM      [34] %12.5lf\n",Timer[34]);
  fprintf(fp,"    save electron config    [35] %12.5lf\n",Timer[35]);
  fprintf(fp,"  VMCMainCal                [4] %12.5lf\n",Timer[4]);
  fprintf(fp,"    CalculateMAll           [40] %12.5lf\n",Timer[40]);
  fprintf(fp,"    LocEnergyCal            [41] %12.5lf\n",Timer[41]);
  fprintf(fp,"      CalHamiltonian0       [70] %12.5lf\n",Timer[70]);
  fprintf(fp,"      CalHamiltonian1       [71] %12.5lf\n",Timer[71]);
  fprintf(fp,"      CalHamiltonian2       [72] %12.5lf\n",Timer[72]);
  fprintf(fp,"    derivative              [42] %12.5lf\n",Timer[42]);
  fprintf(fp,"    calculate OO and HO     [43] %12.5lf\n",Timer[43]);
  fprintf(fp,"  StochasticOpt             [5] %12.5lf\n",Timer[5]);
  fprintf(fp,"    preprocess              [50] %12.5lf\n",Timer[50]);
  fprintf(fp,"    stcOptMain              [51] %12.5lf\n",Timer[51]);
  fprintf(fp,"      initBLACS             [55] %12.5lf\n",Timer[55]);
  fprintf(fp,"      calculate S and g     [56] %12.5lf\n",Timer[56]);
  fprintf(fp,"      DPOSV                 [57] %12.5lf\n",Timer[57]);
  fprintf(fp,"      gatherParaChange      [58] %12.5lf\n",Timer[58]);
  fprintf(fp,"    postprocess             [52] %12.5lf\n",Timer[52]);
  fprintf(fp,"  UpdateSlaterElm           [20] %12.5lf\n",Timer[20]);
  fprintf(fp,"  WeightAverage             [21] %12.5lf\n",Timer[21]);
  fprintf(fp,"  outputData                [22] %12.5lf\n",Timer[22]);
  fprintf(fp,"  SyncModifiedParameter     [23] %12.5lf\n",Timer[23]);

  fprintf(fp,"  TNS total                 [100] %12.5lf\n",Timer[100]);
  fprintf(fp,"  TNS creation              [101] %12.5lf\n",Timer[101]);
  fprintf(fp,"  TNS compute weight        [102] %12.5lf\n",Timer[102]);
  fprintf(fp,"  TNS compute ratio         [104] %12.5lf\n",Timer[104]);
  fprintf(fp,"  TNS derivative            [103] %12.5lf\n",Timer[103]);

  fclose(fp);
}

void OutputTimerPhysCal() {
  char fileName[D_FileNameMax];
  FILE *fp;
  sprintf(fileName, "%s_HitachiTimer.dat", CDataFileHead); 
  fp = fopen(fileName, "w");

  fprintf(fp,"All                         [0] %12.5lf\n",Timer[0]);
  fprintf(fp,"Initialization              [1] %12.5lf\n",Timer[1]);
  fprintf(fp,"  read options             [10] %12.5lf\n",Timer[10]);
  fprintf(fp,"  ReadDefFile              [11] %12.5lf\n",Timer[11]);
  fprintf(fp,"  SetMemory                [12] %12.5lf\n",Timer[12]);
  fprintf(fp,"  InitParameter            [13] %12.5lf\n",Timer[13]);
  fprintf(fp,"VMCPhysCal                  [2] %12.5lf\n",Timer[2]);
  fprintf(fp,"  VMCMakeSample             [3] %12.5lf\n",Timer[3]);
  fprintf(fp,"    makeInitialSample      [30] %12.5lf\n",Timer[30]);
  fprintf(fp,"    make candidate         [31] %12.5lf\n",Timer[31]);
  fprintf(fp,"    hopping update         [32] %12.5lf\n",Timer[32]);
  fprintf(fp,"      UpdateProjCnt        [60] %12.5lf\n",Timer[60]);
  fprintf(fp,"      CalculateNewPfM2     [61] %12.5lf\n",Timer[61]);
  fprintf(fp,"      CalculateLogIP       [62] %12.5lf\n",Timer[62]);
  fprintf(fp,"      UpdateMAll           [63] %12.5lf\n",Timer[63]);
  fprintf(fp,"    exchange update        [33] %12.5lf\n",Timer[33]);
  fprintf(fp,"      UpdateProjCnt        [65] %12.5lf\n",Timer[65]);
  fprintf(fp,"      CalculateNewPfMTwo2  [66] %12.5lf\n",Timer[66]);
  fprintf(fp,"      CalculateLogIP       [67] %12.5lf\n",Timer[67]);
  fprintf(fp,"      UpdateMAllTwo        [68] %12.5lf\n",Timer[68]);
  fprintf(fp,"    recal PfM and InvM     [34] %12.5lf\n",Timer[34]);
  fprintf(fp,"    save electron config   [35] %12.5lf\n",Timer[35]);
  fprintf(fp,"  VMCMainCal                [4] %12.5lf\n",Timer[4]);
  fprintf(fp,"    CalculateMAll          [40] %12.5lf\n",Timer[40]);
  fprintf(fp,"    LocEnergyCal           [41] %12.5lf\n",Timer[41]);
  fprintf(fp,"      CalHamiltonian0      [70] %12.5lf\n",Timer[70]);
  fprintf(fp,"      CalHamiltonian1      [71] %12.5lf\n",Timer[71]);
  fprintf(fp,"      CalHamiltonian2      [72] %12.5lf\n",Timer[72]);
  fprintf(fp,"    CalculateGreenFunc     [42] %12.5lf\n",Timer[42]);
  fprintf(fp,"      GreenFunc1           [50] %12.5lf\n",Timer[50]);
  fprintf(fp,"      GreenFunc2           [51] %12.5lf\n",Timer[51]);
  fprintf(fp,"      addPhysCA            [52] %12.5lf\n",Timer[52]);
  fprintf(fp,"      addPhysCACA          [53] %12.5lf\n",Timer[53]);
  fprintf(fp,"    Lanczos1               [43] %12.5lf\n",Timer[43]);
  fprintf(fp,"    Lanczos2               [44] %12.5lf\n",Timer[44]);
  fprintf(fp,"  UpdateSlaterElm          [20] %12.5lf\n",Timer[20]);
  fprintf(fp,"  WeightAverage            [21] %12.5lf\n",Timer[21]);
  fprintf(fp,"  outputData               [22] %12.5lf\n",Timer[22]);

  fclose(fp);
}
