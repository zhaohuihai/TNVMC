/*-------------------------------------------------------------
 * Variational Monte Carlo
 * matrix Package (LAPACK and Pfapack)
 *-------------------------------------------------------------
 * by Satoshi Morita
 *-------------------------------------------------------------*/

#include <math.h>

#include "vmcmain.h"
#include <iostream>
#include <fstream>
#include "workspace.h"
#include "mantissa_exponent.h"
#include "vmcmake.h"
#include "matrix.h"

using namespace std ;

int getLWork() {
  char uplo='U', mthd='P';
  int n,lda,lwork,info=0;
  double pfaff;
  int iwork;
  double a;
  double optSize1,optSize2;

  /* ask the optimal size of work */
  n=lda=Nsize;
  lwork=-1;
//  M_DGETRI(&n, &a, &lda, &iwork, &optSize1, &lwork, &info);
  dgetri_(&n, &a, &lda, &iwork, &optSize1, &lwork, &info);
  lwork=-1;
//  M_DSKPFA(&uplo, &mthd, &n, &a, &lda, &pfaff, &iwork, &optSize2, &lwork, &info);
  dskpfa_(&uplo, &mthd, &n, &a, &lda, &pfaff, &iwork, &optSize2, &lwork, &info);

  lwork = (optSize1>optSize2) ? (int)optSize1 : (int)optSize2;
  return lwork;
}

/* Calculate PfM and InvM from qpidx=qpStart to qpEnd */
int CalculateMAll(const int *eleIdx, const int qpStart, const int qpEnd) {
  const int qpNum = qpEnd-qpStart;
  int qpidx;

  int info = 0;

  double *myBufM;
  double *myWork;
  int *myIWork;
  int myInfo;

  RequestWorkSpaceThreadInt(Nsize);
  RequestWorkSpaceThreadDouble(Nsize*Nsize+LapackLWork);

  #pragma omp parallel default(shared)              \
    private(myIWork,myWork,myInfo,myBufM)
  {
    myIWork = GetWorkSpaceThreadInt(Nsize);
    myBufM = GetWorkSpaceThreadDouble(Nsize*Nsize);
    myWork = GetWorkSpaceThreadDouble(LapackLWork);

    #pragma omp for private(qpidx)
    for(qpidx=0;qpidx<qpNum;qpidx++) { // loop for all quantum number projections
      if(info!=0) continue;
      
      myInfo = calculateMAll_child(eleIdx, qpStart, qpEnd, qpidx,
                                   myBufM, myIWork, myWork, LapackLWork);
      if(myInfo!=0) {
        #pragma omp critical
        info=myInfo;
      }
    }
  }
  ReleaseWorkSpaceThreadInt();
  ReleaseWorkSpaceThreadDouble();
  return info;
}

int calculateMAll_child(const int *eleIdx, const int qpStart, const int qpEnd, const int qpidx,
                        double *bufM, int *iwork, double *work, int lwork) {
  #pragma procedure serial
  /* const int qpNum = qpEnd-qpStart; */
  int msi,msj;
  int rsi,rsj;

  char uplo='U'; // 'U':  Upper triangle of A is stored;
  char mthd='P'; // 'P': Compute Pfaffian using Parlett-Reid algorithm (recommended)
  //  n: The order of the matrix A.  n >= 0.
  int m,n,lda,info=0;
  double pfaff;

  /* optimization for Kei */
  const int nsize = Nsize;

  const double *sltE = SlaterElm + (qpidx+qpStart)*Nsite2*Nsite2;
  const double *sltE_i;

  double *invM = InvM + qpidx*Nsize*Nsize;
  double *invM_i;


  double *bufM_i, *bufM_i2;

  m=n=lda=Nsize;

  /* store bufM */
  /* Note that bufM is column-major and skew-symmetric. */
  /* bufM[msj][msi] = -sltE[rsi][rsj] */
  #pragma loop noalias
  for(msi=0;msi<nsize;msi++) {
    rsi = eleIdx[msi] + (msi/Ne)*Nsite;
    bufM_i = bufM + msi*Nsize;
    sltE_i = sltE + rsi*Nsite2;
    #pragma loop norecurrence
    for(msj=0;msj<nsize;msj++) {
      rsj = eleIdx[msj] + (msj/Ne)*Nsite;
      bufM_i[msj] = -sltE_i[rsj];
    }
  }
  //***********************************************************
  double M_norm1 = compute_norm1(bufM, Nsize, qpidx) ;
  //***********************************************************
  double *Mqp = M + qpidx*Nsize*Nsize;
  /* copy bufM to invM */
  /* For Pfaffian calculation, invM is used as second buffer */
  #pragma loop noalias
  for(msi=0;msi<nsize*nsize;msi++) {
    invM[msi] = bufM[msi];
    Mqp[msi] = bufM[msi]; // column-major
  }
  /* calculate Pf M */
  dskpfa_(&uplo, &mthd, &n, invM, &lda, &pfaff, iwork, work, &lwork, &info);
  if(info!=0) return info;
  if(!isfinite(pfaff)) return qpidx+1;
  PfM[qpidx] = pfaff;
  if ( fabs(pfaff) < Pf_thresh ) { return 1 ;}
  /* DInv */
  dgetrf_(&m, &n, bufM, &lda, iwork, &info); /* ipiv = iwork */
  if(info!=0) return info;
  
  dgetri_(&n, bufM, &lda, iwork, work, &lwork, &info);
  if(info!=0) return info;
  //***********************************************************
//  McondNum[qpidx] = compute_condNum_norm1(eleIdx, qpStart, qpEnd, qpidx, bufM) ;
  double Minv_norm1 = compute_norm1(bufM, Nsize) ;
  McondNum[qpidx] = M_norm1 * Minv_norm1 ;
  //***********************************************************
  /* store InvM */
  /* BufM is column-major, InvM is row-major */
  #pragma loop noalias
  for(msi=0;msi<nsize;msi++) {
    invM_i = invM + msi*Nsize;
    bufM_i = bufM + msi*Nsize;
    bufM_i2 = bufM + msi;
    for(msj=0;msj<nsize;msj++) {
    	// bufM_i2[i + j * Nsize]
    	// bufM_i[j + i * Nsize]
    	// skew symmetrize invM
      invM_i[msj] = 0.5*(bufM_i2[msj*nsize] - bufM_i[msj]);
      /* invM[i][j] = 0.5*(bufM[i][j]-bufM[j][i]) */
    }
  }
  return info;
}

double calculatePfaffian(const int *eleIdx, const int qpStart, const int qpEnd, const int qpidx) {
	#pragma procedure serial

	vector<int> iwork(Nsize) ;
	vector<double> bufM(Nsize*Nsize) ;
	vector<double> work(LapackLWork) ;

  /* const int qpNum = qpEnd-qpStart; */
  int msi,msj;
  int rsi,rsj;

  char uplo='U'; // 'U':  Upper triangle of A is stored;
  char mthd='P'; // 'P': Compute Pfaffian using Parlett-Reid algorithm (recommended)
  //  n: The order of the matrix A.  n >= 0.
  int m,n,lda,info=0;
  double pfaff;

  /* optimization for Kei */
  const int nsize = Nsize;

  const double *sltE = SlaterElm + (qpidx+qpStart)*Nsite2*Nsite2;
  const double *sltE_i;

  double *bufM_i, *bufM_i2;

  m=n=lda=Nsize;

  /* store bufM */
  /* Note that bufM is column-major and skew-symmetric. */
  /* bufM[msj][msi] = -sltE[rsi][rsj] */
  #pragma loop noalias
  for(msi=0;msi<nsize;msi++) {
    rsi = eleIdx[msi] + (msi/Ne)*Nsite;
    bufM_i = &(bufM[0]) + msi*Nsize;
    sltE_i = sltE + rsi*Nsite2;
    #pragma loop norecurrence
    for(msj=0;msj<nsize;msj++) {
      rsj = eleIdx[msj] + (msj/Ne)*Nsite;
      bufM_i[msj] = -sltE_i[rsj];
    }
  }

  int lwork = LapackLWork ;
  /* calculate Pf M */
  dskpfa_(&uplo, &mthd, &n, &(bufM[0]), &lda, &pfaff, &(iwork[0]), &(work[0]), &lwork, &info);

  return pfaff;
}

double compute_condNum(double *A, int m) {
	char jobu = 'N' ;
	char jobvt = 'N' ;
	int n = m ; // The number of columns in A
	int lda = m ; // The leading dimension of  A
	int ldu = m ; // The leading dimensions of U
	int ldvt = m ; // The leading dimensions of VT

	vector<double> s(m, 0.0) ;
	vector<double> u(1, 0.0) ;
	vector<double> vt(1, 0.0) ;

	int info ;
	//************************************************
	// workspace query
	vector<double> work(1, 0.0) ;
	int lwork = -1 ;

	dgesvd_(&jobu, &jobvt, &m, &n, A, &lda, &(s[0]), &(u[0]), &ldu,
			&(vt[0]), &ldvt, &(work[0]), &lwork, &info) ;
	//************************************************
	// computation
	lwork = (int)work[0] + 1 ;
	work.resize(lwork) ;

	dgesvd_(&jobu, &jobvt, &m, &n, A, &lda, &(s[0]), &(u[0]), &ldu,
			&(vt[0]), &ldvt, &(work[0]), &lwork, &info) ;

	return (s[0] / s[m-1]) ;
}

// A is a column major m x m matrix
double compute_norm1(const double *A, const int m) {
	double val = 0.0 ;
	double col_sum ;
	const double* Aj ;
	// A(i,j)
	for ( int j = 0; j < m; j ++ ) {
		Aj = A + j * m ;
		col_sum = xasum(m, Aj) ;

		val = max(val, col_sum) ;
	}
	return val ;
}

double compute_norm1(const double *A, const int m, const int qpidx) {
	double* col_sum = Mcol_sum + qpidx * Nsize ;
	double val = 0.0 ;
	const double *Aj ;
	// A(i, j)
	for ( int j = 0; j < m; j ++ ) {
		Aj = A + j * m ;
		col_sum[j] = xasum(m, Aj) ;
		val = max(val, col_sum[j]) ;
	}
	return val ;
}

double compute_condNum_norm1(const int *eleIdx,
		const int qpStart, const int qpEnd, const int qpidx, double* invM) {
	int msi,msj;
	int rsi,rsj;
	double M_norm1, Minv_norm1 ;
  const double *sltE = SlaterElm + (qpidx+qpStart)*Nsite2*Nsite2;
  vector<double> M_new(Nsize*Nsize, 0.0) ;
  int iN, iN2 ;
  // M_new[msj][msi]
  for(msi=0;msi<Nsize;msi++) {
  	rsi = eleIdx[msi] + (msi/Ne)*Nsite;
		iN = msi * Nsize ;
		iN2 = rsi * Nsite2 ;
		for(msj=0;msj<Nsize;msj++) {
			rsj = eleIdx[msj] + (msj/Ne)*Nsite;
			M_new[iN + msj] = -sltE[iN2 + rsj];
		}
  }
  M_norm1 = compute_norm1(&(M_new[0]), Nsize) ;

  Minv_norm1 = compute_norm1(invM, Nsize) ;

  return (M_norm1 * Minv_norm1) ;
}

void output_array(const double* a, const int len, const std::string filename) {
	ofstream outfile( &(filename[0]), ios::out ) ;
	if (!outfile) {
		cerr << "output_array: open error!" << endl ;
		exit(0) ;
	}
	outfile.precision(30) ;
	for ( int i = 0; i < len; i ++ ) {
		outfile << a[i] << endl ;
	}
}
