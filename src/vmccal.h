/*
 * vmccal.h
 *
 *  Created on: 2015年3月22日
 *      Author: zhaohuihai
 */

#ifndef SRC_VMCCAL_H_
#define SRC_VMCCAL_H_

#include "correlator_product_state.h"
#include "fat_tree_tensor_network.h"

#include <vector>

void VMCMainCal(MPI_Comm comm, CorrelatorProductState& peps) ;
void VMCMainCal(MPI_Comm comm, FatTreeTensorNetwork& fttn) ;

void VMCMainCal_sym(MPI_Comm comm, FatTreeTensorNetwork& fttn) ;
void VMCMainCal_symCorFactor(MPI_Comm comm, FatTreeTensorNetwork& fttn) ;

void output_energy_sample(std::vector<double>& energy_sample) ;

void clearPhysQuantity();
void calculateOptTransDiff(double *srOptO, const double ipAll);
void calculateOO_Store(double *srOptOO, double *srOptHO, double *srOptO_Store,
                 const double w, const double e, int srOptSize, int sampleSize) ;
void calculateOO(double *srOptOO, double *srOptHO, const double *srOptO,
                 const double w, const double local_energy, const int srOptSize);

void calculateO(double *srOpt_total, const double *srOptO, const int srOptSize) ;
void calculateO(double *srOpt_total, const double *srOptO, const int *srOptO_ind, const int srOptSize_sparse) ;

void calculateHO(double *srOptHO, const double *srOptO, const double w,
								 const double local_energy, const int srOptSize) ;
void calculateHO(double *srOptHO, const double *srOptO, const int *srOptO_ind, const double w,
								 const double local_energy, const int srOptSize_sparse) ;

void calculateOOdiag(double *srOptOOdiag, const double *srOptO, const int srOptSize) ;
void calculateOOdiag(double *srOptOOdiag, const double *srOptO, const int *srOptO_ind, const int srOptSize_sparse) ;

void calculateOpowDiag(double *srOptOpowDiag, const double *srOptO, const double p, const int srOptSize) ;

void calculateQQQQ(double *qqqq, const double *lslq, const double w, const int nLSHam);
void calculateQCAQ(double *qcaq, const double *lslca, const double *lslq,
                   const double w, const int nLSHam, const int nCA);
void calculateQCACAQ(double *qcacaq, const double *lslca, const double w,
                     const int nLSHam, const int nCA, const int nCACA,
                     int **cacaIdx);

double findStepWid_LS(MPI_Comm comm) ;
void find1stepLSenergy(MPI_Comm comm) ;

#endif /* SRC_VMCCAL_H_ */
