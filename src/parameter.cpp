/*-------------------------------------------------------------
 * Variational Monte Carlo
 * Variational parameters
 *-------------------------------------------------------------
 * by Satoshi Morita
 *-------------------------------------------------------------*/

#include <math.h>

#include "vmcmain.h"

#include "sfmt/SFMT.h"
#include "splitloop.h"
#include "parameter.h"

using namespace std ;

/* initialize variational parameters */
void InitParameter() {
  int i;

  #pragma omp parallel for default(shared) private(i)
  for(i=0;i<NProj;i++) Proj[i] = 0.0;

  for(i=0;i<NSlater;i++){
//    if(OptFlag[i+NProj] > 0){
//      Slater[i] = genrand_real2(); /* uniform distribution [0,1) */
//    } else {
//      Slater[i] = genrand_real2();
////      Slater[i] = 0.0;
//    }
    do {
    	Slater[i] = genrand_real2() - 0.5;
    } while ( fabs(Slater[i]) < 0.1 ) ;
  }

  for(i=0;i<NOptTrans;i++){
    OptTrans[i] = ParaQPOptTrans[i];
  }

  // initialize all TNS elements
  for (i = 0; i < NPEPSelem; i ++) {
  	if (initTNStype == 0) {
  		PEPSelem[i] = 1.0 ;
  	}
  	else if ( initTNStype == 1 ) {
  		if ( genrand_real2() > 0.5 ) PEPSelem[i] = 1.0 ;
  		else PEPSelem[i] = -1.0 ;
  	}
  	else if ( initTNStype == 2 ) {
  		PEPSelem[i] = genrand_real2() - 0.5 + randShift ;
  	}
  }

  for (i = 0; i < NPara; i ++) ParaChange[i] = 0.0 ;

  return;
}

/* read initial vaules of variational parameters from initFile */
int ReadInitParameter(char *initFile) {
  FILE *fp;
  int i,xi;
  double ptmp, xtmp;
  int flag ;
  int nmps ; // numm of para in SBS
  // count num of para in file
  fp = fopen(initFile, "r");
  if(fp!=NULL){
    while(fscanf(fp, "%lf ", &xtmp)!=EOF)
    {
      for(i=1;i<4;i++) fscanf(fp, "%lf ", &xtmp);
      for(xi=0;xi<NProj;xi++) {
        fscanf(fp, "%lf %lf ", &ptmp, &xtmp);
      }
      for(xi=0;xi<NSlater;xi++) {
        fscanf(fp, "%lf %lf ", &ptmp, &xtmp);
      }
      for(xi=0;xi<NOptTrans;xi++) {
        fscanf(fp, "%lf %lf ", &ptmp, &xtmp);
      }
      nmps = 0 ;
      for(xi=0;xi<NPEPSelem;xi++)
      {
      	flag = fscanf(fp, "%lf %lf ", &ptmp, &xtmp) ;
      	if (flag != EOF) {
      		nmps ++ ;
      	}
      }
    }
    fclose(fp);
  } else { fprintf(stderr, "Error: %s does not exist.\n",initFile); }
  // read values
  int dim = sqrt((double)(nmps / (Nsite*siteDim))) ;
  fp = fopen(initFile, "r");
  while(fscanf(fp, "%lf ", &xtmp)!=EOF)
  {
    for(i=1;i<4;i++) fscanf(fp, "%lf ", &xtmp);
    for(xi=0;xi<NProj;xi++) {
      fscanf(fp, "%lf %lf ", &(Proj[xi]), &xtmp);
    }
    for(xi=0;xi<NSlater;xi++) {
      fscanf(fp, "%lf %lf ", &(Slater[xi]), &xtmp);
    }
    for(xi=0;xi<NOptTrans;xi++) {
      fscanf(fp, "%lf %lf ", &(OptTrans[xi]), &xtmp);
    }
    for(xi = 0; xi < nmps; xi++)
    {
    	flag = fscanf(fp, "%lf %lf ", &(PEPSelem[xi]), &xtmp) ;
//    	flag = fscanf(fp, "%lf %lf ", &ptmp, &xtmp) ;
//    	int p = xi ;
//    	int row = p % dim ; // row index
//    	p /= dim ;
//    	int col = p % dim ; // column index
//    	p /= dim ;
//    	int physNo = p % siteDim ; // physical index
//    	int siteNo = p / siteDim ; // site number
//
//    	int p1 = row + col * bondDim +
//    					 physNo * bondDim * bondDim +
//							 siteNo * bondDim * bondDim * siteDim ;
//    	SBSelem[p1] = ptmp ;
    }
  }
  fclose(fp) ;


//  for (i = 0; i < NPara; i ++)
//  {
//  	cout << "para " << setw(5) << i << ", " << setw(11) << Para[i] << ", " ;
//  	if ( (i % 3) == 2 ) cout << endl ;
//  }
//  cout << endl ;
  return 0;
}

int ReadInitParameter(char *initFile, FatTreeTensorNetwork& fttn) {
	FILE *fp;
	int i, xi;
	double ptmp, xtmp;
	int flag ;
	int NfileTNelem ; // number of tensor network parameters in file for initial wave function
	// count num of para in file
	fp = fopen(initFile, "r");
	if ( fp!=NULL ) {
    while(fscanf(fp, "%lf ", &xtmp)!=EOF)
    {
      for(i=1;i<4;i++) fscanf(fp, "%lf ", &xtmp);
      for(xi=0;xi<NProj;xi++) {
        fscanf(fp, "%lf %lf ", &ptmp, &xtmp);
      }
      for(xi=0;xi<NSlater;xi++) {
        fscanf(fp, "%lf %lf ", &ptmp, &xtmp);
      }
      for(xi=0;xi<NOptTrans;xi++) {
        fscanf(fp, "%lf %lf ", &ptmp, &xtmp);
      }
      NfileTNelem = 0 ;
      for(xi=0;xi<NPEPSelem;xi++) {
      	flag = fscanf(fp, "%lf %lf ", &ptmp, &xtmp) ;
      	if (flag != EOF) {
      		NfileTNelem ++ ;
      	} else break ;
      }
    }
    fclose(fp);
	} else { fprintf(stderr, "Error: %s does not exist.\n",initFile); }
	cout << "TN parameters number in initial wave function file: " << NfileTNelem << endl ;
	int dim = findBondDim(NfileTNelem) ;
	// read values
	fp = fopen(initFile, "r");
	while(fscanf(fp, "%lf ", &xtmp)!=EOF) {
    for(i=1;i<4;i++) fscanf(fp, "%lf ", &xtmp);
    for(xi=0;xi<NProj;xi++) {
      fscanf(fp, "%lf %lf ", &(Proj[xi]), &xtmp);
    }
    for(xi=0;xi<NSlater;xi++) {
      fscanf(fp, "%lf %lf ", &(Slater[xi]), &xtmp);
    }
    for(xi=0;xi<NOptTrans;xi++) {
      fscanf(fp, "%lf %lf ", &(OptTrans[xi]), &xtmp);
    }
    //-------------------------------------------------------
    int inode = 0 ;
    // assign leaf node value
    int nElemLeaf = pow((double)siteDim, (double)FTTN_NleafSites) * dim ;
    vector<double> nodeFile( nElemLeaf ) ;
    for ( i = 0; i < fttn.nleaves(); i ++ ) {
    	if ( fttn.ti()[inode] == false ) { // if it is not translated copy
    		for ( xi = 0; xi < nElemLeaf; xi ++ ) {
    			fscanf(fp, "%lf %lf ", &(nodeFile[xi]), &xtmp);
    		}
    		fttn.assign(inode, dim, nodeFile) ;
    	}
    	inode ++ ;
    }
    // assign internal node value
    int nElemInterNode = pow((double)dim, (double)FTTN_inNodeRank) ;
    nodeFile.resize(nElemInterNode) ;
    for ( i = 0; i < fttn.nInterNodes(); i ++ ) {
    	if ( fttn.ti()[inode] == false ) { // if it is not translated copy
    		for ( xi = 0; xi < nElemInterNode; xi ++ ) {
    			fscanf(fp, "%lf %lf ", &(nodeFile[xi]), &xtmp);
    		}
    		fttn.assign(inode, dim, nodeFile) ;
    	}
    	inode ++ ;
    }
    // assign root node value
    int nElemRootNode = pow((double)dim, (double)(FTTN_inNodeRank-1)) ;
    nodeFile.resize(nElemRootNode) ;
    for ( xi = 0; xi < nElemRootNode; xi ++ ) {
    	fscanf(fp, "%lf %lf ", &(nodeFile[xi]), &xtmp);
    }
    fttn.assign(inode, dim, nodeFile) ;
	}
	fclose(fp) ;
	return 0;
}

int findBondDim(int NfileTNelem) {
	int fileBondDim = 0 ;
	int nElem = 0 ; // count number of parameters
	while (nElem != NfileTNelem) {
		fileBondDim ++ ;
		cout << "trial bond dim: " << fileBondDim << endl ;
		// ----------------leaf nodes----------------
		int ucx, ucy, lx, ly ;
		if ( length_x / 2 == 1 ) {
			ucx = 1 ;
			lx = 1 ;
		} else {
  		ucx = unitCell_x ;
  		lx = length_x ;
		}
		if ( length_y / 2 == 1 ) {
			ucy = 1 ;
			ly = 1 ;
		} else {
  		ucy = unitCell_y ;
  		ly = length_y ;
		}
		int nsites_unitCell = ucx * ucy ;
		nElem = nsites_unitCell * pow((double)siteDim, (double)FTTN_NleafSites) * fileBondDim ;
		// ----------------internal nodes----------------
		int r = FTTN_inNodeRank - 1 ; // reduce factor
		int nnodes_eachLevel = lx * ly ; //
		int height = 0 ;
		while (nnodes_eachLevel > r) {
  		if ( height % 2 == 0 ) // coarse-grain along x direction
  			ucx = max(ucx / 2, 1) ;
  		else // coarse-grain along y direction
  			ucy = max(ucy / 2, 1) ;
  		nsites_unitCell = ucx * ucy ;
  		nElem += nsites_unitCell * pow((double)fileBondDim, (double)FTTN_inNodeRank) ;

  		nnodes_eachLevel /= r ;
  		height ++ ;
		}
		// ----------------root node----------------
		nElem += pow((double)fileBondDim, (double)(FTTN_inNodeRank-1)) ;
	}
	cout << "TN bond dim in file: " << fileBondDim << endl ;

	return fileBondDim ;
}

/* sync and modify variational parameters */
void SyncModifiedParameter(MPI_Comm comm) {
  double gShift=0.0;
  double xmax,ratio;
  int i;

#ifdef _mpi_use
  int size;
  MPI_Comm_size(comm, &size);
  if(size>1) MPI_Bcast(Para, NPara, MPI_DOUBLE, 0, comm);
#endif /* _mpi_use */

  /***** shift correlation factors *****/
  /* shift the DH correlation factors */
  if(FlagShiftDH2==1) gShift += shiftDH2();
  if(FlagShiftDH4==1) gShift += shiftDH4();
//  std::cout << "gShift " << gShift << std::endl ;
  /* shift the Gutzwiller factors */
  for(i=0;i<NGutzwillerIdx;i++) Proj[i] += gShift;
  /* shift the Gutzwiller-Jastrow factors */
  if(FlagShiftGJ==1) shiftGJ();

  /***** rescale Slater *****/
  xmax = fabs(Slater[0]);
  for(i=1;i<NSlater;i++){
    if(xmax < fabs(Slater[i])) xmax = fabs(Slater[i]);
  }
  ratio = D_AmpMax/xmax;
  #pragma omp parallel for default(shared) private(i)
  for(i=0;i<NSlater;i++) Slater[i] *= ratio;

  /***** normalize OptTrans *****/
  if(FlagOptTrans>0){
    xmax = fabs(OptTrans[0]);
    for(i=1;i<NOptTrans;++i) {
      if(xmax < fabs(OptTrans[i])) xmax = fabs(OptTrans[i]);
    }
    ratio = 1.0/copysign(xmax, OptTrans[0]);
    #pragma omp parallel for default(shared) private(i)
    for(i=0;i<NOptTrans;++i) {
      OptTrans[i] *= ratio;
    }
  }

  // rescale SBS
  xmax = fabs(PEPSelem[0]) ;
  for ( i = 1; i < NPEPSelem; i ++ )
  {
  	if ( xmax < fabs(PEPSelem[i]) )
  	{
  		xmax = fabs(PEPSelem[i]) ;
  	}
  }
  ratio = D_AmpMax / xmax ;
	#pragma omp parallel for default(shared) private(i)
  for(i = 0; i < NPEPSelem; i ++)
  {
  	PEPSelem[i] *= ratio;
  }
  return;
}

void SyncModifiedParameter(MPI_Comm comm, CorrelatorProductState& cps)
{
  double gShift=0.0;
  double xmax,ratio;
  int i;

#ifdef _mpi_use
  int size;
  MPI_Comm_size(comm, &size);
  if(size>1) MPI_Bcast(Para, NPara, MPI_DOUBLE, 0, comm);
#endif /* _mpi_use */

  /***** shift correlation factors *****/
  /* shift the DH correlation factors */
  if(FlagShiftDH2==1) gShift += shiftDH2();
  if(FlagShiftDH4==1) gShift += shiftDH4();
//  std::cout << "gShift " << gShift << std::endl ;
  /* shift the Gutzwiller factors */
  for(i=0;i<NGutzwillerIdx;i++) Proj[i] += gShift;
  /* shift the Gutzwiller-Jastrow factors */
  if(FlagShiftGJ==1) shiftGJ();

  /***** rescale Slater *****/
  xmax = fabs(Slater[0]);
  for(i=1;i<NSlater;i++){
    if(xmax < fabs(Slater[i])) xmax = fabs(Slater[i]);
  }
  ratio = D_AmpMax/xmax;
  #pragma omp parallel for default(shared) private(i)
  for(i=0;i<NSlater;i++) Slater[i] *= ratio;

  /***** normalize OptTrans *****/
  if(FlagOptTrans>0){
    xmax = fabs(OptTrans[0]);
    for(i=1;i<NOptTrans;++i) {
      if(xmax < fabs(OptTrans[i])) xmax = fabs(OptTrans[i]);
    }
    ratio = 1.0/copysign(xmax, OptTrans[0]);
    #pragma omp parallel for default(shared) private(i)
    for(i=0;i<NOptTrans;++i) {
      OptTrans[i] *= ratio;
    }
  }

  // rescale SBS
  cps.rescale(PEPSampMax) ;
  return;
}

void SyncModifiedParameter(MPI_Comm comm, FatTreeTensorNetwork& fttn) {
  double gShift=0.0;
  double xmax,ratio;
  int i;

#ifdef _mpi_use
  int size;
  MPI_Comm_size(comm, &size);
  if(size>1) {
  	MPI_Bcast(Para, NPara, MPI_DOUBLE, 0, comm);
  	MPI_Bcast(ParaChange, NPara, MPI_DOUBLE, 0, comm);
  }
#endif /* _mpi_use */

  /***** shift correlation factors *****/
  /* shift the DH correlation factors */
  if(FlagShiftDH2==1) gShift += shiftDH2();
  if(FlagShiftDH4==1) gShift += shiftDH4();
//  std::cout << "gShift " << gShift << std::endl ;
  /* shift the Gutzwiller factors */
  for(i=0;i<NGutzwillerIdx;i++) Proj[i] += gShift;
  /* shift the Gutzwiller-Jastrow factors */
  if(FlagShiftGJ==1) shiftGJ();

  /***** rescale Slater *****/
  xmax = fabs(Slater[0]);
  for(i=1;i<NSlater;i++){
    if(xmax < fabs(Slater[i])) xmax = fabs(Slater[i]);
  }
  ratio = D_AmpMax/xmax;
#pragma omp parallel for default(shared) private(i)
  for(i=0;i<NSlater;i++) Slater[i] *= ratio;

  /***** normalize OptTrans *****/
  if(FlagOptTrans>0){
    xmax = fabs(OptTrans[0]);
    for(i=1;i<NOptTrans;++i) {
      if(xmax < fabs(OptTrans[i])) xmax = fabs(OptTrans[i]);
    }
    ratio = 1.0/copysign(xmax, OptTrans[0]);
    #pragma omp parallel for default(shared) private(i)
    for(i=0;i<NOptTrans;++i) {
      OptTrans[i] *= ratio;
    }
  }
  // rescale fttn
  fttn.rescale(PEPSampMax) ;
  return;
}

void mixOptSteps(const int Optstep) {

	if (NmixPara == 1) return ; // no mix

	int n = Optstep % NmixPara ;
	// store Para in storePara
	double* para = storePara + n * NPara ; // storePara[:][n]
	copy(Para, Para + NPara, para) ;

	if ( Optstep > NmixPara ) { // mix NmixPara steps of parameters
		for ( int i = 0; i < NmixPara; i ++ ) {
			if ( i == n ) continue ; // storePara[:][n] is already added in Para
			para = storePara + i * NPara ; // storePara[:][i]
			xaxpy(NPara, 1.0, para, Para) ; // Para += para
		}
		double a = 1.0 / (double)NmixPara ;
		xscal(NPara, a, Para) ; // Para /= NmixPara
	}
}

// rescale Proj Slater and every tensor to have the same max <Ok*Ok> - <Ok><Ok>
void rescaleSROpt_max(MPI_Comm comm, FatTreeTensorNetwork& fttn) {
	if ( useSRCG ) {
		rescaleSROpt_max_CG(comm, fttn) ;
	} else {
		rescaleSROpt_max_noCG(comm, fttn) ;
	}
}

void rescaleSROpt_max_CG(MPI_Comm comm, FatTreeTensorNetwork& fttn) {
  int rank,size;
  MPI_Comm_rank(comm,&rank);
  MPI_Comm_size(comm,&size);

	vector<double> sDiagElm(NPara, 0) ;
	// S(i,i) = OOdiag[i] - O[i] * O[i]
	for ( int pi = 0; pi < NPara; pi ++) {
		sDiagElm[pi] = SROptOOdiag[pi + 1] - SROptO[pi + 1] * SROptO[pi + 1] ;
	}
	// use ProjMax to rescale Pf and TNS
	double ProjMax = 1.0 ;
	if ( OptProj ) ProjMax = findMax(NProj, sDiagElm, 0) ;

	double SlaterMax = findMax(NSlater, sDiagElm, NProj) ;
	double minSlaterMax = 1e-12 ;
	if ( SlaterMax < minSlaterMax ) {
		SlaterMax = minSlaterMax ;
	}
	double SlaterRatio = ProjMax / SlaterMax ;
	double * OSlater = SROptO + NProj + 1 ;

	double * HOSlater = SROptHO + NProj + 1 ;
	double * HOSlater_SD = SROptHO_SD + NProj + 1 ;

	double * OOdiagSlater = SROptOOdiag + NProj + 1 ;
	double * OOOdiagSlater = SROptOOOdiag + NProj + 1 ;
	double * OOOOdiagSlater = SROptOOOOdiag + NProj + 1 ;

	for ( int i = 0; i < NSlater; i ++ ) {
		OSlater[i] *= sqrt(SlaterRatio) ; // <O>

		HOSlater[i] *= sqrt(SlaterRatio) ; // <HO>
		HOSlater_SD[i] *= sqrt(SlaterRatio) ;

		OOdiagSlater[i] *= SlaterRatio ; // diagonal elements of <OO>
		OOOdiagSlater[i] *= SlaterRatio * sqrt(SlaterRatio) ; // <OOO>
		OOOOdiagSlater[i] *= SlaterRatio * SlaterRatio ; // <OOOO>

		Slater[i] /= sqrt(SlaterRatio) ; // wave function
	}

	vector<double> NodesRatio ;
	for ( int k = 0 ; k < fttn.nnodes(); k ++ ) {
		if ( fttn.ti()[k] == false ) {
			int offset = fttn.nodes()[k].offset() ;
			int nelements = fttn.nodes()[k].nelements() ;
			int para_offset = NProj + NSlater + offset ;
			double NodeMax = findMax(nelements, sDiagElm, para_offset) ;
			double minNodeMax = 1e-12 ;
			if ( NodeMax < minNodeMax ) {
				NodeMax = minNodeMax ;
//				fprintf(stderr,"warning: small NodeMax set to: %e\n",NodeMax);
			}
			double ratio = ProjMax/NodeMax ;
//			cout << k << "-th node ratio: " << ratio << endl ;
			NodesRatio.push_back( ratio ) ;

			double * ONode = SROptO + 1 + para_offset ;

			double * HONode = SROptHO + 1 + para_offset ;
			double * HONode_SD = SROptHO_SD + 1 + para_offset ;

			double * OOdiagNode = SROptOOdiag + 1 + para_offset ;
			double * OOOdiagNode = SROptOOOdiag + 1 + para_offset ;
			double * OOOOdiagNode = SROptOOOOdiag + 1 + para_offset ;

			double * ParaNode = PEPSelem + offset ;

			for ( int i = 0 ; i < nelements; i ++ ) {
				ONode[i] *= sqrt(ratio) ;

				HONode[i] *= sqrt(ratio) ;
				HONode_SD[i] *= sqrt(ratio) ;

				OOdiagNode[i] *= ratio ;
				OOOdiagNode[i] *= ratio * sqrt(ratio) ;
				OOOOdiagNode[i] *= ratio * ratio ;

				ParaNode[i] /= sqrt(ratio) ;
			}
		}
	}
	//---------------------------------------------------
	double *srOptO ;
	int sample, sampleStart, sampleEnd, sampleSize;
	SplitLoop(&sampleStart,&sampleEnd,NVMCSample,rank,size);
//	if (rank == 0) cout << sampleStart << ", " << sampleEnd << endl ;
	for(sample=sampleStart;sample<sampleEnd;sample++) {
		srOptO = SROptO_Store + sample * SROptSize ;

		double * OstoreSlater = srOptO + NProj + 1 ;
		for ( int i = 0; i < NSlater; i ++  ) {
			OstoreSlater[i] *= sqrt(SlaterRatio) ; // O
		}

		int j = 0 ;
		for ( int k = 0; k < fttn.nnodes(); k ++ ) {
			if ( fttn.ti()[k] == false ) {
				int offset = fttn.nodes()[k].offset() ;
				int nelements = fttn.nodes()[k].nelements() ;
				int para_offset = NProj + NSlater + offset ;
				double ratio = NodesRatio.at(j) ;
				j ++ ;

				double * OstoreNode = srOptO + 1 + para_offset ;
				for ( int i = 0; i < nelements; i ++ ) {
					OstoreNode[i] *= sqrt(ratio) ;
				}
			}
		}
	}
	//==================================================
	MPI_Barrier(comm) ;
}

void rescaleSROpt_max_noCG(MPI_Comm comm, FatTreeTensorNetwork& fttn) {
	cout << "rescaleSROpt_max_noCG not implemented yet." << endl ;
	exit(0) ;
}

void rescaleSROpt_mean(MPI_Comm comm, FatTreeTensorNetwork& fttn) {
	if ( useSRCG ) {
		rescaleSROpt_mean_CG(comm, fttn) ;
	} else {
		rescaleSROpt_mean_noCG(comm, fttn) ;
	}
}

void rescaleSROpt_mean_CG(MPI_Comm comm, FatTreeTensorNetwork& fttn) {
  int rank,size;
  MPI_Comm_rank(comm,&rank);
  MPI_Comm_size(comm,&size);

	vector<double> sDiagElm(NPara, 0) ;
	// S(i,i) = OOdiag[i] - O[i] * O[i]
	for ( int pi = 0; pi < NPara; pi ++) {
		sDiagElm[pi] = SROptOOdiag[pi + 1] - SROptO[pi + 1] * SROptO[pi + 1] ;
	}
	// use ProjMean to rescale Pf and TNS
	double ProjMean = findMean(NProj, sDiagElm, 0) ;
	double SlaterMean = findMean(NSlater, sDiagElm, NProj) ;
	double SlaterRatio = ProjMean / SlaterMean ;
	double * OSlater = SROptO + NProj + 1 ;

	double * HOSlater = SROptHO + NProj + 1 ;
	double * HOSlater_SD = SROptHO_SD + NProj + 1 ;

	double * OOdiagSlater = SROptOOdiag + NProj + 1 ;
	double * OOOdiagSlater = SROptOOOdiag + NProj + 1 ;
	double * OOOOdiagSlater = SROptOOOOdiag + NProj + 1 ;

	for ( int i = 0; i < NSlater; i ++ ) {
		OSlater[i] *= sqrt(SlaterRatio) ; // <O>

		HOSlater[i] *= sqrt(SlaterRatio) ; // <HO>
		HOSlater_SD[i] *= sqrt(SlaterRatio) ;

		OOdiagSlater[i] *= SlaterRatio ; // diagonal elements of <OO>
		OOOdiagSlater[i] *= SlaterRatio * sqrt(SlaterRatio) ; // <OOO>
		OOOOdiagSlater[i] *= SlaterRatio * SlaterRatio ; // <OOOO>

		Slater[i] /= sqrt(SlaterRatio) ; // wave function
	}

	vector<double> NodesRatio ;
	for ( int k = 0 ; k < fttn.nnodes(); k ++ ) {
		if ( fttn.ti()[k] == false ) {
			int offset = fttn.nodes()[k].offset() ;
			int nelements = fttn.nodes()[k].nelements() ;
			int para_offset = NProj + NSlater + offset ;
			double NodeMean = findMean(nelements, sDiagElm, para_offset) ;
			double ratio = ProjMean/NodeMean ;
//			cout << k << "-th node ratio: " << ratio << endl ;
			NodesRatio.push_back( ratio ) ;

			double * ONode = SROptO + 1 + para_offset ;

			double * HONode = SROptHO + 1 + para_offset ;
			double * HONode_SD = SROptHO_SD + 1 + para_offset ;

			double * OOdiagNode = SROptOOdiag + 1 + para_offset ;
			double * OOOdiagNode = SROptOOOdiag + 1 + para_offset ;
			double * OOOOdiagNode = SROptOOOOdiag + 1 + para_offset ;

			double * ParaNode = PEPSelem + offset ;

			for ( int i = 0 ; i < nelements; i ++ ) {
				ONode[i] *= sqrt(ratio) ;

				HONode[i] *= sqrt(ratio) ;
				HONode_SD[i] *= sqrt(ratio) ;

				OOdiagNode[i] *= ratio ;
				OOOdiagNode[i] *= ratio * sqrt(ratio) ;
				OOOOdiagNode[i] *= ratio * ratio ;

				ParaNode[i] /= sqrt(ratio) ;
			}
		}
	}
	//---------------------------------------------------
	double *srOptO ;
	int sample, sampleStart, sampleEnd, sampleSize;
	SplitLoop(&sampleStart,&sampleEnd,NVMCSample,rank,size);
//	if (rank == 0) cout << sampleStart << ", " << sampleEnd << endl ;
	for(sample=sampleStart;sample<sampleEnd;sample++) {
		srOptO = SROptO_Store + sample * SROptSize ;

		double * OstoreSlater = srOptO + NProj + 1 ;
		for ( int i = 0; i < NSlater; i ++  ) {
			OstoreSlater[i] *= sqrt(SlaterRatio) ; // O
		}

		int j = 0 ;
		for ( int k = 0; k < fttn.nnodes(); k ++ ) {
			if ( fttn.ti()[k] == false ) {
				int offset = fttn.nodes()[k].offset() ;
				int nelements = fttn.nodes()[k].nelements() ;
				int para_offset = NProj + NSlater + offset ;
				double ratio = NodesRatio.at(j) ;
				j ++ ;

				double * OstoreNode = srOptO + 1 + para_offset ;
				for ( int i = 0; i < nelements; i ++ ) {
					OstoreNode[i] *= sqrt(ratio) ;
				}
			}
		}
	}
	//==================================================
	MPI_Barrier(comm) ;
}
void rescaleSROpt_mean_noCG(MPI_Comm comm, FatTreeTensorNetwork& fttn) {
  int rank,size;
  MPI_Comm_rank(comm,&rank);
  MPI_Comm_size(comm,&size);

	vector<double> sDiagElm(NPara, 0) ;
	for ( int pi = 0; pi < NPara; pi ++) {
		// sDiagElm is temporarily used for diagonal elements of S
		// S[i][i] = OO[pi+1][pi+1] - OO[0][pi+1] * OO[0][pi+1]
		sDiagElm[pi] = SROptOO[(pi+1)*SROptSize+pi+1] - SROptOO[pi+1] * SROptOO[pi+1] ;
	}
	vector<double> ratios(NProj + 1, 1.0) ;
	// use ProjMean to rescale Pf and TNS
	double ProjMean = findMean(NProj, sDiagElm, 0) ;
	double SlaterMean = findMean(NSlater, sDiagElm, NProj) ;
	double SlaterRatio = ProjMean / SlaterMean ;
	double * HOSlater = SROptHO + NProj + 1 ;
	for ( int i = 0; i < NSlater; i ++ ) {
		HOSlater[i] *= sqrt(SlaterRatio) ; // <HO>
		ratios.push_back(sqrt(SlaterRatio)) ;
		Slater[i] /= sqrt(SlaterRatio) ; // wave function
	}

	for ( int k = 0 ; k < fttn.nnodes(); k ++ ) {
		if ( fttn.ti()[k] == false ) { // non-equivalent site
			int offset = fttn.nodes()[k].offset() ;
			int nelements = fttn.nodes()[k].nelements() ;
			int para_offset = NProj + NSlater + offset ;
			double NodeMean = findMean(nelements, sDiagElm, para_offset) ;
			double ratio = ProjMean/NodeMean ;

			double * HONode = SROptHO + 1 + para_offset ;
			double * ParaNode = PEPSelem + offset ;

			for ( int i = 0 ; i < nelements; i ++ ) {
				HONode[i] *= sqrt(ratio) ;
				ratios.push_back(sqrt(ratio)) ;
				ParaNode[i] /= sqrt(ratio) ;
			}
		}
	}
	// rescale SROptOO
	for ( int j = 0; j < SROptSize; j ++ ) {
		for ( int i = 0; i < SROptSize; i ++ ) {
			int idx = i + j * SROptSize ;
			SROptOO[idx] *= ratios[i] * ratios[j] ;
		}
	}
	//==================================================

}

double findMax(int n, std::vector<double> V, int i0) {
	double Vmax = V[i0] ;
	for ( int i = i0; i < ( i0 + n ); i ++ ) {
		if ( Vmax < fabs(V[i]) ) Vmax = V[i] ;
//		cout << V[i] << ",  " ;
	}
//	cout << endl << "max value: " << Vmax << endl;
//	cout << endl << "---------------------------------------------------------------------" << endl ;
	return Vmax ;
}

double findMean(int n, std::vector<double> V, int i0) {
	double Vmean = 0 ;
	int n_nonzero = 0 ;
	for ( int i = i0; i < (i0 + n ); i ++ ) {
		if ( V[i] > 1e-14 ) {
			Vmean += V[i] ;
			n_nonzero ++ ;
		}
	}
	return (Vmean / n_nonzero) ;
}

/* shift Gutzwiller-Jastrow factor */
void shiftGJ() {
  double shift=0.0;
  const int n = NGutzwillerIdx+NJastrowIdx;
  int i;

  if(NGutzwillerIdx==0||NJastrowIdx==0) return;

  for(i=0;i<n;i++) {
    shift += Proj[i];
  }
  shift /= (double)n;

  for(i=0;i<n;i++) {
    Proj[i] -= shift;
  }

  return;
}

/* shift 2-site DH Correlation factor */
/* The return value is gutzwiller shift width */
double shiftDH2() {
  double gShift=0.0;
  double shift;
  int xn,n0,n1,n2,offset;

  if(NDoublonHolon2siteIdx==0) return 0.0;

  /* 2-site doublon-holon correlation factor */
  offset = NGutzwillerIdx + NJastrowIdx;
  for(xn=0;xn<2*NDoublonHolon2siteIdx;xn++) { /* factor 2: d or h */
    /* n = offset + xn + (xdh+2*xm)*xNDoublonHolon2siteIdx; */
    n0 = offset + xn;
    n1 = n0 + 2*NDoublonHolon2siteIdx;
    n2 = n0 + 4*NDoublonHolon2siteIdx;
    shift = (Proj[n0]+Proj[n1]+Proj[n2])/3.0;
    Proj[n0] -= shift;
    Proj[n1] -= shift;
    Proj[n2] -= shift;
    gShift += shift;
  }

  return gShift;
}

/* shift 4-site DH Correlation factor */
/* The return value is gutzwiller shift width */
double shiftDH4() {
  double gShift=0.0;
  double shift;
  int xn,n0,n1,n2,n3,n4,offset;

  if(NDoublonHolon4siteIdx==0) return 0.0;

  offset = NGutzwillerIdx + NJastrowIdx + 6*NDoublonHolon2siteIdx;
  for(xn=0;xn<2*NDoublonHolon4siteIdx;xn++) { /* factor 2: d or h */
    /* n = offset + xn + (xdh+2*xm)*xNDoublonHolon4siteIdx; */
    n0 = offset + xn;
    n1 = n0 + 2*NDoublonHolon4siteIdx;
    n2 = n0 + 4*NDoublonHolon4siteIdx;
    n3 = n0 + 6*NDoublonHolon4siteIdx;
    n4 = n0 + 8*NDoublonHolon4siteIdx;
    shift = (Proj[n0]+Proj[n1]+Proj[n2]+Proj[n3]+Proj[n4])/5.0;
    Proj[n0] -= shift;
    Proj[n1] -= shift;
    Proj[n2] -= shift;
    Proj[n3] -= shift;
    Proj[n4] -= shift;
    gShift += shift;
  }

  return gShift;
}

void SetFlagShift() {
  int i,start,end;

  /* Gutzwiller */
  FlagShiftGJ=0;
  FlagShiftDH2=0;
  FlagShiftDH4=0;
  if(NGutzwillerIdx==0) return;
  start = 0;
  end = start + NGutzwillerIdx;
  for(i=start;i<end;i++) {
    if(OptFlag[i]!=1) return;
  }
  
  /* Jastrow */
  if(NJastrowIdx>0) {
    start = end;
    end   = start + NJastrowIdx;
    FlagShiftGJ=1;
    for(i=start;i<end;i++) {
      if(OptFlag[i]!=1) {
        FlagShiftGJ=0;
        break;
      }
    }
  }
  
  /* 2-site Doublon-Holon */
  if(NDoublonHolon2siteIdx>0) {
    start = end;
    end   = start + 6*NDoublonHolon2siteIdx;
    FlagShiftDH2=1;
    for(i=start;i<end;i++) {
      if(OptFlag[i]!=1) {
        FlagShiftDH2=0;
        break;
      }
    }
  }
  
  /* 4-site Doublon-Holon */
  if(NDoublonHolon4siteIdx>0) {
    start = end;
    end   = start + 10*NDoublonHolon4siteIdx;
    FlagShiftDH4=1;
    for(i=start;i<end;i++) {
      if(OptFlag[i]!=1) {
        FlagShiftDH4=0;
        break;
      }
    }
  }
  
  return;
}
